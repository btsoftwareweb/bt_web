<?php include 'header.php';?>
        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/about-slider-bg.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Refund Policy</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="privacy-policy">
  <div class="container">
    <div class="p-t">
    	<h2 class="secondary-color text-uppercase font-900">Refund Policy</h2>
        
        <p>We guarantee your full satisfaction or your money back. An administration fee of 3.5% of your original purchase price will be deducted from the amount refunded.</p>
        
        <h4 class="secondary-color text-uppercase">The refund policy will not take effect in any of the following events:</h4>
      
        <ul>
        	<li>If you have received the final files of your design.</li>
            <li>If you have approved your logo/web design.</li>
            <li>If work was commenced on one of your samples and 3 or more logo design changes were effected at your request.</li>
            <li>If the party for whom the logo/web is being designed closes, or changes its name, or changes its activity.</li>
            <li>If the project was cancelled for reason(s) unrelated to the logo/website design of BT Software Design.</li>
            <li>If you do not communicate with BT Software Design. for more than 4 months following the submission of the logo/web design.</li>
            <li>If other design companies were hired to work on the same design project at the same time.</li>
        </ul>
        
        <h4 class="secondary-color text-uppercase">How the money will be transferred:</h4>
        <p>If you have paid using your credit card OR Paypal - the money will be transferred back to your credit card account OR Paypal. It may take up to 6 business days before the money will appear on your account. If you have paid using a cheque or a money order your refund will be sent to you by paypal.</p>
   
    </div>
  </div>
</section>
<div class="clearfix"></div>
		
<?php include 'footer.php';?>