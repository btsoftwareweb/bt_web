<?php include 'header.php';?>

        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/web-design/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Web Development</span></h1>
					  <h3>
                            Create valuable and scalable web applications with <br>BT Software Design Agency
                        </h3>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->
			</div> <!-- /.carousel -->
        </section> <!-- /#home -->
		
<!-- Who We Are
    ================================================== -->

    <section id="who-we-are" class="wow fadeInLeft animated">
        <div class="container">
            <div class="row top-heading_row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="section_title ">
                        <h2>
                            Web design Types
                        </h2>
                        
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row tabs_row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" href="#tab1">Responsive <span>Website Development</span></a></li>
                        <li><a data-toggle="tab" href="#tab2">Content <span>Management System</span></a></li>
                        <li><a data-toggle="tab" href="#tab3">E-comm<span>erce Website / Online Store</span></a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-6 wow fadeInLeft animated">
                                    <img src="assets/images/web-design/site-img13.png" class="img-responsive center-block lazy" alt="image" style="margin-top:10px;" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents wow fadeInRight animated">
                                        <h3>
                                            Responsive Website Development
                                        </h3>
                                        <p>
                                            With responsive website development – you can customize your webpages to be viewed in response to the size and dimensions of the device in which it is being viewed. Create your Customized Responsive Web Design (RWD) with <span class="text-grad">BT Software</span>. As the name implies, it maximizes the response level. At <span class="text-grad">BT Software</span>, we develop website designs with responsive development strategy that adapts to mobile devices, promoting proper usability to the size and shape of these devices. We constantly train and develop our team members for providing the best responsive website solutions.  </p>
										<p class="text-grad"><b>Our RWD services include:</b></p>
										
										<div class="col-md-6 no-pad">
											<ul>
												<li>WordPress website development</li>
												<li>WordPress customization</li>
												<li>Plugin development</li>
												<li>Installation & configuration</li>
											</ul>
										</div>
										<div class="col-md-6">
											<ul>
												<li>Support & maintenance</li>
												<li>PSD to WordPress Conversion</li>
												<li>WordPress server and domain migration</li>
											</ul>
										</div>
										<div class="clearfix"></div>
										<a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/web-design/site-img14.png" class="img-responsive center-block lazy" alt="image" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                            Content Management System
                                        </h3>
                                        <p>
                                           Content is the heart of every website. It plays a major role of communication. Today, organizations face difficulties in managing and updating their website content and when they fail to do so, the website stagnates, and customers see the wrong information. <span class="text-grad">Content Management System</span> is recommended to manage your website content in a hassle-free way. Our advanced CMS solutions allows non-technical users to carry out maintainability of the website design. It minimizes the need of a web designer or website design agency for making changes to the site. </p>
										   <p>At <span class="text-grad">BT Software</span>, we have team of professionals specialized in <span class="text-grad">WordPress, Drupal</span> and <span class="text-grad">Customize CMS</span>.</p>
                                            <p class="text-grad"><b>Our CMS solutions will help you with:</b></p>
											<div class="col-md-6 no-pad">
												<ul>
													<li>Management of content</li>
													<li>Preview pages</li>
													<li>Date scheduling and time of publication</li>
													<li>Pages creation</li>
													<li>Custom Design</li>
													<li>Search system with display results by relevance</li>
													<li>Technical Facilities for SEO</li>
												</ul>
											</div>
											<div class="col-md-6">
												<ul>
													<li>Integration with Social Media</li>
													<li>High performance through features Cache</li>
													<li>Easy integration with Web Analytics systems</li>
													<li>User profile Management administrative area</li>
													<li>Customizing content with restricted access</li>
													<li>Discussion forum deployment possibility</li>
												</ul>
											</div>
											<a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab3" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/web-design/site-img15.png" class="img-responsive center-block lazy" alt="image" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                            E-commerce Website / Online Store
                                        </h3>
                                        <p>
                                           <span class="text-grad">BT Software</span> will help you create a flexible and organized <span class="text-grad">E-Commerce website design</span> solutions. It can be used for various industries as well as importers, distributors, wholesalers and start-ups. With the help of our advanced E-commerce solutions, organizations can generate their <span class="text-grad">ROI</span> effectively - in terms of increased sales and reduced costs. Leverage your business in the digital marketplace with our advanced E-Commerce solutions. </p>
										   <p class="text-grad"><b>BT Software’s Signature E-Commerce Solutions includes:</b></p>
										   <div class="col-md-6 no-pad">
												<ul>
													<li>B2B and B2C Portals OpenCart Ecommerce Solution</li> 
													<li>Prestation Ecommerce Solution</li>
													<li>Magento Community Edition</li>
													<li>Zen Cart Ecommerce Solution</li>
													<li>Spree Commerce Solution</li>
													<li>Multi-vendor Marketplace</li>
												</ul>
										   </div>
										   <div class="col-md-6">
											<ul>
												<li>Drupal Commerce Solution</li>
												<li>osCommerce Solution</li>
												<li>Simple Cart Solution</li>
												<li>WooCommerce Solution</li>
												<li>WP e-Commerce</li>
												<li>Jigoshop Ecommerce Solution</li>
											</ul>
										   </div>
										   <div class="clearfix"></div>
										<a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End Who We Are-->
        

        <section class="portfolio-sec">
          <img src="assets/images/client-bg.png" class="shape">
          <div class="container">
            <h1 class="wow fadeInUp">We are serving <span>2000+</span> Clients</h1>
            <p class="wow fadeInUp">We create experiences that transform brands & grow businesses. We have enriched some of the biggest brands and our partnerships with them have caused the creation of some outstanding outcomes. </p>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row logo logo-portfolio">
	<div class="row mr">
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/1.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/1-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/2.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/2-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/4.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/4-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/5.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/5-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/6.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/6-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/7.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/7-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/8.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/8-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/9.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/9-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/10.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/10-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/11.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/11-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/12.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/12-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/13.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/13-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/14.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/14-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/15.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/15-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/16.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/16-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/17.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/17-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/18.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/18-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/19.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/19-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/20.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/20-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/21.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/21-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/22.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/22-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>  
</div>                        
                </div>
              </div>
            </div>
           <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL</a></div>          </div>
        </section>

       
		<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section>
        

        <section class="packages portfolio-sec">
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">Web Packages We Offer</h1>
                <p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget. </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="tabs">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#resp_web" data-toggle="tab" aria-expanded="true">Responsive <span>Website</span></a></li>
        <li class=""><a href="#cms_web" data-toggle="tab" aria-expanded="false">CMS <span>Website</span></a></li>
		<li class=""><a href="#ecomm_web" data-toggle="tab" aria-expanded="false">E-Comm<span>erce Development</span></a></li>
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<div id="resp_web" class="tab-pane active">
			<div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box ninteen">
				  <div class="collapse-top collapsed" data-target="#ninteen" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$800</strike> $400</h6>
					  <p><strong class="text-uppercase">Basic </strong></br>Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="ninteen">
					<div class="package-list">
					  <ul>
						<li>4 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 3 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 2 Months Web Hosting</li>
						<li><strong>FREE</strong> 2 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 1GB Web Hosting Space</li>
						<li><strong>FREE</strong> 3 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twenty">
				  <div class="collapse-top collapsed" data-target="#twenty" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1600</strike> $800</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twenty">
					<div class="package-list">
					  <ul>
						<li>8 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 5 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 4 Months Web Hosting</li>
						<li><strong>FREE</strong> 4 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 3GB Web Hosting Space</li>
						<li><strong>FREE</strong> 5 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					</ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyone">
				  <div class="collapse-top collapsed" data-target="#twentyone" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$3200</strike> $1600</h6>
					  <p><strong class="text-uppercase">Premium</strong></br>Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyone">
					<div class="package-list">
					  <ul>
						<li>16 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Mobile Websites</li>
						<li>Custom Design - Exclusve Rights</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 10 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 6 Months Web Hosting</li>
						<li><strong>FREE</strong> 6 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 5GB Web Hosting Space</li>
						<li><strong>FREE</strong> 10 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box twentytwo bundle">
				  <div class="collapse-top collapsed" data-target="#twentytwo" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1500</strike> $750</h6>
					  <p><strong class="text-uppercase">Bundle </strong></br>(Logo & Web)<br>Responsive Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentytwo">
					<div class="package-list">
						<div class="col-md-4">
							<h6 class="text-grad">Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
								<li class="first">6 Unique Logo Concepts <strong>(FREE)</strong></li>
								<li>Complementary Icon <strong>(FREE)</strong></li>
								<li>Unlimited Revisions* <strong>(FREE)</strong></li>
								<li>100% Ownership Rights <strong>(FREE)</strong></li>
								<li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats <strong>(FREE)</strong></li>
								<li>Complementary Rush Delivery <strong>(FREE)</strong></li>
								<li class="last">Get Initial Concepts within 24 hours.</li>
							</ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
								<li class="first">8 Unique Pages Design</li>
								<li>1 Basic Contact / Inquiry Form</li>
								<li>Responsive ( Mobile / Tablet )</li>
								<li>Complete W3C Certified HTML</li>
								<li class="last">6 Month Maintenance/Support <strong>(FREE)</strong></li>
							</ul>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Branding</h6>
							<ul>
								<li class="first">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
								<li>Letterhead Design <strong>(FREE)</strong></li>
								<li>Envelope Design <strong>(FREE)</strong></li>
								<li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
							</ul>
						</div>
						<div class="col-xs-12">
							<ul>
								<li class="first"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>5 Stock Photos</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong> Google Friendly Sitemap</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong> 6 Months Web Hosting</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Free Domain Registration</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>3GB Web Hosting Space</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Email Accounts</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							</ul>
						</div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>
        </div>
		<div id="cms_web" class="tab-pane">
            <div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box twentythree">
				  <div class="collapse-top collapsed" data-target="#twentythree" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$800</strike> $400</h6>
					  <p><strong class="text-uppercase">Basic </strong></br>CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentythree">
					<div class="package-list">
					  <ul>
						<li>5 Unique Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li><strong>FREE</strong> 2 Months Web Hosting</li>
						<li><strong>FREE</strong> 2 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 1GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 3 Email Accountst</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyfour">
				  <div class="collapse-top collapsed" data-target="#twentyfour" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1600</strike> $800</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyfour">
					<div class="package-list">
					  <ul>
						<li>10 Unique Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li>Text Editor Module</li>
						<li>Photo Gallery Module</li>
						<li>Google Map Module</li>
						<li><strong>FREE</strong> 4 Months Web Hosting</li>
						<li><strong>FREE</strong> 4 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 3GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 5 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyfive">
				  <div class="collapse-top collapsed" data-target="#twentyfive" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$2400</strike> $1200</h6>
					  <p><strong class="text-uppercase">Premium</strong></br> CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyfive">
					<div class="package-list">
					  <ul>
						<li>Unlimited Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li>Visual Composer Module</li>
						<li>Photo Gallery Module</li>
						<li>Google Map Module</li>
						<li>Sitemap Module</li>
						<li>Blog Integration</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li><strong>FREE</strong> 6 Months Web Hosting</li>
						<li><strong>FREE</strong> 6 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 5GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 10 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on" style="color: #ed145b !important;">Unlimited Pages</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box twentysix bundle">
				  <div class="collapse-top collapsed" data-target="#twentysix" aria-expanded="true">
					<div class="price-box">
					  <h6> <strike>$1500</strike> $750</h6>
					  <p><strong class="text-uppercase">Bundle</strong></br> (Logo & Web)<br>CMS Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentysix">
					<div class="package-list">
						<div class="col-md-4">
							<h6 class="text-grad">Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">8 Unique Logo Concepts <strong>(FREE)</strong></li>
                                <li>Complementary Icon<strong>(FREE)</strong></li>
                                <li>Unlimited Revisions*<strong>(FREE)</strong></li>
                                <li>100% Ownership Rights<strong>(FREE)</strong></li>
                                <li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats<strong>(FREE)</strong></li>
                                <li class="last">Stationery Design <strong>(FREE)</strong></li>
                            </ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">10 Unique Pages Design</li>
                                <li>Contact Form</li>
                                <li>Social Media Integration</li>
                                <li>Text Editor Module</li>
                                <li>Photo Gallery Module</li>
                                <li>Google Map Module</li>
                                <li>6 Month Maintenance/Support <strong>(FREE)</strong></li>
                                <li class="last">Secure Sockets Layer(SSL) <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Branding</h6>
							<ul>
                                <li class="first active">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
                                <li>Letterhead Design <strong>(FREE)</strong></li>
                                <li>Envelope Design <strong>(FREE)</strong></li>
                                <li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-xs-12">
                            <ul>
                                <li class="first active"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Manage Your Own Content</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Web Hosting</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Free Domain Registration</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>3GB Web Hosting Space</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>1 MySQL Database</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Control Panel Access</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>FTP Access</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Email Accounts</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
                             </ul>
                        </div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>                            

        </div>
		<div id="ecomm_web" class="tab-pane ">
            <div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box twentyseven">
				  <div class="collapse-top collapsed" data-target="#twentyseven" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1690</strike> $845</h6>
					  <p><strong class="text-uppercase">Basic</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyseven">
					<div class="package-list">
					  <ul>
						<li>5 Unique Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Upto 100 Products</li>
						<li>Upto 5 Categories</li>
						<li>Mini Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 5 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li>Dedicated Designer and Developer</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
					 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyeight">
				  <div class="collapse-top collapsed" data-target="#twentyeight" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$3040</strike> $1520</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyeight">
					<div class="package-list">
					  <ul>
						<li>8 Unique Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Upto 300 Products</li>
						<li>Upto 10 Categories</li>
						<li>Customer's Login Area</li>
						<li>Full Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Easy Product Search</li>
						<li>Product Reviews</li>
						<li>Complete W3C Certified HTML</li>
						<li class="grad-color"> BASIC LOGO DESIGN</li>
						<li>2 Logo Design Concepts</li>
						<li>1 Dedicated Logo Designer</li>
						<li>Unlimited Revisions*</li>
						<li>5 Promotional Banners</li>
						<li><strong>FREE</strong> 10 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li>Team of Expert Designers and Developers</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentynine">
				  <div class="collapse-top collapsed" data-target="#twentynine" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$9100</strike> $4550</h6>
					  <p><strong class="text-uppercase">Premium</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentynine">
					<div class="package-list">
					  <ul>
						<li>Unlimited Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Unlimited Products</li>
						<li>Unlimited Categories</li>
						<li>Customer's Login Area</li>
						<li>Full Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Sales & Inventory Management</li>
						<li>Newsletter Subscription List Integration</li>
						<li>Easy Product Search</li>
						<li>Product Reviews</li>
						<li>Complete W3C Certified HTML</li>
						<li class="grad-color"> PROFESSIONAL LOGO DESIGN PACKAGE</li>
						<li>5 Logo Design Concepts</li>
						<li>2 Dedicated Logo Designers</li>
						<li>Unlimited Revisions*</li>
						<li class="grad-color"> LANDING PAGE</li>
						<li>1 Customized Landing Page Design</li>
						<li class="grad-color"> SOCIAL MEDIA</li>
						<li>Facebook, Youtube & Twitter Pages Design</li>
						<li class="grad-color"> BROCHURE</li>
						<li>1 Double Sided Brochure Design (A4) or Trifold</li>
						<li>5 Promotional Banners</li>
						<li>FREE 1 Stationery Set<span>(Letterhead, Envelope, Business Card)</span></li>
						<li>FREE 15 Stock Photos</li>
						<li>FREE Search Engine Submission</li>
						<li>FREE Google Friendly Sitemap</li>
						<li>Team of Dedicated Designers and Developers</li>
						<li>Dedicated Brand Experts</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box thirty bundle">
				  <div class="collapse-top collapsed" data-target="#thirty" aria-expanded="true">
					<div class="price-box">
					  <h6> <strike>$7000</strike> $3500</h6>
					  <p><strong class="text-uppercase">Bundle</strong></br> (Logo & Web)<br>E-Commerce Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="thirty">
					<div class="package-list">
						<div class="col-md-4">
							<h6>Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">12 Unique Logo Concepts<strong>(FREE)</strong></li>
                                <li>Complementary Icon<strong>(FREE)</strong></li>
                                <li>Unlimited Revisions*<strong>(FREE)</strong></li>
                                <li>100% Ownership Rights<strong>(FREE)</strong></li>
                                <li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats<strong>(FREE)</strong></li>
                                <li class="last">Stationery Design <strong>(FREE)</strong></li>
                            </ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6>Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">Unlimited Unique Pages Design</li>
                                <li>Content Management System (CMS)</li>
                                <li>Upto 300 Products</li>
                                <li>Upto 10 Categories</li>
                                <li>Customer's Login Area</li>
                                <li>Full Shopping Cart Integration</li>
                                <li>Payment Module Integration</li>
                                <li>Easy Product Search</li>
                                <li>Product Reviews</li>
                                <li>Complete W3C Certified HTML</li>
								<li>1 Year Maintenance/Support <strong>(FREE)</strong></li>
								<li>Secure Sockets Layer(SSL) <strong>(FREE)</strong></li>
								<li class="last">Security Pack <strong>(Add-on)</strong></li>
                            </ul>
						</div>
						<div class="col-md-4">
							<h6>Branding</h6>
							<ul>
                                <li class="first active">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
                                <li>Letterhead Design <strong>(FREE)</strong></li>
                                <li>Envelope Design <strong>(FREE)</strong></li>
                                <li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-xs-12">
                            <ul>
                                <li class="first active"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Stock Photos</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Google Friendly Sitemap</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Team of Expert Designers and Developers</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
                            </ul>
                        </div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>                              
        </div>
        
        
        
                                    
    </div>
</div>
					
					
					
					<!---->

                       
                </div>
              </div>
            </div>
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
                <h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>              </div>
            </div>
          </div>
        </section>

       

        <div class="clearfix"></div>

        <section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="javascript:;"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
				</section>    

				<?php include 'footer.php';?>