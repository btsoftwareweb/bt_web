<?php include 'header.php';?>
        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/about-slider-bg.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Terms Of Use</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="privacy-policy">
  <div class="container">
    <div class="p-t">
    	<h2 class="secondary-color text-uppercase font-900">Terms of Services</h2>
        
        <p>For the entirety of this document “we”, “our”, “us” and “BT Software DESIGN” refer to BT Software DESIGN that are the owners of this site and their associate that deliver services to you are subject to given terms and conditions. When you visit our website or buy our product/service, you agree to these terms and conditions.  Kindly refer to the below carefully:</p>
        
        <h4 class="secondary-color text-uppercase">Acceptance of Terms</h4>
        <p>When you visit BT Software DESIGN (The website) you agree to abide by the terms and conditions and you consent to our privacy policy. If you don’t agree to any of the terms and conditions mentioned in this document, you should not go ahead with any service/products from us and leave this website immediately. You agree that you will not be making use of this website for unlawful purposes and would bear in mind all the laws and regulations. You agree to not use this website in a way that may impact its performance, corrupt the content or in any way hinder the functionality of this website. You also agree to not compromise the safety of this website or trying to enter restricted areas of this website.
You consent to be entirely responsible for any claim, liability, expense, costs and losses that include legal fees by us which starts from violation of the mentioned terms and conditions.
</p>
        
        <h4 class="secondary-color text-uppercase">Modification</h4>
        <p>We reserve the right to change any part of this document without any prior notice and your usage of our website would amount to your consent to these terms and conditions. We encourage our visitors to constantly check this document. BT Software DESIGN reserves the full right to alter or erase any part of this site without warning or liability that sparks from that action.</p>
        
        <h4 class="secondary-color text-uppercase">Limitation of Liability</h4>
        <p>BT Software DESIGN would not be held responsible under any conditions for indirect, unique or consequential damages that include any business, profit, revenue or data loss linked with your use of our website or any service/product provided by us.  We will not be held responsible under any conditions for the death or any personal injury. If you are not comfortable or satisfied with any of our material or our terms and conditions, you should leave right now and stop using our services. We strive to ensure that our website is virus-free, we however don’t guarantee the absence of all malwares and viruses and we also don’t give any guarantees regarding e-mail attachments that you might have received from third parties. The recipients are encouraged to check email, attachments and other files before accessing them.</p>
        
        <h4 class="secondary-color text-uppercase">Loss</h4>
        <p>We are not responsible for any lost file; you are advised to always keep an original copy of your work.</p>
        
        <h4 class="secondary-color text-uppercase">Prices</h4>
        <p>Prices included on the website are just estimates that are liable to change without prior notice. You are encouraged to get a definite quote before you submit your project.</p>
        
        <h4 class="secondary-color text-uppercase">Payment</h4>
        <p>Payment is to be made in full in advance. We might be able to set up monthly/weekly/quarterly payment plans for our loyal users.</p>
        
        <h4 class="secondary-color text-uppercase">Warranty</h4>
        <p>Both the parties warrant their power to enter this agreement and gained the approval to do so.</p>
        
        <ul>
        	<li>Any alterations in your instructions.</li>
            <li>If you forgot to mention the complete instructions.</li>
            <li>Interruptions, delays or overtime for reason beyond the control of BT Software DESIGN.</li>
        </ul>
        
        <h4 class="secondary-color text-uppercase">Cancellation</h4>
        <p>You have the right to cancel your work any time before we have started working on it. If you wish to stop the work once we have started working on it, you would be charged for the work that has been completed till then. The half completed work will also be delivered to you upon cancellation.</p>
        
        <h4 class="secondary-color text-uppercase">Contract Termination</h4>
        <p>We reserve the right to terminate any assignment for any reason without any prior notice.</p>

<h4 class="secondary-color text-uppercase">Copyright</h4>
<p>Your usage of the website means that you agree to respect the individual property of BT Software DESIGN for example parents, design trademarks, names and other property owned by the website. You promise to not copy, reproduce, print, download or transmit any material provided within the website.</p>

<h4 class="secondary-color text-uppercase">Third Parties</h4>
        <p>This website might include hyperlinks to third party sites. We do not endorse such websites nor take any responsibility for it. We are not liable for the content displayed by these sites. Such sites are only advertised for your convenience and ease. These third party websites, their services and products are neither endorsed nor linked to our website.</p>
        
        <h4 class="secondary-color text-uppercase">Severance</h4>
        <p>In case, any part of this document is considered void or unenforceable, the remaining policy would still stay in effect.</p>
        
        <h4 class="secondary-color text-uppercase">Force Majeure</h4>
        <p>Both parties would be free from their duties in the event of national emergency, war, governmental regulations or any other reasons both parties cannot control.</p>
        
         <p>For the entirety of this document “we”, “our”, “us” and “BT Software DESIGN” refer to BT Software DESIGN that are the owners of this site and their associate that deliver services to you are subject to given terms and conditions. When you visit our website or buy our product/service, you agree to these terms and conditions.  Kindly refer to the below carefully:</p>
        
    </div>
  </div>
</section>
<div class="clearfix"></div>
		
<?php include 'footer.php';?>