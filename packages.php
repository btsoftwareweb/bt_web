<?php include 'header.php';?>
        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/portfolio/portfolio-slider.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>OUR PRICING</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="packages portfolio-sec">
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
				<div class="col-md-2"></div>
              <div class="col-md-8">
                <h1 class="wow fadeInUp">Packages We Offer</h1>
                <p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models.  We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget.  </p>
              </div>
			  <div class="col-md-2"></div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs wow fadeInUp" role="tablist">
						  <li role="presentation" class="active"><a href="#website-pkg" role="tab" data-toggle="tab">Website</a></li>
                          <li role="presentation" ><a href="#logo-pkg" role="tab" data-toggle="tab">Logo</a></li>
                          <li role="presentation"><a href="#corp-iden-pkg" role="tab" data-toggle="tab">Branding</a></li>
                          <li role="presentation"><a href="#vid-anim-pkg" role="tab" data-toggle="tab">Video <span>Animation</span></a></li>
                          <li role="presentation"><a href="#cpy-write-pkg" role="tab" data-toggle="tab">Copy<span>Writing</span></a></li>
                          <li role="presentation"><a href="#app-design-pkg" role="tab" data-toggle="tab">App<span>Designs</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
						  <div role="tabpanel" class="tab-pane fade in active" id="website-pkg">
                            <div class="tabs">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#resp_web" data-toggle="tab" aria-expanded="true">Responsive <span>Website</span></a></li>
        <li class=""><a href="#cms_web" data-toggle="tab" aria-expanded="false">CMS <span>Website</span></a></li>
		<li class=""><a href="#ecomm_web" data-toggle="tab" aria-expanded="false">E-Comm<span>erce Development</span></a></li>
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<div id="resp_web" class="tab-pane active">
			<div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box ninteen">
				  <div class="collapse-top collapsed" data-target="#ninteen" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$800</strike> $400</h6>
					  <p><strong class="text-uppercase">Basic </strong></br>Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="ninteen">
					<div class="package-list">
					  <ul>
						<li>4 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 3 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 2 Months Web Hosting</li>
						<li><strong>FREE</strong> 2 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 1GB Web Hosting Space</li>
						<li><strong>FREE</strong> 3 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twenty">
				  <div class="collapse-top collapsed" data-target="#twenty" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1600</strike> $800</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twenty">
					<div class="package-list">
					  <ul>
						<li>8 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 5 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 4 Months Web Hosting</li>
						<li><strong>FREE</strong> 4 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 3GB Web Hosting Space</li>
						<li><strong>FREE</strong> 5 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					</ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyone">
				  <div class="collapse-top collapsed" data-target="#twentyone" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$3200</strike> $1600</h6>
					  <p><strong class="text-uppercase">Premium</strong></br>Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyone">
					<div class="package-list">
					  <ul>
						<li>16 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Mobile Websites</li>
						<li>Custom Design - Exclusve Rights</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 10 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 6 Months Web Hosting</li>
						<li><strong>FREE</strong> 6 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 5GB Web Hosting Space</li>
						<li><strong>FREE</strong> 10 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box twentytwo bundle">
				  <div class="collapse-top collapsed" data-target="#twentytwo" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1500</strike> $750</h6>
					  <p><strong class="text-uppercase">Bundle </strong></br>(Logo & Web)<br>Responsive Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentytwo">
					<div class="package-list">
						<div class="col-md-4">
							<h6 class="text-grad">Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
								<li class="first">6 Unique Logo Concepts <strong>(FREE)</strong></li>
								<li>Complementary Icon <strong>(FREE)</strong></li>
								<li>Unlimited Revisions* <strong>(FREE)</strong></li>
								<li>100% Ownership Rights <strong>(FREE)</strong></li>
								<li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats <strong>(FREE)</strong></li>
								<li>Complementary Rush Delivery <strong>(FREE)</strong></li>
								<li class="last">Get Initial Concepts within 24 hours.</li>
							</ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
								<li class="first">8 Unique Pages Design</li>
								<li>1 Basic Contact / Inquiry Form</li>
								<li>Responsive ( Mobile / Tablet )</li>
								<li>Complete W3C Certified HTML</li>
								<li class="last">6 Month Maintenance/Support <strong>(FREE)</strong></li>
							</ul>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Branding</h6>
							<ul>
								<li class="first">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
								<li>Letterhead Design <strong>(FREE)</strong></li>
								<li>Envelope Design <strong>(FREE)</strong></li>
								<li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
							</ul>
						</div>
						<div class="col-xs-12">
							<ul>
								<li class="first"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>5 Stock Photos</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong> Google Friendly Sitemap</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong> 6 Months Web Hosting</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Free Domain Registration</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>3GB Web Hosting Space</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Email Accounts</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							</ul>
						</div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>
        </div>
		<div id="cms_web" class="tab-pane">
            <div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box twentythree">
				  <div class="collapse-top collapsed" data-target="#twentythree" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$800</strike> $400</h6>
					  <p><strong class="text-uppercase">Basic </strong></br>CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentythree">
					<div class="package-list">
					  <ul>
						<li>5 Unique Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li><strong>FREE</strong> 2 Months Web Hosting</li>
						<li><strong>FREE</strong> 2 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 1GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 3 Email Accountst</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyfour">
				  <div class="collapse-top collapsed" data-target="#twentyfour" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1600</strike> $800</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyfour">
					<div class="package-list">
					  <ul>
						<li>10 Unique Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li>Text Editor Module</li>
						<li>Photo Gallery Module</li>
						<li>Google Map Module</li>
						<li><strong>FREE</strong> 4 Months Web Hosting</li>
						<li><strong>FREE</strong> 4 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 3GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 5 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a></div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyfive">
				  <div class="collapse-top collapsed" data-target="#twentyfive" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$2400</strike> $1200</h6>
					  <p><strong class="text-uppercase">Premium</strong></br> CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyfive">
					<div class="package-list">
					  <ul>
						<li>Unlimited Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li>Visual Composer Module</li>
						<li>Photo Gallery Module</li>
						<li>Google Map Module</li>
						<li>Sitemap Module</li>
						<li>Blog Integration</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li><strong>FREE</strong> 6 Months Web Hosting</li>
						<li><strong>FREE</strong> 6 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 5GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 10 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on" style="color: #ed145b !important;">Unlimited Pages</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box twentysix bundle">
				  <div class="collapse-top collapsed" data-target="#twentysix" aria-expanded="true">
					<div class="price-box">
					  <h6> <strike>$1500</strike> $750</h6>
					  <p><strong class="text-uppercase">Bundle</strong></br> (Logo & Web)<br>CMS Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentysix">
					<div class="package-list">
						<div class="col-md-4">
							<h6 class="text-grad">Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">8 Unique Logo Concepts <strong>(FREE)</strong></li>
                                <li>Complementary Icon<strong>(FREE)</strong></li>
                                <li>Unlimited Revisions*<strong>(FREE)</strong></li>
                                <li>100% Ownership Rights<strong>(FREE)</strong></li>
                                <li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats<strong>(FREE)</strong></li>
                                <li class="last">Stationery Design <strong>(FREE)</strong></li>
                            </ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">10 Unique Pages Design</li>
                                <li>Contact Form</li>
                                <li>Social Media Integration</li>
                                <li>Text Editor Module</li>
                                <li>Photo Gallery Module</li>
                                <li>Google Map Module</li>
                                <li>6 Month Maintenance/Support <strong>(FREE)</strong></li>
                                <li class="last">Secure Sockets Layer(SSL) <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Branding</h6>
							<ul>
                                <li class="first active">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
                                <li>Letterhead Design <strong>(FREE)</strong></li>
                                <li>Envelope Design <strong>(FREE)</strong></li>
                                <li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-xs-12">
                            <ul>
                                <li class="first active"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Manage Your Own Content</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Web Hosting</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Free Domain Registration</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>3GB Web Hosting Space</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>1 MySQL Database</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Control Panel Access</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>FTP Access</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Email Accounts</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
                             </ul>
                        </div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>                            

        </div>
		<div id="ecomm_web" class="tab-pane ">
            <div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box twentyseven">
				  <div class="collapse-top collapsed" data-target="#twentyseven" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1690</strike> $845</h6>
					  <p><strong class="text-uppercase">Basic</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyseven">
					<div class="package-list">
					  <ul>
						<li>5 Unique Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Upto 100 Products</li>
						<li>Upto 5 Categories</li>
						<li>Mini Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 5 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li>Dedicated Designer and Developer</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
					 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyeight">
				  <div class="collapse-top collapsed" data-target="#twentyeight" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$3040</strike> $1520</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyeight">
					<div class="package-list">
					  <ul>
						<li>8 Unique Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Upto 300 Products</li>
						<li>Upto 10 Categories</li>
						<li>Customer's Login Area</li>
						<li>Full Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Easy Product Search</li>
						<li>Product Reviews</li>
						<li>Complete W3C Certified HTML</li>
						<li class="grad-color"> BASIC LOGO DESIGN</li>
						<li>2 Logo Design Concepts</li>
						<li>1 Dedicated Logo Designer</li>
						<li>Unlimited Revisions*</li>
						<li>5 Promotional Banners</li>
						<li><strong>FREE</strong> 10 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li>Team of Expert Designers and Developers</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentynine">
				  <div class="collapse-top collapsed" data-target="#twentynine" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$9100</strike> $4550</h6>
					  <p><strong class="text-uppercase">Premium</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentynine">
					<div class="package-list">
					  <ul>
						<li>Unlimited Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Unlimited Products</li>
						<li>Unlimited Categories</li>
						<li>Customer's Login Area</li>
						<li>Full Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Sales & Inventory Management</li>
						<li>Newsletter Subscription List Integration</li>
						<li>Easy Product Search</li>
						<li>Product Reviews</li>
						<li>Complete W3C Certified HTML</li>
						<li class="grad-color"> PROFESSIONAL LOGO DESIGN PACKAGE</li>
						<li>5 Logo Design Concepts</li>
						<li>2 Dedicated Logo Designers</li>
						<li>Unlimited Revisions*</li>
						<li class="grad-color"> LANDING PAGE</li>
						<li>1 Customized Landing Page Design</li>
						<li class="grad-color"> SOCIAL MEDIA</li>
						<li>Facebook, Youtube & Twitter Pages Design</li>
						<li class="grad-color"> BROCHURE</li>
						<li>1 Double Sided Brochure Design (A4) or Trifold</li>
						<li>5 Promotional Banners</li>
						<li>FREE 1 Stationery Set<span>(Letterhead, Envelope, Business Card)</span></li>
						<li>FREE 15 Stock Photos</li>
						<li>FREE Search Engine Submission</li>
						<li>FREE Google Friendly Sitemap</li>
						<li>Team of Dedicated Designers and Developers</li>
						<li>Dedicated Brand Experts</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box thirty bundle">
				  <div class="collapse-top collapsed" data-target="#thirty" aria-expanded="true">
					<div class="price-box">
					  <h6> <strike>$7000</strike> $3500</h6>
					  <p><strong class="text-uppercase">Bundle</strong></br> (Logo & Web)<br>E-Commerce Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="thirty">
					<div class="package-list">
						<div class="col-md-4">
							<h6>Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">12 Unique Logo Concepts<strong>(FREE)</strong></li>
                                <li>Complementary Icon<strong>(FREE)</strong></li>
                                <li>Unlimited Revisions*<strong>(FREE)</strong></li>
                                <li>100% Ownership Rights<strong>(FREE)</strong></li>
                                <li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats<strong>(FREE)</strong></li>
                                <li class="last">Stationery Design <strong>(FREE)</strong></li>
                            </ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6>Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">Unlimited Unique Pages Design</li>
                                <li>Content Management System (CMS)</li>
                                <li>Upto 300 Products</li>
                                <li>Upto 10 Categories</li>
                                <li>Customer's Login Area</li>
                                <li>Full Shopping Cart Integration</li>
                                <li>Payment Module Integration</li>
                                <li>Easy Product Search</li>
                                <li>Product Reviews</li>
                                <li>Complete W3C Certified HTML</li>
								<li>1 Year Maintenance/Support <strong>(FREE)</strong></li>
								<li>Secure Sockets Layer(SSL) <strong>(FREE)</strong></li>
								<li class="last">Security Pack <strong>(Add-on)</strong></li>
                            </ul>
						</div>
						<div class="col-md-4">
							<h6>Branding</h6>
							<ul>
                                <li class="first active">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
                                <li>Letterhead Design <strong>(FREE)</strong></li>
                                <li>Envelope Design <strong>(FREE)</strong></li>
                                <li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-xs-12">
                            <ul>
                                <li class="first active"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Stock Photos</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Google Friendly Sitemap</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Team of Expert Designers and Developers</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
                            </ul>
                        </div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>                              
        </div>
        
        
        
                                    
    </div>
</div>
					
					
					
					<!---->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="logo-pkg">
                            <div class="tabs logo-pricing">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class=""><a href="#illus-pkg" data-toggle="tab" aria-expanded="false"> Illustrative <span>Design</span></a></li>
		<li class=""><a href="#mascot-pkg" data-toggle="tab" aria-expanded="false"> Mascot <span>Design</span></a></li>
		<li class=""><a href="#3d-pkg" data-toggle="tab" aria-expanded="false">3D <span>Design</span> </a></li>
		<li class=""><a href="#abs-pkg" data-toggle="tab" aria-expanded="false">Abstract <span>Design</span></a></li>
		<li class=""><a href="#icon-pkg" data-toggle="tab" aria-expanded="true">Iconic <span>Design</span></a></li>
		<li class="active"><a href="#flat-pkg" data-toggle="tab" aria-expanded="false">Flat <span>Design</span> </a></li>
		<!--<li class=""><a href="#real-pkg" data-toggle="tab" aria-expanded="false">Realistic Logo Design</a></li>-->
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--illus-logo-->
		<div id="illus-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$398</strike> $199</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box two">
					  <div class="collapse-top collapsed" data-target="#two" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$598</strike> $299</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="two">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box three">
					  <div class="collapse-top collapsed" data-target="#three" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$798</strike> $399</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="three">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--mascot-logo-->
		<div id="mascot-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box four">
					  <div class="collapse-top collapsed" data-target="#four" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="four">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box five">
					  <div class="collapse-top collapsed" data-target="#five" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$700</strike> $350</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="five">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box six">
					  <div class="collapse-top collapsed" data-target="#six" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$1100</strike> $550</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="six">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50 for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--3d-logo-->
		<div id="3d-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box seven">
					  <div class="collapse-top collapsed" data-target="#seven" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="seven">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eight">
					  <div class="collapse-top collapsed" data-target="#eight" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$800</strike> $400</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eight">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box nine">
					  <div class="collapse-top collapsed" data-target="#nine" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$1200</strike> $600</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="nine">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--abs-logo-->
		<div id="abs-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box ten">
					  <div class="collapse-top collapsed" data-target="#ten" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$160</strike> $80</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="ten">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eleven">
					  <div class="collapse-top collapsed" data-target="#eleven" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$240</strike> $120</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eleven">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box tweleve">
					  <div class="collapse-top collapsed" data-target="#tweleve" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="tweleve">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--icon-logo-->
		<div id="icon-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box thirteen">
					  <div class="collapse-top collapsed" data-target="#thirteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$120</strike> $60</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="thirteen">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box forteen">
					  <div class="collapse-top collapsed" data-target="#forteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$190</strike> $95</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="forteen">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box fifteen">
					  <div class="collapse-top collapsed" data-target="#fifteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$300</strike> $150</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="fifteen">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		
		<!--flat-logo-->
		<div id="flat-pkg" class="tab-pane active">
			<div class="row mr">
			<div class="col-md-4">
					<div class="pricing-box sixteen">
					  <div class="collapse-top collapsed" data-target="#sixteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$96</strike> $48</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="sixteen">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box seventeen">
					  <div class="collapse-top collapsed" data-target="#seventeen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$180</strike> $90</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="seventeen">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eighteen">
					  <div class="collapse-top collapsed" data-target="#eighteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$240</strike> $120</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eighteen">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50 for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--real-logo--
		<div id="real-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$63</strike> $20</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					   					  <a class="order_now" href="/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$123</strike> $49</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					   					  <a class="order_now" href="/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$198</strike> $79</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					  					  <a class="order_now" href="/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>-->
	</div>
</div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="corp-iden-pkg">
                            <div class="tabs logo-pricing">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#stat-pkg" data-toggle="tab" aria-expanded="true">Stationery <span>Design</span></a></li>
        <li class=""><a href="#sm-pkg" data-toggle="tab" aria-expanded="false"> Social Media <span>Design</span></a></li>
		<li class=""><a href="#brou-pkg" data-toggle="tab" aria-expanded="false"> Brochure <span>Design</span></a></li>
		<li class=""><a href="#banner-pkg" data-toggle="tab" aria-expanded="false">Banner <span>Design</span> </a></li>
		<li class=""><a href="#promo-pkg" data-toggle="tab" aria-expanded="false">Promotional <span>Design</span></a></li>
		<li class=""><a href="#mag-pkg" data-toggle="tab" aria-expanded="false">Magazine <span>Design</span></a></li>
	</ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--stat-design-->
		<div id="stat-pkg" class="tab-pane active">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box thirtyone">
      <div class="collapse-top collapsed" data-target="#thirtyone" aria-expanded="true">
        <div class="price-box">
          <h6>$98</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Stationery Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyone">
        <div class="package-list">
          <ul>
            <li>1 Stationery Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><strike><i class="fa fa-check-circle" aria-hidden="true"></i> FREE MS Word Letterhead</strike></li>
            <li><strike><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Fax Template</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtytwo">
      <div class="collapse-top collapsed" data-target="#thirtytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$160</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Stationery Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtytwo">
        <div class="package-list">
          <ul>
            <li>2 Stationery Design Concepts</li>
            <li>2 Dedicated Designers</li>
            <li>3 Revisions Round</li>
            <li><strike>24 Hours Delivery Time</strike></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Tri-fold Brochure</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Fax Template</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtythree">
      <div class="collapse-top collapsed" data-target="#thirtythree" aria-expanded="true">
        <div class="price-box">
          <h6>$250</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Stationery Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtythree">
        <div class="package-list">
          <ul>
            <li>2 Stationery Design Concepts</li>
            <li>2 Dedicated Designers</li>
            <li>5 Revisions Round</li>
			<li><strike>24 Hours Delivery Time</strike></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 4 Pages Brochure (A4 Executive)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 T-Shirt Design</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Sticker Design</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Sticker Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--sm-design-->
		<div id="sm-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box thirtyfour">
      <div class="collapse-top collapsed" data-target="#thirtyfour" aria-expanded="true">
        <div class="price-box">
          <h6>$60</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Social Media Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyfour">
        <div class="package-list">
          <ul>
            <li>Facebook, Youtube & Twitter Pages Design</li>
            <li>1 Dedicated Logo Designer</li>
            <li>3 Rounds of Revision</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtyfive">
      <div class="collapse-top collapsed" data-target="#thirtyfive" aria-expanded="true">
        <div class="price-box">
          <h6>$98</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Social Media Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyfive">
        <div class="package-list">
          <ul>
            <li>Book Cover</li>
            <li>2 Dedicated Logo Designers</li>
            <li>5 Rounds of Revision</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Bookmark Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Icon Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Email Signature Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtysix">
      <div class="collapse-top collapsed" data-target="#thirtysix" aria-expanded="true">
        <div class="price-box">
          <h6>$150</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Social Media Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtysix">
        <div class="package-list">
          <ul>
            <li>Catalogue ( Only Theme )</li>
            <li>2 Dedicated Logo Designers</li>
            <li>Unlimited Revisions*/li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Postcard Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Product Label Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Sticker Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Poster Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Flyer Design (Single Sided)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  FREE 8 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--brou-design-->
		<div id="brou-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box thirtyseven">
      <div class="collapse-top collapsed" data-target="#thirtyseven" aria-expanded="true">
        <div class="price-box">
          <h6>$140</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Brochure Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyseven">
        <div class="package-list">
          <ul>
            <li>Tri-fold Brochure</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Bookmark Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtyeight">
      <div class="collapse-top collapsed" data-target="#thirtyeight" aria-expanded="true">
        <div class="price-box">
          <h6>$199</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Brochure Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyeight">
        <div class="package-list">
          <ul>
            <li>4 Pages Brochure</li>
            <li>2 Dedicated Logo Designers</li>
            <li>5 Rounds of Revision</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Stationery Set</br><span>(Letterhead, Envelope, Business Card)</span></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Fax Template Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 5 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
        <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtynine">
      <div class="collapse-top collapsed" data-target="#thirtynine" aria-expanded="true">
        <div class="price-box">
          <h6>$235</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Brochure Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtynine">
        <div class="package-list">
          <ul>
            <li>8 Pages Brochure</li>
            <li>2 Dedicated Logo Designers</li>
            <li>Unlimited Revisions*/li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Stationery Set</br><span>(Letterhead, Envelope, Business Card)</span></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> MS Word Letterhead</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Email Template Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Flyer (single sided) Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Promotional Mug Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  FREE 5 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--banner-design-->
		<div id="banner-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box forty">
      <div class="collapse-top collapsed" data-target="#forty" aria-expanded="true">
        <div class="price-box">
          <h6>$75</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="forty">
        <div class="package-list">
          <ul>
            <li>1 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyone">
      <div class="collapse-top collapsed" data-target="#fortyone" aria-expanded="true">
        <div class="price-box">
          <h6>$95</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyone">
        <div class="package-list">
          <ul>
            <li>2 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>4 Revisions Round</li>
            <li>24 Hours Delivery Time</li>
			     <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortytwo">
      <div class="collapse-top collapsed" data-target="#fortytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$145</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortytwo">
        <div class="package-list">
          <ul>
            <li>2 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>4 Revisions Round</li>
			<li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--promo-design-->
		<div id="promo-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortythree">
      <div class="collapse-top collapsed" data-target="#fortythree" aria-expanded="true">
        <div class="price-box">
          <h6>$89</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Promotional Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortythree">
        <div class="package-list">
          <ul>
            <li>T-Shirt</li>
            <li>1 Dedicated Logo Designer</li>
            <li>3 Rounds of Revision</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Promotional Mug Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortytwo">
      <div class="collapse-top collapsed" data-target="#fortytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$119</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Promotional Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortytwo">
        <div class="package-list">
          <ul>
            <li>Book Cover</li>
            <li>2 Dedicated Logo Designers</li>
            <li>5 Rounds of Revision</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Bookmark Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Icon Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Email Signature Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortythree">
      <div class="collapse-top collapsed" data-target="#fortythree" aria-expanded="true">
        <div class="price-box">
          <h6>$399</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Promotional Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortythree">
        <div class="package-list">
          <ul>
            <li>Catalogue ( Only Theme )</li>
            <li>2 Dedicated Logo Designers</li>
            <li>Unlimited Revisions*/li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Postcard Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Product Label Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Sticker Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Poster Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Flyer Design (Single Sided)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  FREE 8 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--Amgzine-design-->
		<div id="mag-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fifty">
      <div class="collapse-top collapsed" data-target="#fifty" aria-expanded="true">
        <div class="price-box">
          <h6>$399</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Magazine Cover Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fifty">
        <div class="package-list">
          <ul>
            <li>1 Front Cover Design</li>
            <li>1 Back Cover Design</li>
            <li>1 Table Of Content Concept </li>
            <li>Unlimited Revisions</li>
            <li>Free Color Options</li>
            <li>Free Grayscale Format</li>
            <li>Free Final Files</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftyone">
      <div class="collapse-top collapsed" data-target="#fiftyone" aria-expanded="true">
        <div class="price-box">
          <h6>$499</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> Magazine Cover Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftyone">
        <div class="package-list">
          <ul>
            <li>2 Front Cover Design</li>
            <li>2 Back Cover Design</li>
            <li>1 Table Of Content Concept</li>
            <li>Unlimited Revisions</li>
            <li>Free Color Options</li>
            <li>Free Grayscale Format</li>
            <li>Free Final Files</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftytwo">
      <div class="collapse-top collapsed" data-target="#fiftytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$599</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Magazine Cover Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftytwo">
        <div class="package-list">
          <ul>
            <li>2 Front Cover Design</li>
            <li>2 Back Cover Design</li>
            <li>2 Table Of Content Concept</li>
            <li>Unlimited Revisions</li>
            <li>Free Color Options</li>
            <li>Free Grayscale Format</li>
            <li>Free Final Files</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
	</div>
</div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="vid-anim-pkg">
                            <div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortyfour">
      <div class="collapse-top collapsed" data-target="#fortyfour" aria-expanded="true">
        <div class="price-box">
          <h6>$399</h6>
          <p>2D Video Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyfour">
        <div class="package-list">
          <ul>
            <li>1 Video Concept</li>
            <li>Up to 10 Seconds Duration</li>
            <li>Unlimited Revisions*</li>
            <li>1 Week Delivery</li>
			<li class="grad-color">$100 Additional Charges Includes</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Storyboard Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Background Music Included</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i> Voice Over Included</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyfive">
      <div class="collapse-top collapsed" data-target="#fortyfive" aria-expanded="true">
        <div class="price-box">
          <h6>$699</h6>
          <p>3D Video Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyfive">
        <div class="package-list">
          <ul>
            <li>1 Video Concept</li>
            <li>Up to 10 Seconds Duration</li>
            <li>Unlimited Revisions*</li>
            <li>2 Week Delivery</li>
			<li class="grad-color">$100 Additional Charges Includes</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Storyboard Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Background Music Included</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i> Voice Over Included</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortysix">
      <div class="collapse-top collapsed" data-target="#fortysix" aria-expanded="true">
        <div class="price-box">
          <h6>$999</h6>
          <p>Animated Video Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortysix">
        <div class="package-list">
          <ul>
            <li>1 Video Concept</li>
            <li>Up to 10 Seconds Duration</li>
            <li>Unlimited Revisions*</li>
            <li>2 Week Delivery</li>
			<li class="grad-color">$100 Additional Charges Includes</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Storyboard Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Background Music Included</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i> Voice Over Included</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                          </div>
						  <div role="tabpanel" class="tab-pane fade" id="cpy-write-pkg">
                            <div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortyseven">
      <div class="collapse-top collapsed" data-target="#fortyseven" aria-expanded="true">
        <div class="price-box">
          <h6>$500</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyseven">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 6 web pages)</li>
            <li>Copywriting (Up to 6 web pages)</li>
            <li>Article Writing (Up to 5 web pages)</li>
            <li>Blog Writing (Up to 1200 words)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> Proofreading and editing</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Re-write/Edit existing web copy (Up to 8 web pages)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyeight">
      <div class="collapse-top collapsed" data-target="#fortyeight" aria-expanded="true">
        <div class="price-box">
          <h6>$1000</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyeight">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 12 web pages)</li>
            <li>Copywriting (Up to 12 web pages)</li>
            <li>Article Writing (Up to 10 web pages)</li>
            <li>SEO Articles (6)</li>
            <li>Press Releases (3)</li>
            <li>On-line Catalogues</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i>  Proofreading and editing</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Re-write/Edit existing web copy</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Sales Letters</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortynine">
      <div class="collapse-top collapsed" data-target="#fortynine" aria-expanded="true">
        <div class="price-box">
          <h6>$1800</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortynine">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 30 web pages)</li>
            <li>Copywriting (Up to 30 web pages)</li>
            <li>Article Writing (Up to 20 web pages)</li>
            <li>Blog Writing (Up to 6000 words)</li>
            <li>SEO Articles (20)</li>
            <li>Press Releases (7)</li>
            <li>Original Web Content Copy</li>
            <li>Newsletters (2)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Re-write/Edit existing web copy</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Sales Letters</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  E-mail Marketing Letter</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  On-line Catalogues</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                          </div>
						  <div role="tabpanel" class="tab-pane fade" id="app-design-pkg">
                            <div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fifty">
      <div class="collapse-top collapsed" data-target="#fifty" aria-expanded="true">
        <div class="price-box">
          <h6>$1500</h6>
          <p><strong class="text-uppercase">Basic</strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fifty">
        <div class="package-list">
          <ul>
            <li>Upto 10 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftyone">
      <div class="collapse-top collapsed" data-target="#fiftyone" aria-expanded="true">
        <div class="price-box">
          <h6>$2000</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftyone">
        <div class="package-list">
          <ul>
            <li>Upto 20 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftytwo">
      <div class="collapse-top collapsed" data-target="#fiftytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$2500</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftytwo">
        <div class="package-list">
          <ul>
            <li>Upto 30 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                          </div>
                        </div>
                      </div>
              </div>
            </div>
            
</div>
        </section>
		<div class="clearfix"></div>
		<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
    </section>
    <?php include 'footer.php';?>