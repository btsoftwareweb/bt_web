<?php include 'header.php';?>
        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/app-design/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>App Designs</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->
			</div> <!-- /.carousel -->
        </section> <!-- /#home -->
		

    <section class="portfolio-sec">
          <img src="assets/images/client-bg.png" class="shape">
          <div class="container">
             <h1 class="wow fadeInUp">We are serving <span>2000+</span> Clients</h1>
<p class="wow fadeInUp">We create experiences that transform brands & grow businesses. We have enriched some of the biggest brands and our partnerships with them have caused the creation of some outstanding outcomes. </p>			<div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img5-hvr.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img7-hvr.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>                        
                </div>
              </div>
            </div>
           <!--<div class="text-center"><a href="/portfolio/" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL</a></div>-->          
           </div>
</section>    
<!--<section class="what-we-are-offering">
	<div class="container">
    	<div class="row">
			<div class="col-md-2"></div>
        	<div class="col-md-8 text-center">
            	<h2 class="secondary-color font-900 text-uppercase">WHAT WE ARE OFFERING?</h2>
                <p>Our apps have a cross-platform compatibility that is embedded with automated monitoring checks, layered security protection and a backup for disaster recovery management. </p>
            </div>
			<div class="col-md-2"></div>
        </div>
    	<div class="row mt-50">
        	<div class="col-md-4">
            	<div class="what-box wow fadeInLeft animated">
                	<div class="bg" style="background-image:url(..assets/images/mobile-app-dev/ios-app.jpg);"></div>
                	<h3>iOS APP</h3>
                    <p> We have a knack for novelty and have a record of providing a top-notch iPhone application development service. Ready to discuss your project with us?</p>
                    <a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeDown animated">
                	<div class="bg" style="background-image:url(..assets/images/mobile-app-dev/android-app.jpg);"></div>
                	<h3>Android App</h3>
                    <p>BT Software Design is known for its cutting edge technology services. With a professional team of Android app developers, we have the ability to maximize your business growth to a great scale. Ready to discuss your project with us?</p>
                    <a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeInRight animated">
                	<div class="bg" style="background-image:url(..assets/images/mobile-app-dev/cross-platform.jpg);"></div>
                	<h3>Cross Platform</h3>
                    <p>Experience a seamless cross platform application service backed with a layered security protection. Feel free to contact us and get a quotation for your project. </p>
                    <a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-4">
            	<div class="what-box wow fadeInLeft animated">
                	<div class="bg" style="background-image:url(..assets/images/mobile-app-dev/iot.jpg);"></div>
                	<h3>IoT</h3>
                    <p>Custom build your app idea by entrusting our expert team of mobile application developers. We have proficiency in a wide-range of platforms and can deliver what you desire. Contact us today and get your project started at the most affordable price. </p>
                    <a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeInUp animated">
                	<div class="bg" style="background-image:url(..assets/images/mobile-app-dev/mobile-games.jpg);"></div>
                	<h3>Mobile Gaming</h3>
                    <p>We are new at this, but have nevertheless successfully published two clientele mobile games with a limited scope (MVP). We are focusing towards providing premier iOS, Android mobile app games. We will help you team up with our experienced developers to build beautiful apps and awesome games for you.</p>
                    <a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeInRight animated">
                	<div class="bg" style="background-image:url(..assets/images/mobile-app-dev/ar.jpg);"></div>
                	<h3>Augmented Reality</h3>
                    <p>Convert your ideas into an AR featured application. We have years of expertise in the latest trend of Augmented Reality app development, which though does not make us the leader but still different from other app development companies. We can assure you to provide the most affordable and unmatched AR apps development solutions within your proposed time and budget.</p>
                    <a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</section>-->
<div class="clearfix"></div>
<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="#" onclick="Tawk_API.toggle();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section>
 <div class="clearfix"></div>
       
<section class="packages portfolio-sec cpy-pack" >
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">App Designs Packages We Offer</h1>
                <p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget. </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fifty">
      <div class="collapse-top collapsed" data-target="#fifty" aria-expanded="true">
        <div class="price-box">
          <h6>$1500</h6>
          <p><strong class="text-uppercase">Basic</strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fifty">
        <div class="package-list">
          <ul>
            <li>Upto 10 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="#" onclick="Tawk_API.toggle();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftyone">
      <div class="collapse-top collapsed" data-target="#fiftyone" aria-expanded="true">
        <div class="price-box">
          <h6>$2000</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftyone">
        <div class="package-list">
          <ul>
            <li>Upto 20 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="#" onclick="Tawk_API.toggle();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftytwo">
      <div class="collapse-top collapsed" data-target="#fiftytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$2500</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftytwo">
        <div class="package-list">
          <ul>
            <li>Upto 30 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="#" onclick="Tawk_API.toggle();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                       
                </div>
              </div>
            </div>
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
                <h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<!--<a href="/packages/" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>-->              </div>
            </div>
          </div>
        </section>
<div class="clearfix"></div>

        <section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="javascript:;"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <!--<div class="text-center"><a href="/blog/" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>-->
            </div>
          </div>
        </section>   

        <?php include 'footer.php';?>