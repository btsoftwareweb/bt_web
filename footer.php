 <!-- Contact Form Section
    ================================================== -->
    <?php include 'mainfolder.php';
if(isset($_POST['together_btn']))
{
    $_together_btn = 2;
}
?>
<style>
    .hidden
    {
        display:none;
    }
</style>
    <section id="contact-sec">
  <div class="container">
   <h2 class="text-center wow fadeInUp">Let’s Flaunt Some Extraordinary Projects</h2>
   <h1 class="text-center text-grad wow fadeInUp">Together</h1>
    <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="contact_home wow fadeInUp">
        <div class="conform" id="cform">
          <form name="contact" class="jform" action="order.php"  method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <div class="field">
                  <input type="text" class="required" name="order_name" placeholder="Name" maxlength="70">
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="field">
                  <input type="email" name="order_email" class="email required" placeholder="Email" maxlength="100">
                </div>
              </div>
              
               <div class="col-md-4 col-xs-12">
                <div class="field">
                  <input name="order_phone" class="required number phoneflags" id="phoneflags" type="tel" minlength="1" maxlength="13" >
                </div>
               </div> 
            </div>

            <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="field">
                        <select name="order_service" class="intrested">
                          <option value="-1">I am Interested in</option>
                          <option value="Logo Design">Logo Design</option>
                          <option value="Brand Development">Brand Development</option>
                          <option value="Web Design & Development">Web Design & Development</option>
                          <option value="App Design &amp; Development">App Design &amp; Development</option>
                          <option value="Back-End Development">Back-End Development</option>
                          <option value="Digital Marketing">Digital Marketing</option>
                          <option value="Marketing Collaterals">Marketing Collaterals</option>
                          <option value="Motion Graphics">Motion Graphics</option>
                          <option value="Website Management">Website Management</option>
                          <option value="Domain Registration">Domain Registration</option>
                          <option value="Creative Copywriting">Creative Copywriting</option>
                          <option value="2D &amp; 3D Illustration">2D &amp; 3D Illustration</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="field">
                         <input type="file" name="file" id="file" />
                      </div>
                    </div>
                  </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="field">
                    <input type="text" class="hidden" id="package" name="order_package" value="website order">
                  <textarea class="required" name="order_message" placeholder="Any Other Description" maxlength="254"></textarea>
                </div>
              </div>
            </div>
            <div class="row">
				<div class="col-md-8 col-xs-12">
					<div class="field">
						<p class="agrement">
							<label>Please send me a Non Disclosure Agreement for a Confidential Consultation
							  <input type="checkbox">
							  <span class="checkmark"></span>
							</label>
							
						</p>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 agre-buton">
					<input type="hidden" value="Footer contact form" name="sfp">
					<input type="hidden" value="index.html" name="su">
					<input type="hidden" name="ctry" value="Solomon Islands">
					<input type="hidden" value="/" name="pth">
					<input type="hidden" value="1" name="tp">
					<input type="hidden" value="2" name="ft" />
					<input type="hidden" name="ru" value="">
					<input type="submit" name="together_btn" value="SUBMIT" class="btn grad-color">
              <div class="col-md-12 col-xs-12 text-center">
							    <span style= "color:red;">
								<?php 
								if($_together_btn == 2)
								    {
								        echo "Your request has been submitted successfuly!";
								        $_together_btn = 3;
								    }
								    ?>
								    </span>
							</div>
				</div>
			</div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- End Contact Form -->

<!--Signup Modal Start-->
<div id="SignupModal" class="myFormTheme " role="dialog" tabindex="-1" aria-labelledby="SignupModalLabel" style="outline:none;">
<div class="modal-dialog animated bounceInDown">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="clearfix"></div>
<h2>Order</h2>
<p class="text-center tagline">Provide your contact information</p>
</div>
<div class="modal-body">
<div class="conform" id="popupform" >
        <form name="contact" class="jform" action="post_order.php" method="post">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="field"><i class="fa fa-user"></i>
              <input name="order_name" type="text" class="required" name="order_name" placeholder="Name" maxlength="70">
             
            </div>
          </div>
         </div>
         <div class="row">
         <div class="col-md-12 col-xs-12">
            <div class="field"><i class="fa fa-envelope"></i>
              <input name="order_email" type="text" class="required email" name="order_email" placeholder="Email"  maxlength="254">
            </div>
          </div>
         </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="field"><i class="fa fa-flag"></i>
              <select name="order_service" class="countrylist required" name="service">
                          <option value="-1">I am Interested in</option>
                          <option value="Logo Design">Logo Design</option>
                          <option value="Brand Development">Brand Development</option>
                          <option value="Web Design & Development">Web Design & Development</option>
                          <option value="App Design &amp; Development">App Design &amp; Development</option>
                          <option value="Back-End Development">Back-End Development</option>
                          <option value="Digital Marketing">Digital Marketing</option>
                          <option value="Marketing Collaterals">Marketing Collaterals</option>
                          <option value="Motion Graphics">Motion Graphics</option>
                          <option value="Website Management">Website Management</option>
                          <option value="Domain Registration">Domain Registration</option>
                          <option value="Creative Copywriting">Creative Copywriting</option>
                          <option value="2D &amp; 3D Illustration">2D &amp; 3D Illustration</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="field number"> 
              <input name="order_phone" class="required number phoneflags" id="phoneflags1" type="tel" minlength="1" maxlength="13" >
              
            </div>
          </div>
          
          <p class="hidden-xs">Phone numbers are used only to contact you about updates on your order.</p>
        </div>
        
        
        <div class="row">
          <div class="col-md-12 col-xs-12">
        
							<input type="text" class="hidden" id="package" name="order_package" value="website order">	
							<input type="submit" value="PROCEED" class="grad-color">
          </div>
        </div>
      
        </form>
        </div>

</div>

</div>
</div>
</div>
<!--Signup Modal End-->

<!--message Modal Start-->
<div id="MessageModal" class="myFormTheme " role="dialog" tabindex="-1" aria-labelledby="SignupModalLabel" style="outline:none;">
<div class="modal-dialog animated bounceInDown">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="clearfix"></div>
<h2 class="text-center">Message</h2>
<p class="text-center tagline">Send messages to the admin</p>
</div>
<div class="modal-body">
<div class="conform" id="popupform" >
        <form name="contact" class="jform" action="post_message.php" method="post">
				<div><i class="fa fa-user"></i>
						<textarea class="required" style="     width: 616px;
    margin: 0px;
    height: 146px; text-align:left;   color: #5c5c5c;" name="message" placeholder="Type Message.." ></textarea>
             
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            
          </div>
         </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
        
							<!-- <input type="text" id="package" name="package" value="website order">	 -->
            <input type="submit" value="PROCEED" class="grad-color">
						
<?php

if(isset($_SERVER['message'])) {
	
	if($_SERVER['message'] != "")
	{
	echo '<span style="text-align:center;color:red;">Message has been sent.</span>';
	$_SERVER['message'] = "";
	}
}
?>
          </div>
        </div>
      
        </form>
        </div>

</div>

</div>
</div>
</div>
<!--message Modal End-->

<!--zoom Modal Start-->
<div id="ZoomModal" class="myFormTheme " role="dialog" tabindex="-1" aria-labelledby="SignupModalLabel" style="outline:none;">
<div class="modal-dialog animated bounceInDown">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="clearfix"></div>
<h2 class="text-center">Message</h2>
<p class="text-center tagline">Send messages to the admin</p>
</div>
<div class="modal-body">
<div class="conform" id="popupform" >
        <form name="contact" class="jform" action="post_message.php" method="post">
				<div><i class="fa fa-user"></i>
						<textarea class="required" style="     width: 616px;
    margin: 0px;
    height: 146px; text-align:left;   color: #5c5c5c;" name="message" placeholder="Type Message.." ></textarea>
             
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            
          </div>
         </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
        
							<!-- <input type="text" id="package" name="package" value="website order">	 -->
            <input type="submit" value="PROCEED" class="grad-color">
						
<?php

if(isset($_SERVER['message'])) {
	
	if($_SERVER['message'] != "")
	{
	echo '<span style="text-align:center;color:red;">Message has been sent.</span>';
	$_SERVER['message'] = "";
	}
}
?>
          </div>
        </div>
      
        </form>
        </div>

</div>

</div>
</div>
</div>
<!--message Modal End-->

<!--sideform-->
<div class="sideform hidden-xs hidden-sm">
  <div class="switch"> </div>
  <div class="sidfrmdv">
    <div class="inform">
      <!--<h4>READY TO START WITH YOUR PROJECT?<span>Sign Up Now & Reserve Your Discount
</span></h4>-->
		<div class="side-logo">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/footer-logo.png">
		</div>
      <div id="sdform" class="form">
        <div id="sideform" class="formwrap">
        
        <form name="ordersideform" action="post_order.php"  class="jform" method="post">
          <div class="row">
		  <div class="col-lg-1"></div>
            <div class="col-lg-10">
              <div class="field"><i class="fa fa-user"></i>
                <input class="required" name="order_name" placeholder="Full Name" type="text" maxlength="70">
              </div>
            </div>
            
          </div>
          <div class="row">
              <div class="col-lg-1"></div>
            <div class="col-lg-10">
              <div class="field"><i class="fa fa-envelope-o"></i>
                <input class="email required"  name="order_email" placeholder="Email" type="email" maxlength="100">
              </div>
            </div>
          </div>
          <div class="row">
		    <div class="col-lg-1"></div>
                <div class="col-lg-5 pdr0">
              <div class="field"> <i class="fa fa-cog"></i>
                <select class="required" name="order_service">
                <option value="-1">Select Service</option>
                  <option value="Creative Identity" style="font-weight:bolder;background-color:#eeeeee;">Creative Identity</option>
				  <option value="Logo Design">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Logo Design</option>
                  <option value="Stationery Design">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Stationery Design</option>
                  <option value="Brochure Design">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Brochure Design</option>
                  <option value="Social Media Design">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Social Media Design</option>
                  <option value="Promotional Design">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Promotional Design</option>

                  <option value="Custom Websites" style="font-weight:bolder;background-color:#eeeeee;">Custom Websites</option>
                  <option value="Responsive Website">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Responsive Website</option>
                  <option value="CMS Website">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CMS Website</option>
                  <option value="E-Commerce Solution">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; E-Commerce Solution</option>
                  <option value="B2B & B2C Portals">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B2B & B2C Portals</option>

                  <option value="Digital Marketing" style="font-weight:bolder;background-color:#eeeeee;"><strong>Digital Marketing</strong></option>
                  <option value="Search Engine Optimization">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Search Engine Optimization</option>
                  <option value="Search Engine Marketing">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Search Engine Marketing</option>
                  <option value="Advertisement Campaigns">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Advertisement Campaigns</option>
                  <option value="Email Marketing">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email Marketing</option>
                  <option value="Social Media Marketing">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Social Media Marketing</option>
                  
                  <option value="Video Animation" style="font-weight:bolder;background-color:#eeeeee;">Video Animation</option>
                  <option value="2D Animated Video">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2D Animated Video</option>
                  <option value="3D Animated Video">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3D Animated Video</option>
                  <option value="White Board Animation">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; White Board Animation</option>
                  <option value="Stop Motion Animation">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Stop Motion Animation</option>
				  
                 <option value="Copywriting" style="font-weight:bolder;background-color:#eeeeee;">Copywriting</option>
                 <option value="Web Copywriting">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Web Copywriting</option>
                 <option value="SEO Copywriting">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SEO Copywriting</option>
                 <option value="Article Copywriting">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Article Copywriting</option>
                 <option value="Creative Writing">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Creative Writing</option>
                 <option value="Blog Writing">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Blog Writing</option>
                </select>
              </div>
              
            </div>
            <div class="col-lg-5">
              <!--<div class="field code">
                <input type="text" name="cpcode" readonly>
              </div>-->
              <div class="field number" style="padding: 0; padding-bottom: 0 !important;"> 
                <input class="required number phoneflags" id="phoneflags2" name="order_phone" type="tel" maxlength="13" minlength="1">
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="row">
          
            
            <div class="col-lg-5">
            
            
            <input type="hidden" value="Side form" name="sfp">
              <input type="hidden" value="/" name="su">
              <input type="hidden"  name="ctry">
               <input type="hidden"  name="tp" value="1">
              <input type="hidden"  name="pth" value="/">
               <input type="hidden" value="1" name="ft" />
			    <input type="hidden" name="ru" value="">
              <input type="submit" name="together_btn" value="SUBMIT" class="btn grad-color">
              <div class="col-md-12 col-xs-12 text-center">
							    <span style= "color:red;">
								<?php 
								if($_together_btn == 2)
								    {
								        echo "Your request has been submitted successfuly!";
								        $_together_btn = 3;
								    }
								    ?>
								    </span>
							</div>
              <div class="clearfix"></div>
            </div>
            
          </div>
         
        
        </form>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
</div><footer class="footer-section">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-3 wow fadeIn">
                        <h3>Top Services</h3>
                        <ul>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/logo-design.php">Logo design</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/web-development.php">Website design & development</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/digital-marketing.php">Digital Marketing(SEO)</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/app-designs.php">App Designs</a></li>
                           <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/magazine-cover-design.php">Magazine Cover Design</a></li>
                            <!--<li><a href="/corporate-identity-design/">Corporate identity</a></li>-->
                        </ul>
                    </div>

                    <div class="col-md-3 wow fadeIn" data-wow-delay="0.3s">
						<h3>Best Services</h3>
                        <ul>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/video-animation.php">Video Animation</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/mobile-apps-development.php">Mobile App development</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/web-development.php">Web app development</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/software-development.php">Software Development</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/copy-writing.php">Copywriting</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3 wow fadeIn" data-wow-delay="0.5s">
                        <h3>Packages</h3>
                        <ul>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/logo-design.php">Logo design Package</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/web-development.php">Website design & development Package</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/video-animation.php">Video Animation Package</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/mobile-apps-development.php">App Development Package</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3 wow fadeIn" data-wow-delay="0.7s">
                        <h3>About Company</h3>
                        <ul>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/about.php">About us</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/contact-us.php">Contact us</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/terms-of-use.php"> Terms of use</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/privacy-policy.php">Privacy Policy</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/refund-policy.php">Refund Policy</a></li>
                            <!--<li><a href="#">FAQS</a></li>-->
                        </ul>
                    </div>

                </div>

                <div class="row text-center mt-100">
                    <div class="col-xs-12">
                        <div class="footer-logo">
                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/index.php"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/footer-logo.png" alt=""></a>
												
                        </div>
<div class="social-media-links">
    <ul>
        <li><a href="https://www.facebook.com/BT-Software-Development-and-AI-Technology-617693511994387/?modal=admin_todo_tour"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
        <li><a href="https://www.instagram.com/bt_software/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li><a href="https://www.pinterest.com/btsoftwareAI/"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
        <li><a href="https://twitter.com/SoftwareBt?s=08"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
    </ul>
</div>

                        <div class="copyright">
                            <p>Copyright @ 2019 <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/index.php">Bt software</a> All Right Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
            <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/footer-shape.png" class="shape footer-shape">
        </footer>



        <a class="page-scroll backToTop" href="#page-top"><i class="fa fa-angle-up"></i></a>

        <!-- Preloader -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
		
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-127216449-2', 'auto');
      ga('send', 'pageview');
    </script>
        <!-- jQuery -->
	
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery-2.1.3.min.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.easing.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.sticky.min.js"></script>
        
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.stellar.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.inview.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/wow.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.countTo.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.shuffle.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.BlackAndWhite.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/magnific-popup/jquery.magnific-popup.min.js"></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets//js/scripts.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/formvalidation.js"></script>
        <script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/jquery.fancybox.pack.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/particles.js"></script>
		<!--<script src="http://threejs.org/examples/js/libs/stats.min.js"></script>-->
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/app.js"></script>
		
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/particle-color.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/layer.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/isotope.min.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/ouibounce.js"></script>
		<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/form-wizard.js"></script>
		
		
		
		

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.1.0/dist/jquery.countdown.min.js"></script>

<script src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/intlTelInput.js"></script>
  <script>
    var input = document.querySelector("#phoneflags");
    window.intlTelInput(input, {
      utilsScript: "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/utils.js",
    });
    
     var input = document.querySelector("#phoneflags1");
    window.intlTelInput(input, {
      utilsScript: "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/utils.js",
    });
    
     var input = document.querySelector("#phoneflags2");
    window.intlTelInput(input, {
      utilsScript: "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/js/utils.js",
    });
    
     
    
  </script>


<script>

      // if you want to use the 'fire' or 'disable' fn,
      // you need to save OuiBounce to an object
      var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'), {
        aggressive: true,
        timer: 0,
        callback: function() { console.log('ouibounce fired!'); }
      });

      $('body').on('click', function() {
        $('#ouibounce-modal').hide();
      });

      $('#ouibounce-modal .modal-footer').on('click', function() {
        $('#ouibounce-modal').hide();
      });

      $('#ouibounce-modal .modal').on('click', function(e) {
        e.stopPropagation();
      });
    </script>

        <script type="text/javascript">
            function vidplay() {
              var video = document.getElementById("videoPlayer");
              var button = document.getElementById("playbtn");
              if (video.paused) {
                video.play();
                button.className = "pause"
              } else {
                video.pause();
                button.className = "play"
              }
            }
        </script>

  <script type="text/javascript">
      var labels = ['wks', 'days', 'hrs', 'mins', 'secs'],
      nextYear = (new Date('Feb 15, 2019 10:00:00')),
      template = _.template('<div class="time <%= label %>"><span class="count curr top"><%= curr %></span><span class="count next top"><%= next %></span><span class="count next bottom"><%= next %></span><span class="count curr bottom"><%= curr %></span><span class="label"><%= label.length < 6 ? label : label.substr(0, 3)  %></span></div>'),
      currDate = '00:00:00:00:00',
      nextDate = '00:00:00:00:00',
      parser = /([0-9]{2})/gi,
      $example = $('#main-example');
    // Parse countdown string to an object
    function strfobj(str) {
      var parsed = str.match(parser),obj = {};
      labels.forEach(function(label, i) {
        obj[label] = parsed[i]
      });
      return obj;
    }
    // Return the time components that diffs
    function diff(obj1, obj2) {
      var diff = [];
      labels.forEach(function(key) {
        if (obj1[key] !== obj2[key]) {
          diff.push(key);
        }
      });
      return diff;
    }
    // Build the layout
    var initData = strfobj(currDate);
    labels.forEach(function(label, i) {
      $example.append(template({
        curr: initData[label],
        next: initData[label],
        label: label
      }));
    });
    // Starts the countdown
    $example.countdown(nextYear, function(event) {
      var newDate = event.strftime('%w:%d:%H:%M:%S'),data;
      if (newDate !== nextDate) {
        currDate = nextDate;
        nextDate = newDate;
        // Setup the data
        data = {
          'curr': strfobj(currDate),
          'next': strfobj(nextDate)
        };
        // Apply the new values to each node that changed
        diff(data.curr, data.next).forEach(function(label) {
          var selector = '.%s'.replace(/%s/, label),
            $node = $example.find(selector);
          // Update the node
          $node.removeClass('flip');
          $node.find('.curr').text(data.curr[label]);
          $node.find('.next').text(data.next[label]);
          // Wait for a repaint to then flip
          _.delay(function($node) {
            $node.addClass('flip');
          }, 50, $node);
        });
      }
    });
</script>

<!--Step Form JS Start-->



<script type="text/javascript">
			$('#classic').click(function() {
			    $('.form-wizard').addClass("form-header-classic").removeClass("form-header-stylist form-header-modarn");
			});

			$('#modarn').click(function() {
			    $('.form-wizard').addClass("form-header-modarn").removeClass("form-header-classic form-header-stylist");
			});

			$('#stylist').click(function() {
			    $('.form-wizard').addClass("form-header-stylist").removeClass("form-header-classic form-header-modarn");
			});
</script>
		
		
<script type="text/javascript">
			$('#classic-body').click(function() {
			    $('.form-wizard').addClass("form-body-classic").removeClass("form-body-stylist form-body-material");
			});

			$('#material-body').click(function() {
			    $('.form-wizard').addClass("form-body-material").removeClass("form-body-classic form-body-stylist");
			});

			$('#stylist-body').click(function() {
			    $('.form-wizard').addClass("form-body-stylist").removeClass("form-body-classic form-body-material");
			});
</script>
<!--Step Form JS End-->



<script>
    document.getElementById('vid').play();
</script>
         <!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+971 55 888 9544", // WhatsApp number
            email: "tahiralain@gmail.com", // Email
            call_to_action: "Message us", // Call to action
            button_color: "#129BF4", // Color of button
            position: "left", // Position may be 'right' or 'left'
            order: "whatsapp,email", // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->

<!-- Start of btsoftware Zendesk Widget script -->
<!--<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=5d73f985-5ce1-477e-bd5f-d8a0117aa3f2"> </script>-->
<!-- End of btsoftware Zendesk Widget script -->
 
<script>
    var uploadField = document.getElementById("file");

uploadField.onchange = function() {
    if(this.files[0].size > 2097152){
       alert("File is too big!");
       this.value = "";
    };
};

</script>

<script>

        /*======== Isotope Filter Script =========*/

        var myTheme = window.myTheme || {},
        $win = $(window);

        myTheme.Isotope = function () {

            // 4 column layout
            var isotopeContainer = $(".isotopeContainer");
            if (!isotopeContainer.length || !jQuery().isotope) return;
            $win.load(function () {
                var $selected = $('.isotope-filter_links #default-filter');
                var filterValue = $selected.attr("data-filter");
                $(".isotope-filter_links ul li").find(".active").removeClass("active");
                $selected.addClass("active");
                isotopeContainer.isotope({
                    filter: filterValue,
                    itemSelector: ".isotopeSelector"
                });
                
                $(".isotope-filter_links").on("click", "a", function (e) {
                    $(".isotope-filter_links ul li").find(".active").removeClass("active");
                    $(this).addClass("active");
                    var filterValue = $(this).attr("data-filter");
                    isotopeContainer.isotope({ filter: filterValue });
                    e.preventDefault();
                });
            });

        };

        myTheme.Isotope();

        /* End Isotope Filter */

$( document ).ready(function() {
	$(function() {
    $("img.lazy").lazyload();
});
	
    $('.SeeMore').click(function(){
		var $this = $(this);
		$this.toggleClass('SeeMore');
		if($this.hasClass('SeeMore')){
			$this.text('Read More');			
		} else {
			$this.text('Read Less');
		}
	});
});
// $( document ).ready(function() {
//     setTimeout(function(){
//         $('.isotope-filter_links #default-filter').click();
//     }, 1000);
// });
</script>



    </body>
  
</html>