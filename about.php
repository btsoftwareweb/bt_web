<?php include 'header.php';?>

        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/about-slider-bg.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>About us</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="about-mission">
<div class="container">
<div class="row">
	<div class="col-md-3">
		<div class="story wow fadeInLeft animated">
			<img src="assets/images/mission-icon.png">
			<h1 class="text-grad">Our Mission</h1>
			<p>Our primary mission is to meet the demands of ever-increasing intelligent tech savvy audience and to help our clients build meaningful brands. We aim to provide high quality, professional and creative designs and web-development services.</p>
		</div>
	</div>
	<div class="col-md-6">
		<div class="img_holder wow fadeInUp animated">
			<img src="assets/images/about-mission-img.png">
		</div>
	</div>
	<div class="col-md-3">
		<div class="story wow fadeInRight animated">
			<img src="assets/images/story-icon.png">
			<h1 class="text-grad">Our Story</h1>
			<p>After years of being in the industry – we take pride in our work and strive to provide our services in a professional way. BT Software is always up for new business or challenges.  </p>
		</div>
	</div>
</div>
</div>
</section>
<div class="clearfix"></div>
<section class="about-us">
<!--<img src="assets/images/about-us-bg.png" class="about-img wow fadeInRight animated">-->
<div class="container">
<div class="row">
	<div class="col-md-6">
		<div class="about-text wow fadeInLeft animated">
			<h1>About us</h1>
			<p>BT Software is a boutique of creative services offering multiple business solutions under one roof. We have a team of professionals and our goal is to provide critical solutions to our clients in establishing their business presence. At BT Software, everything we create is human-centered and focused on helping people accomplish tasks as simply as possible. We can handle all your digital needs and deliver them with the personal service and customization that you deserve. Our development methodology promotes a result-driven interactive approach and guarantees better customer service in terms of quality and cost. </p>
			<p>Our aim is to assist our clients and the success of their business at all levels. As a Company, BT Software’s core values shape the employee-ownership culture and inspire our common mission statement.</p>
		</div>
	</div>
	<div class="col-md-6">
		<div class="img_holder wow fadeInRight animated">
			<img src="assets/images/about-left.png">
		</div>
	</div>
</div>
</div>
</section>
<div class="clearfix"></div>

<section class="strategy">
<img src="assets/images/develop-right.png" class="develop wow fadeInRight animated">
<div class="container">
<div class="row">
	<div class="col-md-6">
		<div class="product">
			<h3 class="text-grad" >We develop digital strategies, products and services.</h3>
			<div class="progress-content mt-30">
                	<div class="row">
                    	<div class="col-md-6">
                        	<h4>BRANDING</h4>
                        </div>
                        <div class="col-md-6">
                        	<h4 class="pull-right">89%</h4>
                        </div>
                    </div>
                    <div class="progress">
						<div class="progress-bar six-sec-ease-in-out grad-color" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
              </div>
			  <div class="progress-content mt-30">
                	<div class="row">
                    	<div class="col-md-6">
                        	<h4>DEVELOPMENT</h4>
                        </div>
                        <div class="col-md-6">
                        	<h4 class="pull-right">82%</h4>
                        </div>
                    </div>
                    <div class="progress">
						<div class="progress-bar six-sec-ease-in-out grad-color" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
              </div>
			  <div class="progress-content mt-30">
                	<div class="row">
                    	<div class="col-md-6">
                        	<h4>MARKETING</h4>
                        </div>
                        <div class="col-md-6">
                        	<h4 class="pull-right">59%</h4>
                        </div>
                    </div>
                    <div class="progress">
						<div class="progress-bar six-sec-ease-in-out grad-color" role="progressbar" aria-valuenow="59" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
              </div>
			  <div class="progress-content mt-30">
                	<div class="row">
                    	<div class="col-md-6">
                        	<h4>seo</h4>
                        </div>
                        <div class="col-md-6">
                        	<h4 class="pull-right">62%</h4>
                        </div>
                    </div>
                    <div class="progress">
						<div class="progress-bar six-sec-ease-in-out grad-color" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
              </div>
		</div>
	</div>
	<div class="col-md-6"></div>
</div>
</div>
</section>
<div class="clearfix"></div>

<section class="about-why">
<div class="container">
<div class="row">
	<div class="col-md-12">
		<div class="title">
			<h1 class="wow fadeInUp animated">Why choose Us</h1>
		</div>
	</div>
	<div class="col-md-4">
		<div class="customer wow fadeInLeft animated">
			<img src="assets/images/check-sign.png">
			<h1>1</h1>
			<h4>Customer Center</h4>
			<p>We are a client-centric company. For us – customers comes first. We believe in building long-term relationships with our clients, because your success is our success.</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="customer wow fadeInDown animated">
			<img src="assets/images/check-sign.png">
			<h1>2</h1>
			<h4>24/7 Support</h4>
			<p>Sit back and relax.We make sure that your project is going smoothly. A dedicated agent will be there 24/7 to assist all your queries. </p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="customer wow fadeInRight animated">
			<img src="assets/images/check-sign.png">
			<h1>3</h1>
			<h4>Best Quality</h4>
			<p>Quality is our backbone. Our quality-minded staff will take care of your business projects in the most efficient manner. </p>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-4">
		<div class="customer wow fadeInLeft animated">
			<img src="assets/images/check-sign.png">
			<h1>4</h1>
			<h4>100% Satisfection</h4>
			<p>Customer satisfaction is our first priority. We give our 100% efforts to satisfy our clients. </p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="customer wow fadeInUp animated">
			<img src="assets/images/check-sign.png">
			<h1>5</h1>
			<h4>Most Innovative</h4>
			<p>BT Software fosters a corporate climate that is open to new technologies, ideas and opportunities. We encourage innovation in our employees and reward them for bespoke ideas and performance that provides best-value to our customers and their businesses.</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="customer wow fadeInRight animated">
			<img src="assets/images/check-sign.png">
			<h1>6</h1>
			<h4>Cost Effective</h4>
			<p>We are committed to providing affordable, well-managed and creative IT solutions across all of your designs and digital projects.</p>
		</div>
	</div>
</div>
</div>
</section>
<div class="clearfix"></div>
<?php include 'footer.php';?>