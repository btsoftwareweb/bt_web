<?php include 'header.php';?>
        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/logo-design/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Logo Design</span></h1>
					  <h3>
                           Whizz with our trendy yet classic logo design services
                        </h3>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->
			</div> <!-- /.carousel -->
        </section> <!-- /#home -->
		
<!-- LOGO TYPES
    ================================================== -->

    <section id="who-we-are">
        <div class="container">
            <div class="row top-heading_row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="section_title">
                        <h2>
                            LOGO TYPES
                        </h2>
                        
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row tabs_row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" href="#tab1">Iconic <span>Design</span></a></li>
                        <li><a data-toggle="tab" href="#tab2">Illustrative <span>Design</span></a></li>
                        <li><a data-toggle="tab" href="#tab3">Wordmark <span>Design</span></a></li>
                        <li><a data-toggle="tab" href="#tab4">3D <span>Design</span></a></li>
                        <li><a data-toggle="tab" href="#tab5">Abstract <span>Design</span></a></li>
                        <li><a data-toggle="tab" href="#tab6">Flat <span>Design</span></a></li>
                        <li><a data-toggle="tab" href="#tab7">Realistic <span>Design</span></a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/logo-design/site-img8.png" class="img-responsive center-block lazy" alt="image" style="margin-top:10px;" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                            Iconic Design
                                        </h3>
                                        <p>
                                           Our experts create iconic logo designs that are versatile and memorable. Our iconic logos will help you boost your brand affinity. It will reflect your brand culture and goals. Iconic logos are self-explanatory and team <span class="text-grad">BT Software</span> love to experiment with new techniques and color coding so that your logo becomes a talking symbol of your brand. </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/logo-design/computer.png" alt="image" class="img-responsive center-block lazy" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                           Illustrative Design
                                        </h3>
                                        <p>
                                           If you want to create a work of art – illustrative logos are the best fit as it contains intricate artwork. It will act as a trendy as well as contemporary image of your brand and keep the viewers attention longer than the traditional logo design. We at <span class="text-grad">BT Software  Design Agency</span> use latest tools and technology to create a remarkable illustrative logo for your brand. </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab3" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/logo-design/site-img9.png" alt="image" class="img-responsive center-block lazy" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                           Wordmark Design
                                        </h3>
                                        <p>
                                          Create your professional wordmark logo with <span class="text-grad">BT Software Design Agency</span>. It is a pure form of logo and a text-only representation of your brand – which makes it a good choice for start-ups and accelerating businesses that are seeking a static and no-hassle solution. We allow full customization to create your own wordmark logo design to enhance your brand visibility.  
                                        </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab4" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/logo-design/site-img10.png" alt="image" class="img-responsive center-block lazy" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                           3D Design
                                        </h3>
                                        <p>
                                           If you’re looking for a modern style logo – our 3-Dimensional logo designs will serve you best. We specialize in making 3D logos which reflects your brand in an attractive way. With our abundant 3D logo designs – enjoy the rewards of attracting a large number of clients as your audience will be able to experience look and feel of your business. Ping us now and let’s add some dimensions to your boring logo designs. 
                                        </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab5" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
									<img src="assets/images/logo-design/site-img17.png" alt="image" class="img-responsive center-block lazy" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                           Abstract Design
                                        </h3>
                                        <p>
                                           Abstract logo designs also known as smart logos is a mix of pictures, typography and shapes. A lot of organizations today use abstract logos for their businesses. They are usually used as a conceptual strategy to represent your business idea. It also allows you to exhibit different functions of your company. Join hands with <span class="text-grad">BT Software</span> to create your abstract logo that reflects an unclear vision of your business – so that you can play safe in your business operations.
                                        </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab6" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/logo-design/site-img11.png" alt="image" class="img-responsive center-block lazy" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                           Flat Design
                                        </h3>
                                        <p>
                                          Flat design or a minimalistic logo design is a clean and crisp image bearing bright colors and two-dimensional illustrations. <span class="text-grad">BT Software</span> helps you create a simple, yet innovative flat logo deigns that are classic and cluster free. We make sure that your flat designs are a combination of solid, vivid colors, concise text and to-the-point. 
                                        </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab7" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="assets/images/logo-design/site-img12.png" alt="image" class="img-responsive center-block lazy" />
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-contents">
                                        <h3>
                                            Realistic Design
                                        </h3>
                                        <p>
                                           Realistic designs are the enchanted graphical representations of your business. We help you create a logo design that incorporates your brand roots. Whether you are a start up, medium enterprise or a large-scale business - Realistic logos are for everyone who wishes to build a strong brand image. 
                                        </p>
                                        <a href="#" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End LOGO TYPES -->
        

        <section class="portfolio-sec">
          <img src="assets/images/client-bg.png" class="shape">
          <div class="container">
             <h1 class="wow fadeInUp">We are serving <span>2000+</span> Clients</h1>
<p class="wow fadeInUp">We create experiences that transform brands & grow businesses. We have enriched some of the biggest brands and our partnerships with them have caused the creation of some outstanding outcomes. </p>			<div class="row">
              <div class="col-md-12">
                <div class="mission-tab">

                        <!-- Nav tabs -->
                        <!--<ul class="nav nav-tabs wow fadeInUp" role="tablist">
                          <li role="presentation" class="active"><a href="#logo" role="tab" data-toggle="tab">Logo</a></li>
                          <li role="presentation"><a href="#website" role="tab" data-toggle="tab">Website</a></li>
                          <li role="presentation"><a href="#mob-app" role="tab" data-toggle="tab">Mobile App</a></li>
                          <li role="presentation"><a href="#vid-anim" role="tab" data-toggle="tab">Video Animation</a></li>
                        </ul>-->
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane fade in active" id="logo">
                            <div class="tabs logo logo-portfolio">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#illus-logo" data-toggle="tab" aria-expanded="false"> Illustrative <span>Design</span></a></li>
		<li class=""><a href="#mascot-logo" data-toggle="tab" aria-expanded="false"> Mascot <span>Design</span></a></li>
		<!--<li class=""><a href="#3d-logo" data-toggle="tab" aria-expanded="false">3D Design </a></li>-->
		<li class=""><a href="#abs-logo" data-toggle="tab" aria-expanded="false">Abstract <span>Design</span></a></li>
		<li class=""><a href="#icon-logo" data-toggle="tab" aria-expanded="true">Iconic <span>Design</span></a></li>
		<li class=""><a href="#flat-logo" data-toggle="tab" aria-expanded="false">Flat <span>Design</span> </a></li>
		<!--<li class=""><a href="#real-logo" data-toggle="tab" aria-expanded="false">Realistic Logo Design</a></li>-->
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		
		<!--illus-logo-->
		<div id="illus-logo" class="tab-pane active">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6 ">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/2-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/3-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/8-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/10.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/10.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/11.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/11.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/12.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/12.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/13.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/13.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/14.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/14.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/15.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/15.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/16.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/16.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/17.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/17.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/18.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/18.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--mascot-logo-->
		<div id="mascot-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/10.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/10.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/11.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/11.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/12.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/12.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--3d-logo
		<div id="3d-logo" class="tab-pane">
			<div class="row mr">
				
			</div>
        </div>
		<!--abs-logo-->
		<div id="abs-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/1-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/2-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/4-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--icon-logo-->
		<div id="icon-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		
		<!--flat-logo-->
		<div id="flat-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
	</div>
</div>
                          </div>
                        </div>
                      </div>
              </div>
            </div>
            <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL</a></div>          </div>
        </section>

        
		<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section>        

        <section class="packages portfolio-sec">
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">Logo Packages We Offer</h1>
                <p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget. </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab ">
					<div class="tabs logo-pricing">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class=""><a href="#illus-pkg" data-toggle="tab" aria-expanded="false"> Illustrative <span>Design</span></a></li>
		<li class=""><a href="#mascot-pkg" data-toggle="tab" aria-expanded="false"> Mascot <span>Design</span></a></li>
		<li class=""><a href="#3d-pkg" data-toggle="tab" aria-expanded="false">3D <span>Design</span> </a></li>
		<li class=""><a href="#abs-pkg" data-toggle="tab" aria-expanded="false">Abstract <span>Design</span></a></li>
		<li class=""><a href="#icon-pkg" data-toggle="tab" aria-expanded="true">Iconic <span>Design</span></a></li>
		<li class="active"><a href="#flat-pkg" data-toggle="tab" aria-expanded="false">Flat <span>Design</span> </a></li>
		<!--<li class=""><a href="#real-pkg" data-toggle="tab" aria-expanded="false">Realistic Logo Design</a></li>-->
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--illus-logo-->
		<div id="illus-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$398</strike> $199</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box two">
					  <div class="collapse-top collapsed" data-target="#two" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$598</strike> $299</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="two">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box three">
					  <div class="collapse-top collapsed" data-target="#three" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$798</strike> $399</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="three">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--mascot-logo-->
		<div id="mascot-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box four">
					  <div class="collapse-top collapsed" data-target="#four" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="four">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box five">
					  <div class="collapse-top collapsed" data-target="#five" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$700</strike> $350</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="five">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box six">
					  <div class="collapse-top collapsed" data-target="#six" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$1100</strike> $550</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="six">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50 for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--3d-logo-->
		<div id="3d-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box seven">
					  <div class="collapse-top collapsed" data-target="#seven" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="seven">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eight">
					  <div class="collapse-top collapsed" data-target="#eight" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$800</strike> $400</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eight">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box nine">
					  <div class="collapse-top collapsed" data-target="#nine" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$1200</strike> $600</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="nine">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--abs-logo-->
		<div id="abs-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box ten">
					  <div class="collapse-top collapsed" data-target="#ten" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$160</strike> $80</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="ten">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eleven">
					  <div class="collapse-top collapsed" data-target="#eleven" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$240</strike> $120</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eleven">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box tweleve">
					  <div class="collapse-top collapsed" data-target="#tweleve" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="tweleve">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--icon-logo-->
		<div id="icon-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box thirteen">
					  <div class="collapse-top collapsed" data-target="#thirteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$120</strike> $60</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="thirteen">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box forteen">
					  <div class="collapse-top collapsed" data-target="#forteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$190</strike> $95</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="forteen">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box fifteen">
					  <div class="collapse-top collapsed" data-target="#fifteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$300</strike> $150</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="fifteen">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		
		<!--flat-logo-->
		<div id="flat-pkg" class="tab-pane active">
			<div class="row mr">
			<div class="col-md-4">
					<div class="pricing-box sixteen">
					  <div class="collapse-top collapsed" data-target="#sixteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$96</strike> $48</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="sixteen">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box seventeen">
					  <div class="collapse-top collapsed" data-target="#seventeen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$180</strike> $90</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="seventeen">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eighteen">
					  <div class="collapse-top collapsed" data-target="#eighteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$240</strike> $120</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eighteen">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50 for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--real-logo--
		<div id="real-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$63</strike> $20</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					   					  <a class="order_now" href="/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$123</strike> $49</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					   					  <a class="order_now" href="/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$198</strike> $79</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					  					  <a class="order_now" href="/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>-->
	</div>
</div>
                       
                </div>
              </div>
            </div>
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
               <h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>              </div>
            </div>
          </div>
        </section>

        <div class="clearfix"></div>

        <section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>
         <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="javascript:;"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
				</section>
				
				<?php include 'footer.php';?>