-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 19, 2019 at 03:02 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `btsoftware_btsoftware`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `message`) VALUES
(5, 'hello please call me'),
(4, 'HI'),
(6, 'hello please call me'),
(7, 'hello chat with us'),
(8, 'hi'),
(9, 'hitesting');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `package` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(4) NOT NULL,
  `content` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `service`, `phone`, `package`, `create_date`, `is_active`, `content`) VALUES
(2, 'sameer', 'sameer@email.com', 'option 2', '231213', 'Basic ', '2019-03-01 14:07:08', 1, ''),
(3, 'sameer', 'sameer1122@email.com', 'option 2', '231213', 'website order', '2019-03-05 10:12:51', 1, ''),
(4, 'sameer', 'sameer@email.com', 'option 2', '231213', 'website order', '2019-03-05 10:21:39', 0, ''),
(5, 'sameerasd', 'sameer@email.com', 'option 2', '231213', 'website order', '2019-03-05 10:22:24', 1, ''),
(6, 'Muhammad Tahir', 'tahiralain@gmail.com', 'option 2', '971558889544', 'website order', '2019-03-05 19:48:05', 1, ''),
(7, '123321', '123321@gmail.com', 'Motion Graphics', '123321312', '', '2019-03-11 12:22:36', 1, ''),
(8, 'test', 'test12321@gmail.com.com', 'Digital Marketing', '12321', 'website order', '2019-03-11 13:28:24', 0, 'all-recipes-temp.txt'),
(9, 'asdsada', '123321@gmail.com', '-1', '21321', 'website order', '2019-03-11 13:37:11', 0, 'alls-recipes-temp.txt'),
(10, '1231221321', 'asdsadas@gmail.com', 'Back-End Development', '1232132132', 'website order', '2019-03-11 13:39:28', 0, 'allsa-recipes-temp.txt'),
(11, 'Muhammad Tahir', 'tahiralain@gmail.com', 'Logo Design', '971558889544', 'website order', '2019-03-12 21:14:00', 0, ''),
(12, 'sssdfdsaf', 'testtalha@test.com', 'Web Design & Development', '34534534534', 'website order', '2019-03-14 11:45:45', 0, ''),
(13, 'muhammad', 'tahiralain@gmail.com', 'Brand Development', '558889544', 'website order', '2019-03-15 13:42:29', 0, ''),
(14, 'testab', 'testab@asdf.com', 'Brand Development', '2222222222222', 'website order', '2019-03-15 13:45:31', 0, 'strategy.png'),
(15, 'dsfgfhjh', 'sadgsefea@gmail.com', 'Domain Registration', '212156445484', 'website order', '2019-03-25 12:37:03', 0, 'DSP-8200.jpg'),
(16, 'hhhh', 'hhhhg@gmail.com', 'Domain Registration', '32465', 'website order', '2019-03-25 12:38:14', 1, 'KPC-1570-1770 Series.jpg'),
(17, 'testing-1', 'testing@gmail.com', 'Motion Graphics', '767867878678', 'website order', '2019-03-25 13:05:50', 0, 'Doc1.docx'),
(18, 'shahrukh', 'calldakaar@gmail.com', 'Digital Marketing', '03333638887', 'website order', '2019-03-25 14:36:43', 0, 'Wow Electronic Luxury withoutbackground.png'),
(19, 'Muhammad Tahir', 'tahiralain@gmail.com', '-1', '971558889544', 'website order', '2019-03-25 15:29:32', 0, 'wow Terms of Service.docx'),
(20, 'lol', 'lol@gmail.com', '-1', '2321313132132', 'website order', '2019-03-25 15:39:42', 0, 'Wow Electronic Luxury withoutbackground.png'),
(21, 'Muhammad Tahir', 'tahiralain@gmail.com', 'Back-End Development', '971558889544', 'website order', '2019-03-27 10:35:49', 1, 'wow Terms of Service.docx'),
(22, 'Sameer Ul Haq', 'sa@gmail.com', 'Search Engine Marketing', '03412055848', '', '2019-04-16 13:26:52', 0, ''),
(23, 'Muhammad Tahir', 'tahiralain@gmail.com', 'Brand Development', '971558889544', 'website order', '2019-04-16 14:16:40', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_active`) VALUES
(1, 'sameer', 'email@gmail.com', '123', 1),
(2, 'admin', 'admin@gmail.com', '123', 1),
(3, 'name', 'dsa', 'das', 1),
(4, 'sameer', 'sameer.ulhaq79@gmail.com', '12348', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
