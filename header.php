<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="portfolio, corporate, business, parallax, creative, agency">
        <meta name="author" content="trendytheme.net">

        <title>Bt software Design Agency</title>
<?php include 'mainfolder.php' ?>
        <!--  favicon -->
        <link rel="shortcut icon" href="<?php echo $_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/ico/favicon.png">
		<link href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900" rel="stylesheet">
        <!-- animate CSS -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/animate.css" rel="stylesheet">
        <!-- FontAwesome CSS -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Flat Icon CSS -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/fonts/flaticon/flaticon.css" rel="stylesheet">
        <!-- magnific-popup -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/magnific-popup/magnific-popup.css" rel="stylesheet">
        <!-- owl.carousel -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/owl-carousel/owl.theme.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Style CSS -->
		 <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/mobile-nav.css" rel="stylesheet" />
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/style.css" rel="stylesheet">
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/style-2.css" rel="stylesheet">
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/dodecahedron.css" rel="stylesheet">
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/submarine.css" rel="stylesheet">
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/lib.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/responsive.css" rel="stylesheet">

        <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/jquery.mCustomScrollbar.min.css" rel="stylesheet">
		 <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/ionicons.min.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/jquery.fancybox.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/travel-circle.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/particle.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/layer.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/ouibounce.min.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/form-wizard.css" rel="stylesheet" />
		  
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/intlTelInput.css" rel="stylesheet" />
		  <link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/css/demo.css" rel="stylesheet" />
		  
    <body id="page-top" class="home-main">
<style>
    .top-bar ul li
    {
        margin:unset !important;
    }
</style>
  
<header>
  <div class="top-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-12 pull-right text-right">
          <ul>
            <li>Call Now : <a href="tel:+19174750802">+191-74750802</a></li>
            <li>Email Now : <a href="mailto:info@btsoftwarehouse.com">info@btsoftwarehouse.com</a></li>
            <!--<li class="chat"><a href="#" onclick="Tawk_API.toggle();">Chat With Us</a></li>-->
              <li class="chat"><a href="https://calendly.com/btsoftware">Schedule a Meeting</a></li>
               <li class="chat"><a href="javascript:;" data-toggle="modal" data-target="#ZoomModal">Chat With Us</a> </li>
            <li class="chat last"><a href="javascript:;" data-toggle="modal" data-target="#MessageModal">Send Message</a> </li>
          </ul>
        </div>
      </div>
    </div> 
  </div>

<!-- Navigation start -->
        <nav class="navbar navbar-custom tt-default-nav" role="navigation">
            <div class="container">

              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="index.php"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/logo.png" alt=""></a>
                <a class="navbar-brand sticky sti-logo" href="index.php"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/footer-logo.png" alt=""></a>
                <a class="navbar-brand sticky sti-logo1" href="index.php"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/logo.png" alt=""></a>
              </div>
          
              <div class="collapse navbar-collapse" id="custom-collapse">
          
                <ul class="nav navbar-nav navbar-right">
          
                  <li class="tab-home"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/index.php">Home</a></li>
                  <li class="tab-about"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/about.php">About us</a></li>
					<li class="dropdown tab-services">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Services</a>
                        <ul class="dropdown-menu">
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/logo-design.php">Logo Design</a></li>
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/web-development.php">Web Development</a></li>
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/mobile-apps-development.php">Mobile App Development</a></li>
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/software-development.php">Software Development</a></li>
							<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Branding</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/stationery-design.php">Stationery Design</a></li>
									<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/social-media-design.php">Social Media Design</a></li>
									<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/brochure-design.php">Brochure Design</a></li>
									<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/banner-design.php">Banner Design</a></li>
									<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/magazine-cover-design.php">Magazine Cover Design</a></li>
									<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/promotional-design.php">Promotional Design</a></li>
                                </ul>
                            </li>
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/app-designs.php">App Designs</a></li>
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/digital-marketing.php">Digital Marketing</a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/video-animation.php">Video Animation</a></li>
							<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/copy-writing.php">Copywriting</a></li>
                        </ul>
                    </li>
                  <li class="tab-packages"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php">Pricing</a></li>
                  <li class="tab-portfolio"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php">Portfolio</a></li>
                  <li class="tab-blog"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php">Blog</a></li>
                  <li class="tab-contact-us"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/career.php">Career</a></li>
                  <li class="tab-contact-us"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/contact-us.php">Contact</a></li>
                </ul>
              </div>
            </div><!-- /.container -->
			
			
			
			
			
			
			
			
			<div class="navicon wow animated" style="visibility: visible;"> <a class="nav-toggle" href="#"><span></span></a> </div>
		<div class="header-nav" >
              <div class="container">
				<div id="particles-js">
                <div class="logo-inside">
					<a href="/" title="Bt software Design Agency">
						<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/footer-logo.png">
					</a>
				</div>
               
                <nav>
                 <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 featur-main">
                  <ul class="primary-nav clearfix">
					<li class="feature-menu"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/index.php">Home</a></li>
                    <li class="feature-menu"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/about.php">About us</a></li>
                    <li class="feature-menu"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php.php">Pricing</a></li>
                    <li class="feature-menu"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php">Portfolio</a></li>
					<li class="feature-menu"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php">Blog</a></li>
                    <li class="feature-menu"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/contact-us.php">Contact</a></li>
                  </ul>
                  </div>
                  <div class="col-md-8 col-lg-8  col-sm-6 col-xs-12 featur-main">
                  <div class="main-scroll-overlay">
                  
                    
					<ul id="parent" class="page-overlay-inner">
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/logo-design.php">Logo Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/web-development.php">Web Development</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/mobile-apps-development.php">Mobile App Development</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/software-development.php">Software Development</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/stationery-design.php">Stationery Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/social-media-design.php">Social Media Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/brochure-design.php">Brochure Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/banner-design.php">Banner Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/magazine-cover-design.php">Magazine Cover Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/corporate-identity-design/promotional-design.php">Promotional Design</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/app-designs.php">App Designs</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/digital-marketing.php">Digital Marketing</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/video-animation.php">Video Animation</a></li>
                        <li class="child"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/copy-writing.php">Copywriting</a></li>
						
                        
                        
                        
                    </ul>
						
					</div>
                  </div>
                 </div>
                </nav>
                
                
                </div>
              </div>
            </div>
        </nav>
        <!-- Navigation end -->
		
		
		
		

</header>