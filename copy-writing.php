<?php include 'header.php';?>
        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/copy-writing/copy-write-slider.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Copy Writing</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->
 

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		<!--COPYWRITING tabs-->


<section class="formest">
  <div class="container">
    <h2>Content that Sells. Quality Content Writing Solutions by Our Hand-Picked Professional Creative Writers</h2>
    <ul class="nav nav-tabs">
      <li class="first active"> <a data-toggle="tab" href="#web-copywriting" aria-expanded="true"> WEB COPYWRITING</a></li>
      <li class=""> <a data-toggle="tab" href="#seo-copywriting" aria-expanded="false"> SEO COPYWRITING</a></li>
      <li class=""> <a data-toggle="tab" href="#article-writing" aria-expanded="false">ARTICLE WRITING</a></li>
      <li class=""><a data-toggle="tab" href="#creative-writing" aria-expanded="false">CREATIVE WRITING</a></li>
      <li class="last"> <a data-toggle="tab" href="#blog-writing" aria-expanded="false">BLOG WRITING</a></li>
    </ul>
    <div class="tab-content" style="overflow:hidden">
      <div id="web-copywriting" class="tab-pane fade active in">
        <div class="row">
          <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/copy-writing/web-copywriting.png" width="632" height="366" class="img-responsive" alt=""> </div>
          <div class="col-md-7 col-xs-12 animated bounceInRight">
            <h5>Web CopyWriting<span> <!--Web Copywriting needed to seize the market--></span></h5>
            <p>Enhance your online presence with our attention-grabbing web content. A good content adds value to your project. At <span class="text-grad">BT Software</span>, we have industry-focused writers who are keen at nailing style and tone with smart content marketing approach. Our copywriting services will speak to your target audience and make them your client forever. Looking for a superior content that is enriched with thoughts and is well-researched? – <span class="text-grad">BT Software</span> is the right solution for your web copywriting needs</p>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Website Pages </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Service Pages </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Product Pages </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Company Information </li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Digital Banners </li>
                </ul>
              </div>
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> PPC Banners </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Remarketing Banners </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Floating Banners </li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Header Captions </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="seo-copywriting" class="tab-pane">
        <div class="row">
          <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/copy-writing/seo-copywriting.png" width="632" height="366" class="img-responsive" alt=""> </div>
          <div class="col-md-7 col-xs-12 animated bounceInRight">
            <h5>SEO CopyWriting <span> <!--5987+ Logos Created for 40+ Industries--></span></h5>
            <p>No matter how good a content is, it will never reach its desired audience if it is not properly optimized with the right keywords. With our SEO Copywriting services, you can appear on top of search engines. Not only it will enhance the ranking of your website, but it will also enhance traffic on your website. At <span class="text-grad">BT Software</span>, we provide content that is precise, to-the-point and enriched with the right keywords.  </p>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Press Release </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> SEO Content for Website </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> SEO enriched Article Writing </li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Product/Service Reviews </li>
                </ul>
              </div>
              <div class="col-md-6 col-xs-12"> 
               
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="article-writing" class="tab-pane">
        <div class="row">
          <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/copy-writing/article-writing.png" width="632" height="366" class="img-responsive" alt=""> </div>
          <div class="col-md-7 col-xs-12 animated bounceInRight">
            <h5>Article Writing <span> <!--5987+ Logos Created for 40+ Industries--></span></h5>
            <p>Grab your audience’s interest by engaging them in your articles. With a thoughtful blend of information and enticing style, <span class="text-grad">BT Software</span> ensure to deliver power-pack article writing services to our clients. <br>
				We have a pool of skilled writers that will produce fresh and engaging content for you. We write about anything and everything as per your demand. 
			</p>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> SEO Article Writing </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Directory Submissions </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Web Page Article  </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Review Articles </li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> News Article Writing </li>
                </ul>
              </div>
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Niche Based Articles </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Wikipedia Articles </li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Editorial Articles </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="creative-writing" class="tab-pane">
        <div class="row">
          <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/copy-writing/creative-writing.png" width="632" height="366" class="img-responsive" alt=""> </div>
          <div class="col-md-7 col-xs-12 animated bounceInRight">
            <h5>Creative writing <span> <!--5987+ Logos Created for 40+ Industries--></span></h5>
            <p>Fuel your success with our creative writing solutions. Creative writing is an art. A combination of beautiful words inspires readers. At <span class="text-grad">BT Software</span>, we make sure to prepare artistically crafted content to allure your target audience and let them choose you over their competitors. <br>
				Leave all your creative projects in the hands of <span class="text-grad">BT Software</span> and our skillful writers will transform your vision into influential pack of words that will generate promising results.
			</p>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Blog writing </li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Script writing for videos</li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Brand names &amp; slogans</li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Magazine content </li>
                </ul>
              </div>
              <div class="col-md-6 col-xs-12"> 
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="blog-writing" class="tab-pane">
        <div class="row">
          <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/copy-writing/blog-writing.png" width="632" height="366" class="img-responsive" alt=""> </div>
          <div class="col-md-7 col-xs-12 animated bounceInRight">
            <h5>Blog Writing <!--<span> 5987+ Logos Created for 40+ Industries</span>--></h5>
            <p>Ignite your customer engagement with our Blog writing services. At <span class="text-grad">BT Software</span>, we have a team of writing wizards that creates enchanted content for your blogs making it highly engaging and compelling. <br>
				From microblogging to macroblogging – we can deliver content that will move your readers mind. Whether you need a light-hearted, informative, controversial, promotional or a sale driven content – <span class="text-grad">BT Software’s</span> blog writing services will bring your vision to life. 
			</p>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <ul>
                  <li class="first"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Blog management</li>
                  <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Guest blogging</li>
                  <li class="last"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Social site blogging </li>
                </ul>
              </div>
              <div class="col-md-6 col-xs-12"> 
                
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
    </div>
  </div>
</section>
<!--COPYWRITING tabs-->
<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section><section class="portfolio-sec">
          <img src="assets/images/client-bg.png" class="shape">
          <div class="container">
            <h1 class="wow fadeInUp">We are serving <span>2000+</span> Clients</h1>
            <p class="wow fadeInUp">We create experiences that transform brands & grow businesses. We have enriched some of the biggest brands and our partnerships with them have caused the creation of some outstanding outcomes. </p>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img1.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img1.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img2.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img2.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img3.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img3.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img4.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img4.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img5.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img5.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>                        
                </div>
              </div>
            </div>
            <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL</a></div>          </div>
</section>
<div class="clearfix"></div>


<!--Packages-->
<section class="packages portfolio-sec cpy-pack" >
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">Copy Writing Packages We Offer</h1>
                <p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget. </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortyseven">
      <div class="collapse-top collapsed" data-target="#fortyseven" aria-expanded="true">
        <div class="price-box">
          <h6>$500</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyseven">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 6 web pages)</li>
            <li>Copywriting (Up to 6 web pages)</li>
            <li>Article Writing (Up to 5 web pages)</li>
            <li>Blog Writing (Up to 1200 words)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> Proofreading and editing</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Re-write/Edit existing web copy (Up to 8 web pages)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyeight">
      <div class="collapse-top collapsed" data-target="#fortyeight" aria-expanded="true">
        <div class="price-box">
          <h6>$1000</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyeight">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 12 web pages)</li>
            <li>Copywriting (Up to 12 web pages)</li>
            <li>Article Writing (Up to 10 web pages)</li>
            <li>SEO Articles (6)</li>
            <li>Press Releases (3)</li>
            <li>On-line Catalogues</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i>  Proofreading and editing</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Re-write/Edit existing web copy</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Sales Letters</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortynine">
      <div class="collapse-top collapsed" data-target="#fortynine" aria-expanded="true">
        <div class="price-box">
          <h6>$1800</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortynine">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 30 web pages)</li>
            <li>Copywriting (Up to 30 web pages)</li>
            <li>Article Writing (Up to 20 web pages)</li>
            <li>Blog Writing (Up to 6000 words)</li>
            <li>SEO Articles (20)</li>
            <li>Press Releases (7)</li>
            <li>Original Web Content Copy</li>
            <li>Newsletters (2)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Re-write/Edit existing web copy</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Sales Letters</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  E-mail Marketing Letter</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  On-line Catalogues</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                       
                </div>
              </div>
            </div>
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
                <h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>              </div>
            </div>
          </div>
        </section>
<div class="clearfix"></div>
<!--Packages-->


		<section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="javascript:;"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
        </section>		
		
        <?php include 'footer.php';?>