<?php include 'header.php';?>

        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/contact-us/contact-slider.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>contact us</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
<section class="contact-us">
<div class="container">
<div class="row">
	<div class="col-md-6">
		<div class="text wow fadeInLeft animated">
			<h1 class="text-grad">Get In-Touch With Us</h1>
			<p><span>Ask an BT Software Pro how we can help you.</span> BT Software provides the most flexible IT solutions to the industry. We are at your service to answer all your queries and deliver effective solutions as per your project demands.</p>
			<ul>
				<li><a class="grad-color" href="" data-toggle="modal" data-target="#SignupModal">Request a quote</a></li>
				<li><a class="grad-color" href="javascript:;" >CallBack Request</a></li>
			</ul>
			<p>Share your idea and our super energetic team will get back to you at lightning speed.</p>
			<p>Ready to discuss? Fill out the form below to start the conversation, or email us at <a href="mailto:info@btsoftwarehouse.com">info@btsoftwarehouse.com</a></p>
		</div>
	</div>
	<div class="col-md-6">
		<div class="img_holder wow fadeInRight animated">
			<img src="assets/images/contact-us/contact-us-right.jpg">
		</div>
	</div>
</div>
</div>
</section>		
<div class="clearfix"></div>

<section class="address">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="call wow fadeInLeft animated">
			<img src="assets/images/contact-us/address-icon.png">
			<h4>Address</h4>
			<p>1412 Broadway 21st Floor Suite MA122 New York, NY 10018</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="call wow fadeInUp animated">
			<img src="assets/images/contact-us/call-icon.png">
			<h4>call us</h4>
			<p><a href="tel:+19174750802">+191-74750802</a></p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="call wow fadeInRight animated">
			<img src="assets/images/contact-us/email-icon.png">
			<h4>E-MAIL</h4>
			<p><a href="mailto:info@btsoftwarehouse.com"> info@btsoftwarehouse.com</a></p>
		</div>
	</div>
</div>
</div>
</section>		
<div class="clearfix"></div>

<section class="global">
<div class="container">
<div class="row">
	
</div>
</div>
</section>		
<div class="clearfix"></div>
		
<?php include 'footer.php';?>