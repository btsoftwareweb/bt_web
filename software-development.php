<?php include 'header.php';?>

        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/software-develop/slider-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Software Development</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="services-tabs" style="background-image:url(assets/images/software-develop/web-design-and-dev-tabs-bg.jpg)">
  <div class="row">
    <div class="col-md-4 col-sm-5 remove-padding">
      <div class="serv-sidebar">
        <div class="serv-sidebar-inner pt-80">
          <h2 class="text-uppercase">Custom Software
Development
</h2>
          <p class="white-color mt-30 mb-40">BT Software has a proven track record of delivering hundreds of custom software designs solutions to our clients with strategic insights to generate persuasive brand engagement, higher conversions and consistent progressive results. Our developers work independently across open-source and proprietary technologies predominantly in Java, C#, C++ and Microsoft .Net. At BT Software, we use a SCRUM based approach for project delivery.</p>
          <ul class="nav nav-tabs">
          <li class="active" data-toggle="tab" href="#menu5"><a href="#">Software Product development <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu1"><a href="#">Application security <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu2"><a href="#">custom software development <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu3"><a href="#">Enterprise Application Integration <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu4"><a href="#">Software Prototyping <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu6"><a href="#">Technology Consulting <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu7"><a href="#">Virtualization Management <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
          </ul>
        </div>
        <!--serv-sidebar-inner-->
        
        <div class="clearfix"></div>
      </div>
      <!--serv-sidebar--> 
      
    </div>
    <div class="col-md-8 col-sm-7 remove-padding">
      <div class="tab-content">
        <div id="menu1" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase">Application security</h1>
          <p class="mt-20 animated bounceInRight">Protecting your apps is an extremely challenging issue in the world of Information Technology. <span class="text-grad">BT Software</span> offers quality security solutions to minimize the safety breaches in your apps. We have a team of professionals in developing security patches that reduces all the loopholes in your applications at the early development stage. With our quality security services, your applications are no more vulnerable!<br>
			Our value-added services provide security in the following major blocks: 
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Operational Security</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Infrastructure Security</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Application Security</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Application-Security.png"></div>
          </div>
        </div>
        <div id="menu2" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">custom software <br> development</h1>
          <p class="mt-20 animated bounceInRight">Pull-off your next project without any hassle and at an affordable rate with <span class="text-grad">BT Software’s</span> custom software development services. At <span class="text-grad">BT Software</span>, we combine a wide range of technical expertise with an unparalleled commitment to client satisfaction. Today, <span class="text-grad">BT Software</span> has become the first option for the organizations who opt for a customized software development solution.<br>
			We take pride in delivering high-quality software solutions to our customers.<br><br>
			Our custom software development services include:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Enterprise Application Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Advanced Web Applications</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Mobile Application Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Application Integration and Customization</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Custom-Software-Development.png" ></div>
          </div>
        </div>
        <div id="menu3" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Enterprise Application <br> Integration</h1>
          <p class="mt-20 animated bounceInRight">Streamline your business operations and re-design your processes with our integrated Enterprise application integration (EAI) solutions. Our services will help you bridge the gap between software applications and enable data flow from one application to another providing interfaces to manage the flow of data which will help you to boost your business potential. <br>
			Our comprehensive EAI solutions includes:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Enterprise Architecture Integration</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Business Process Integration</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Business Activity Monitoring</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Enterprise Service Bus (ESB) Solution</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Service Oriented Architecture (SOA) Solution</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Tools and Frameworks</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Enterprise-Application-Integration.png" ></div>
          </div>
        </div>
        <div id="menu4" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Software Prototyping</h1>
          <p class="mt-20 animated bounceInRight">Secure your investments and reduce your development risks with <span class="text-grad">BT Software’s</span> software prototyping services. Our software prototyping services will streamline and automate your business processes. If you’re looking to decontaminate functionality of applications – then our software prototype services are a best fit for you. It also subsidizes in the identification of problems and provide immediate solutions.<br>
			<span class="text-grad">BT Software</span> specializes in the following Software Prototyping Services:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Static mockups (sketches)</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Functional/Clickable Wireframes</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Visual Designed Interface</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Working Prototype</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
             
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Software-Prototyping.png"></div>
          </div>
        </div>
        <div id="menu5" class="tab-pane fade in active">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Software Product <br> development</h1>
          <p class="mt-20 animated bounceInRight">Our developers are pro at guiding customers with every stage of <span class="text-grad">Software Development Lifecycle</span> (SDLC). Our SDLC services promotes new business ideas and strategies. We offer a strategic plan with a combination of technical and innovative expert solutions to re-design your business processes. Our team will assist you at every stage of product prototyping, migration and maintenance.<br>
			Our bespoke Software Product Development Services includes: 
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Product Development Lifecycle</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Quick Prototype Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Enterprise Software Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Custom Mobile Application Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Responsive Application Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Legacy Migration</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Team Extension</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Software-Product-development.png" alt="" class="lazy"></div>
          </div>
        </div>
        <div id="menu6" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Technology Consulting</h1>
          <p class="mt-20 animated bounceInRight">Technology is advancing rapidly, and it becomes challenging for businesses to cope with the ever-advanced technology mayhem. At <span class="text-grad">BT Software</span>, we offer comprehensive and high-class technology consulting services to cover the complete development and implementation of your software application. For over 10 years, we are supporting various companies in software development implementation and quality assurance support.<br>
			Our professional IT team provide new landscapes to enhance the efficiency of your existing IT environment to stay competitive in the industry.<br><br>
			Our technology consulting services include:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> IT Stratagem and Roadmap</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Business Development & Growth</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> On-going Operations</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Next-Generation Software Applications</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Starts-up Resolutions</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Technology-Consulting.png"></div>
          </div>
        </div>
        <div id="menu7" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Virtualization Management</h1>
          <p class="mt-20 animated bounceInRight">Virtualize your management system with <span class="text-grad">BT Software’s</span> Virtualization Management System Services. We provide proficient consultancy from comprehensive IT infrastructure assessment to gap analysis. We also offer infrastructure planning, design, financial analysis, and its implementation. It includes licensing optimization. Our team offer expert advice over migration in ongoing management and maintenance using Microsoft, VMware, Parallels, and Sun virtualization solutions.</p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> VM Storage Optimization</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Optimizing VM Performance</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> VM rightsizing automation</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images/software-develop/Virtualization-Management.png"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--services-tabs-->
<div class="clearfix"></div>
<section class="our-client  pt-40 pb-50">
	<div class="container">
    	<div class="row text-center">
        	<div class="col-md-12">
            	<h1 class="primary-color text-uppercase">Our Partners</h1>
                <p class="hidden-xs">We are proud to have partnered with some of the region's and the world's leading companies, <br> bringing their brands to life and transforming their people and customer experiences.</p>
            </div>
        </div>
        	<div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl1.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl1.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl2.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl2.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl3.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl3.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl4.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl4.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl5.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl5.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl6.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl6.jpg" style="display: block;"></div>
            </div>

        	<div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl7.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl7.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl11.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl11.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl12.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl12.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl13.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl13.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl14.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl14.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl15.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl15.jpg" style="display: block;"></div>
            </div>


            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl16.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl16.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl17.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl17.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl18.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl18.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl19.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl19.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl20.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl20.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl21.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl21.jpg" style="display: block;"></div>
            </div>


            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl22.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl22.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl23.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl23.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl24.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl24.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl25.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl25.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl26.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl26.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl27.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl27.jpg" style="display: block;"></div>
            </div>

        <!--<div class="row mt-50 text-center">
        	<div class="col-md-12">
            	<a href="/portfolio/" class="btn grad-color mt-20">VIEW MORE</a>
            </div>
        </div>-->
    </div>
</section><div class="clearfix"></div>
	
		<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section><section class="trusted-sec pt-40 pb-50">
	<div class="container">
    	<h2 class="secondary-color font-900 text-uppercase text-center">trusted by multiple businesses</h2>
        <p class="text-center mt-20">We think that transformation is the key to success. We levrage and dignify technology to become more effective and efficient in <br> software development solutions.</p>
    	<div class="row">
        	<div class="col-md-12">
				<img src="assets/images/software-develop/trusted/trusted-img-new.jpg" style="display: inline;">
			</div>
        </div>
    </div>
</section>
		<section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="/blog/blog1"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog2">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog3">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog4/">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
        </section>		
		
        <?php include 'footer.php';?>