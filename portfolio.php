<?php include 'header.php';?>
        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/portfolio/portfolio-slider.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>portfolio</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->
			</div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="packages portfolio-sec">
<img src="assets/images/packages-offer.png" class="shape">
<div class="container">
<div class="row text-center">
	<div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="wow fadeInUp">Our Portfolio</h1>
		<p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models.  We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget.  </p>
    </div>
	<div class="col-md-2"></div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="mission-tab">
						<!-- Nav tabs -->
			<ul class="nav nav-tabs wow fadeInUp" role="tablist">
				<li role="presentation" class="active"><a href="#website-port" role="tab" data-toggle="tab">Website</a></li>
				<li role="presentation" ><a href="#logo-port" role="tab" data-toggle="tab">Logo</a></li>
				<li role="presentation"><a href="#corp-iden-port" role="tab" data-toggle="tab">Branding</a></li>
				<li role="presentation"><a href="#vid-anim-port" role="tab" data-toggle="tab">Video Animation</a></li>
				<li role="presentation"><a href="#cpy-write-port" role="tab" data-toggle="tab">Copy Writing</a></li>
				<li role="presentation"><a href="#app-des-port" role="tab" data-toggle="tab">App <span>Designs</span></a></li>
			</ul>
							<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active logo-pricing" id="website-port">
					<div class="row logo logo-portfolio">
	<div class="row mr">
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/1.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/1-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/2.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/2-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/4.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/4-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/5.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/5-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/6.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/6-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/7.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/7-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/8.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/8-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/9.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/9-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/10.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/10-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/11.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/11-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/12.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/12-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/13.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/13-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/14.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/14-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/15.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/15-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/16.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/16-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/17.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/17-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/18.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/18-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/19.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/19-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/20.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/20-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/21.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/21-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/22.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/22-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>  
</div>				</div>
				<div role="tabpanel" class="tab-pane fade logo-pricing" id="logo-port">
					<div class="tabs logo logo-portfolio">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#illus-logo" data-toggle="tab" aria-expanded="false"> Illustrative <span>Design</span></a></li>
		<li class=""><a href="#mascot-logo" data-toggle="tab" aria-expanded="false"> Mascot <span>Design</span></a></li>
		<!--<li class=""><a href="#3d-logo" data-toggle="tab" aria-expanded="false">3D Design </a></li>-->
		<li class=""><a href="#abs-logo" data-toggle="tab" aria-expanded="false">Abstract <span>Design</span></a></li>
		<li class=""><a href="#icon-logo" data-toggle="tab" aria-expanded="true">Iconic <span>Design</span></a></li>
		<li class=""><a href="#flat-logo" data-toggle="tab" aria-expanded="false">Flat <span>Design</span> </a></li>
		<!--<li class=""><a href="#real-logo" data-toggle="tab" aria-expanded="false">Realistic Logo Design</a></li>-->
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		
		<!--illus-logo-->
		<div id="illus-logo" class="tab-pane active">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6 ">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/2-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/3-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/8-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/10.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/10.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/11.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/11.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/12.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/12.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/13.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/13.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/14.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/14.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/15.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/15.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/16.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/16.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/17.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/17.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/18.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/18.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--mascot-logo-->
		<div id="mascot-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/10.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/10.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/11.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/11.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/12.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/12.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--3d-logo
		<div id="3d-logo" class="tab-pane">
			<div class="row mr">
				
			</div>
        </div>
		<!--abs-logo-->
		<div id="abs-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/1-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/2-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/4-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--icon-logo-->
		<div id="icon-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		
		<!--flat-logo-->
		<div id="flat-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
	</div>
</div>
				</div>
				<div role="tabpanel" class="tab-pane fade logo-pricing" id="corp-iden-port">
					<div class="tabs logo logo-portfolio">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#stationery-design" data-toggle="tab" aria-expanded="false">Stationery <span>Design</span></a></li>
		<li class=""><a href="#social-media-design" data-toggle="tab" aria-expanded="false">Social Media <span>Design</span> </a></li>
		<li class=""><a href="#brochure-design" data-toggle="tab" aria-expanded="false"> Brochure <span>Design</span></a></li>
		<li class=""><a href="#banner-design" data-toggle="tab" aria-expanded="true">Banner <span>Design</span></a></li>
		<li class=""><a href="#promotional-design" data-toggle="tab" aria-expanded="false">Promotional <span>Design</span></a></li>
		<li class=""><a href="#mag-design" data-toggle="tab" aria-expanded="false">Magazine <span>Design</span></a></li>
	</ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--stationery-design-->
		<div id="stationery-design" class="tab-pane active">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--social-media-design-->
		<div id="social-media-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--brochure-design-->
		<div id="brochure-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img10.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img10.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img11.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img11.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img12.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img12.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
								
                             
</div>			</div>
        </div>
		<!--banner-design-->
		<div id="banner-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--promotional-design-->
		<div id="promotional-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img10.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img10.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img11.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img11.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img12.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img12.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--Magazine-Design-->
		<div id="mag-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
	</div>
</div>
				</div>
				<div role="tabpanel" class="tab-pane fade logo-pricing" id="vid-anim-port">
					<div class="row video-portfolio">
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/1.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=1Q_S3nqOGU8" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/2.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=7T-yrsTQii4" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/3.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=bLv44cEw5Gk" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/4.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=7oUSaF4x5pQ" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/5.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=glAj2gdMupY" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/6.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=gBVohsdSbFE" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/7.png" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=Qlcqc1acySc" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/8.png" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=ZRbOC3MS1Lg" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/9.png" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=UzRsGpUO8NQ" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	
</div>				</div>
				<div role="tabpanel" class="tab-pane fade logo-pricing" id="cpy-write-port">
					<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img1.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img1.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img2.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img2.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img3.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img3.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img4.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img4.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img5.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img5.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>				</div>
				<div role="tabpanel" class="tab-pane fade logo-pricing" id="app-des-port">
					<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img5-hvr.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img7-hvr.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>				</div>
							
			</div>
		</div>
	</div>
</div>
</div>
</section>
<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section>	

		<?php include 'footer.php';?>