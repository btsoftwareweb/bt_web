<?php include 'header.php';?>
        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/mobile-app-dev/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Mobile App Development</span></h1>
					  <h3>Connect with your clients on-the-go with our customized Mobile Apps</h3>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->
			</div> <!-- /.carousel -->
        </section> <!-- /#home -->
		

        
<section class="what-we-are-offering">
	<div class="container">
    	<div class="row">
			<div class="col-md-2"></div>
        	<div class="col-md-8 text-center">
            	<h2 class="secondary-color font-900 text-uppercase">WHAT WE ARE OFFERING?</h2>
				<p>We offer customized mobile application development that have a cross-platform compatibility to power critical processes beyond mere automation. From ideation to dissemination — we have you covered all the way with our exquisite Mobile App development solutions. </p>
            </div>
			<div class="col-md-2"></div>
        </div>
    	<div class="row mt-50">
        	<div class="col-md-4">
            	<div class="what-box wow fadeInLeft animated">
                	<div class="bg" style="background-image:url(assets/images/mobile-app-dev/ios-app.jpg);"></div>
                	<h3>iOS APP</h3>
                    <p> We offer flawless and dynamic application development for iPhone all over the world. Share your requirements in terms of <span class="text-grad">user-experience, look-and-feel and app functionality</span> - we ensure to build enriched iPhone apps and offer a smoothest possible process to our clients. </p>
                    <!--<a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>-->
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeDown animated">
                	<div class="bg" style="background-image:url(assets/images/mobile-app-dev/android-app.jpg);"></div>
                	<h3>Android App</h3>
                    <p>Our expert developers have updated gears and knowledge about Android development to maximize your business growth to a great scale. At <span class="text-grad">BT Software</span> – we discuss, <span class="text-grad">plan, develop, test and deploy</span> projects as per your demand. Ready to discuss your project with us?</p>
                    <!--<a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>-->
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeInRight animated">
                	<div class="bg" style="background-image:url(assets/images/mobile-app-dev/cross-platform.jpg);"></div>
                	<h3>Cross Platform</h3>
                    <p><span class="text-grad">BT Software's</span> cross-platform mobile applications services can run on devices operating in various mobile platforms and help you reach the maximum clientele. The apps will help your business to be available in the right market at the right time. Contact us now to discuss your plan.</p>
                    <!--<a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>-->
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-4">
            	<div class="what-box wow fadeInLeft animated">
                	<div class="bg" style="background-image:url(assets/images/mobile-app-dev/iot.jpg);"></div>
                	<h3>IoT</h3>
                    <p>Our team helps you create a performance oriented IoT app with minimal possible turnaround time. With our ground breaking IoT mobile application solutions, you will have great opportunities for more direct integration as it allows objects to be controlled remotely across an existing network infrastructure. Our IoT solutions will help you minimize human intervention resulting in enhanced accuracy and efficiency. Send us a message and our representative will get back to you in no time. </p>
                    <!--<a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>-->
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeInUp animated">
                	<div class="bg" style="background-image:url(assets/images/mobile-app-dev/mobile-games.jpg);"></div>
                	<h3>Mobile Gaming</h3>
                    <p>Create your dream games with <span class="text-grad">BT Software</span> mobile gaming solutions. At <span class="text-grad">BT Software</span> – we transform your ideas into business. With a small idea, our developers will turn your investments into super-ultra-mega profits. </p>
                    <!--<a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>-->
                </div>
            </div>
            <div class="col-md-4">
            	<div class="what-box wow fadeInRight animated">
                	<div class="bg" style="background-image:url(assets/images/mobile-app-dev/ar.jpg);"></div>
                	<h3>Augmented Reality</h3>
                    <p>Convert your ideas into an AR featured application. We are proud professionals in developing versatile AR featured application. We ensure that our apps are cost-effective, personalized with integrated point-of-interest to provide value adding services to our clients. Our augmented reality apps are innovative and intuitively designed for our clients to give them a rich and calculated experience.</p>
                    <!--<a href="/contact-us" class="secondary-color mont-font-600">Learn more</a>-->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section><section class="cont mobile-app hidden-xs hidden-sm" style="background:url(assets/images/mobile-app-dev/logo-design-bg.jpg) no-repeat;background-size:cover;">
	<div class="container">
        <h2>OUR APPLICATION SHOWCASE</h2>
        <ul class="nav nav-tabs">
            <li class="active"> <a data-toggle="tab" href="#native-app-dev"> Native App Development</a></li>
            <li> <a data-toggle="tab" href="#cross-platform-apps"> Cross Platform Apps</a></li>
            <li> <a data-toggle="tab" href="#enter-mob-sol"> Enterprise Mobility Solutions</a></li>
            <li><a data-toggle="tab" href="#b2b-b2c-portal"> B2B and B2C Portals</a></li>
            <li> <a data-toggle="tab" href="#web-app"> Web Applicationss</a></li>
        </ul>

<br>
		<div class="tab-content" style="overflow:hidden">
            <div id="native-app-dev" class="tab-pane active">
                <div id="native-app" class="owl-carousel">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> 
								<img src="assets/images/mobile-app-dev/native1.png" width="418" height="470"> 
                            </div>
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
								<div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
											<li><span>Name:</span> Wayyak</li>
											<li><span>Region:</span> Plano, TX</li>
											<li><span>Industry:</span> Human Resource/ Entertainment</li>
											<li><span>Platform:</span> iOS, Android</li>
											<li><span>Technology:</span> JavaScript, HTML, XCode</li>
											<li><span>Description:</span> <span class="f-right">Wayyak application was developed by Etisalat Misr solely for facilitating Etisalat	employees. The app allows Etisalat staff to check and avail latest offers, discounts, enquire	travel balance, gain medical benefits and much more. It is a one stop place designed to make	Etisalat employees lives easier and more convenient.</span></li> 
                                        </ul>
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
											<a href="#"><img src="assets/images/mobile-app-dev/app-store.png" ></a>
											<a href="#"><img src="assets/images/mobile-app-dev/google-play.png"></a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
						</div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> 
								<img src="assets/images/mobile-app-dev/native2.png" width="418" height="470"> 
                            </div>
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
								<div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
											<li><span>Name:</span> VGO</li>
											<li><span>Region:</span> San Jose, CA</li>
											<li><span>Industry:</span> Automotive</li>
											<li><span>Platform:</span> Android</li>
											<li><span>Technology:</span> HTML5, CSS3, JavaScript</li>
											<li><span>Description:</span> <span class="f-right">VGO is a valet service app that allows you to request valet while you are going out for dining or any other meeting, check in the details of your parking through QR scanning of the valet ticket and even request back the vehicle. The application keeps you informed about the whereabouts regarding your vehicle through push notifications. No need of waiting at the valet stand now to retrieve your car.</span></li> 
                                        </ul>
                                        <div class="clearfix"></div>
                                        
                                        <div class="mt50 store">
											<a href="#"><img src="assets/images/mobile-app-dev/app-store.png"></a>
											<a href="#"><img src="assets/images/mobile-app-dev/google-play.png"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
						</div>
                        
                    </div>
                </div>
			</div>
            <!--Cross Platform-->
			<div id="cross-platform-apps" class="tab-pane">
                <div id="cross-platform-app" class="owl-carousel">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn">
								<img src="assets/images/mobile-app-dev/cross-platf1.png" width="418" height="470" class="lazy img-responsive mb40" alt="mioDoggy" title="mioDoggy"> 
							</div>
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
								<div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
											<li><span>Name:</span> MioDoggy</li>
											<li><span>Region:</span> Worcester, MA</li>
											<li><span>Industry:</span> Entertainment</li>
											<li><span>Platform:</span> Android</li>
											<li><span>Technology:</span> HTML5, CSS3, JavaScript</li>
											<li><span>Description:</span> <span class="f-right">An app that allows you to find, connect and engage with likeminded people within
	your vicinity to hang out with your pet. You can also share special dog moments with the world
	and even compete your dog’s abilities with your friends. It is further equipped with the
	functionality to locate your dog in case of being lost.</span></li> 
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
											<a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
											<a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
						</div>
                        
                    </div>
                            
                    <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/cross-platf2.png" width="418" height="470" class="lazy img-responsive mb40" alt="Ingredient Matcher" title="Ingredient Matcher"> 
                          
                            
                            </div>
                            
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> Ingredient Matcher</li>
                                        <li><span>Region:</span> Dallas, TX</li>
                                        <li><span>Industry:</span> Food</li>
                                        <li><span>Platform:</span> Android</li>
                                        <li><span>Technology:</span> HTML5, CSS3, JavaScript</li>
                                        <li><span>Description:</span> <span class="f-right">Ingredient Matcher app allows you to make quick and delicious food with the
ingredients available at your kitchen. The app will store the list of ingredients that you currently
have in your kitchen and match it with famous recipes around the world to suggest meals that
you can instantly prepare.</span></li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                        
                        </div>
                    
                    </div>
					
					<!--enter-mob-sol-->
					<div id="enter-mob-sol" class="tab-pane">
                    
                        <div id="enterprise" class="owl-carousel">
                    		
                            <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/enterprise1.png" width="418" height="470" class="lazy img-responsive mb40" alt="obligo" title="obligo"> 
                            
                            
                            </div>
                            


                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> Obligo</li>
                                        <li><span>Region:</span> Houston, TX</li>
                                        <li><span>Industry:</span> Finance</li>
                                        <li><span>Platform:</span> iOS, Android</li>
                                        <li><span>Technology:</span> JavaScript, HTML, XCode</li>
                                        <li><span>Description:</span> <span class="f-right">An app that works like a mobile wallet. Obligo allows customers to purchase a
product and pay for it later which can be either end of month, quarter or year. The app applies
insurance to each item bought and a credit rate to ensure that the merchants receive their
money.</span></li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="mt50 store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                            
                            <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/enterprise2.png" width="418" height="470" class="lazy img-responsive mb40" alt="we2link" title="we2link"> 
                            
                            
                            </div>
                            
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> We2Link</li>
                                        <li><span>Region:</span> Atlanta, GA</li>
                                        <li><span>Industry:</span> Business &amp; Consulting</li>
                                        <li><span>Platform:</span> iOS, Android</li>
                                        <li><span>Technology:</span> JavaScript, HTML, XCode</li>
                                        <li><span>Description:</span> <span class="f-right">The app is designed to allow people to connect via their business cards. It also
converts a paper based business card into digital form. We2Link facilitates professional
interaction by sharing business card to reach out to various industry experts.</span></li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="mt50 store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                        
                        </div>
                    
                    </div>
                   <!--b2b-b2c-portal-->
				   <div id="b2b-b2c-portal" class="tab-pane">
                    
                        <div id="b2b-b2c" class="owl-carousel">
                    		
                            <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/b2b1.png" width="418" height="470" class="lazy img-responsive mb40" alt="somo" title="somo"> 
                            
                            
                            </div>
                            


                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> Somo</li>
                                        <li><span>Region:</span> Orlando, FL</li>
                                        <li><span>Industry:</span> Fashion</li>
                                        <li><span>Platform:</span> iOS, Android</li>
                                        <li><span>Technology:</span> JavaScript, HTML, XCode</li>
                                        <li><span>Description:</span> <span class="f-right">Somo is an e-commerce portal that allows you to buy and sell shoes. The user can
create its own profile and post details of the product they are selling. Moreover, the user can
also follow top brands around the world and keep themselves updated with latest fashion
trends and news.</span></li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                            
                            <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/b2b2.png" width="418" height="470" class="lazy img-responsive mb40" alt="Goldenage Cheese" title="Goldenage Cheese"> 
                            
                            
                            </div>
                            
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> Goldenage Cheese</li>
                                        <li><span>Region:</span> Philadelphia, PA</li>
                                        <li><span>Industry:</span> Fashion</li>
                                        <li><span>Platform:</span> iOS, Android</li>
                                        <li><span>Technology:</span> JavaScript, HTML, XCode</li>
                                        <li><span>Description:</span> <span class="f-right">The app is developed for cheese lovers. With information of over 400 types of
cheeses available, you can cook and enjoy cheese based recipes to savor the cheese delight.</span></li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                        
                        </div>
                    
                    </div>
					<!--Web app-->
					<div id="web-app" class="tab-pane">
                    
                        <div id="web-apps" class="owl-carousel">
                    		
                            <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/web-application2.png" width="418" height="470" class="lazy img-responsive mb40" alt="travel" title="travel"> 
                            
                           
                            
                            </div>
                            


                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> Travel</li>
                                        <li><span>Regions:</span> Minneapolis, MN</li>
                                        <li><span>Industry:</span> Travel
                                        <li><span>Platform:</span> iOS, Android</li>
                                        <li><span>Technology Used:</span> JavaScript, HTML, XCode</li>
                                        <li><span>Description:</span> <span class="f-right">Embark on a journey with our aesthetically designed Travel mobile app. Explore

the world through our lens and learn about beautiful cities and sights around the globe. Feel

the adrenaline rush with our exciting, inquisitive mobile development service.</li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                            
                            <div class="item">
                            
                        		<div class="row">
                            <div class="col-md-5 col-xs-12  animated zoomIn"> <img src="assets/images/mobile-app-dev/web-application1.png" width="418" height="470" class="lazy img-responsive mb40" alt="La Grata" title="La Grata"> 
                            
                           
                            
                            </div>
                            
                            <div class="col-md-7 col-xs-12 animated bounceInRight">
                              
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="des-mob-app">
                                        
                                        <li><span>Name:</span> La Grata</li>
                                        <li><span>Regions:</span> Minneapolis, MN</li>
                                        <li><span>Industry:</span> Food</li>
                                        <li><span>Platform:</span> Cross Platform Web App</li>
                                        <li><span>Technology Used:</span> JavaScript, HTML, XCode</li>
                                        <li><span>Description:</span> <span class="f-right">A basic and a delighful snack restaurant from where you can get scrumptious food and coffee.</li> 
                                        
                                        
										
                                        </ul>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="store">
                                        
                                        <a href="#"><img src="assets/images/mobile-app-dev/app-store.png" class="lazy"></a>
                                        <a href="#"><img src="assets/images/mobile-app-dev/google-play.png" class="lazy"></a>
                                        
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>

						
                        
                        </div>
                        
                        	</div>
                        
                        </div>
                    
                    </div>
					
				   
        </div>
                    
        </div>
	


</section>
 <div class="clearfix"></div>
       
		

<section class="packages portfolio-sec">
	<img src="assets/images/packages-offer.png" class="shape">
	<div class="container">
				<!--<div class="row text-center">
				  <div class="col-m-12">
					<h1 class="wow fadeInUp">Mobile App Development Packages We Offer</h1>
					<p class="wow fadeInUp">sign Live Pro believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget.</p>
				  </div>
				</div>-->
				<!--<div class="row">
				  <div class="col-md-12">
					<div class="mission-tab">
												   
					</div>
				  </div>
				</div>-->
				<div class="row v-a-pack v-a-no-pack text-center">
					<div class="col-md-12">
						<h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>					</div>
				</div>
          </div>
        </section>
	<div class="clearfix"></div>

        <section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="javascript:;"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="javascript:;">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
        </section>   
        <?php include 'footer.php';?>