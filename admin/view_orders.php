
    

</body>
</html>



<?php
include 'sidebar_header.php';
include '../mainfolder.php';
?>

<table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Services</th>
                    <th>Phone</th>
                    <th>Packages</th>
                    <th>File</th>
                    <th>Is Active</th>
                    <th></th>
                </tr>
        </thead>
        <tbody>
<?php
include '../database.php';

$sql = "SELECT * from orders";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($rowval = $result->fetch_assoc())
    {
        $id = $rowval["id"];
        $name = $rowval["name"];
        $email = $rowval["email"];
        $phone = $rowval["phone"];
        $service = $rowval["service"];
        $package = $rowval["package"];
        $content = $rowval["content"];
        $is_active = $rowval["is_active"];
        if($is_active == 1)
        {
            $checked = "checked";
            $buttonname="Dis-Approve";
            $buttonclass = "btn-danger";
        }
        else {
            $checked = "";
            $buttonname="Approve";
            $buttonclass = "btn-info";
        }
        echo('
        <tr>
        <td>'.$id.'</td>
        <td>'.$name.'</td>
        <td>'.$email.'</td>
        <td>'.$phone.'</td>
        <td>'.$service.'</td>
        <td>'.$package.'</td>
        <td><a href="/'.$mainfolder.'/uploads/'.$content.'"  >'.$content.'</a></td>
        <td><input type="checkbox" name="isactive" '.$checked.'  ></td>
        <td><a href="post_orders.php?is_active='.$is_active.'&id='.$id.'"><input type="submit" class="btn '.$buttonclass.'" name="submit" value="'.$buttonname.'"></a></td>
        </tr>');

    }

} else {
   

}

?>
                
                </tbody>
        </table>



<?php
include 'footer.php';
?>