<?php
session_start();
include '../database.php';
include '../mainfolder.php';
if(isset($_SESSION["id"]))
{
  if($_SESSION["id"] == "error" || $_SESSION["id"] == "")
  {
    header( 'Location:'.$_server["http_host"].'../../'.$mainfolder.'/admin/login.php' ) ;
  }
}
else
{
    header( 'Location:'.$_server["http_host"].'../../'.$mainfolder.'/admin/login.php' ) ;
  }

?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>BT - Software</title>
    <!-- Main styles for this application-->
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar" style="background-color: #161765;">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>">
        <img class="navbar-brand-full" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder.'/assets/images/logo.png'; ?>" width="89" height="25" alt="Logo">
        <img class="navbar-brand-minimized" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder.'/assets/images/logo.png'; ?>" width="30" height="30" alt="Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Dashboard</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Users</a>
        </li>
      </ul>
      <ul class="nav navbar-nav ml-auto">
        
        <li class="nav-item d-md-down-none">
          <a class="nav-link" href="#">
            <i class="icon-list"></i>
          </a>
        </li>
        <li class="nav-item d-md-down-none">
          <a class="nav-link" href="#">
            <i class="icon-location-pin"></i>
          </a>
        </li>
        <li class="nav-item dropdown">
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-bell-o"></i> Updates
              <span class="badge badge-info">42</span>
            </a>
            <a class="dropdown-item" href="#">
              <i class="fa fa-envelope-o"></i> Messages
              <span class="badge badge-success">42</span>
            </a>
            <a class="dropdown-item" href="#">
              <i class="fa fa-tasks"></i> Tasks
              <span class="badge badge-danger">42</span>
            </a>
            <a class="dropdown-item" href="#">
              <i class="fa fa-comments"></i> Comments
              <span class="badge badge-warning">42</span>
            </a>
            <div class="dropdown-header text-center">
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-user"></i> Profile</a>
            <a class="dropdown-item" href="#">
              <i class="fa fa-usd"></i> Payments
              <span class="badge badge-secondary">42</span>
            </a>
            <a class="dropdown-item" href="#">
              <i class="fa fa-file"></i> Projects
              <span class="badge badge-primary">42</span>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-shield"></i> Lock Account</a>
            <a class="dropdown-item" href="#">
              <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
      <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
      </button>
    </header>
    <div class="app-body">
      <div class="sidebar" style="background-color: #161765;">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">
                <i class="nav-icon icon-speedometer"></i> Dashboard
                <!-- <span class="badge badge-primary">NEW</span> -->
              </a>
            </li>
            <!-- <li class="nav-title">Theme</li>
            <li class="nav-item">
              <a class="nav-link" href="colors.html">
                <i class="nav-icon icon-drop"></i> Colors</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="typography.html">
                <i class="nav-icon icon-pencil"></i> Typography</a>
            </li> -->
            <li class="divider"></li>
            <li class="nav-title">PAGES</li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link" href="isactive.php"><i class="nav-icon icon-star"></i> Users</a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link" href="view_orders.php"><i class="nav-icon icon-star"></i> Orders</a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link" href="view_message.php"><i class="nav-icon icon-star"></i> Messages</a>
            </li>




          </ul>
        </nav>
      </div>
      <main class="main" style="    background-color: white;">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item">
            <a href="#">Admin</a>
          </li>
          <li class="breadcrumb-item active">Dashboard</li>
          <!-- Breadcrumb Menu-->
          <li class="breadcrumb-menu d-md-down-none">
            <div class="btn-group" role="group" aria-label="Button group">
              <a class="btn" href="#">
                <i class="icon-speech"></i>
              </a>
              <a class="btn" href="./">
                <i class="icon-graph"></i>  Dashboard</a>
              <a class="btn" href="login.php">
                <i class="icon-settings"></i> Logout</a>
            </div>
          </li>
        </ol>
        <div class="container-fluid">