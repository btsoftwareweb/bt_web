
<?php
include '../mainfolder.php';
include '../database.php';
if(isset($_POST['submit']))
{
$name = $_POST['name'];
$email = $_POST['email'];
$password = $_POST['password'];
$sql = "INSERT INTO `users` (`name`, `email`, `password`, `is_active`) VALUES ('{$name}', '{$email}', '{$password}',0)";

if (mysqli_query($conn, $sql)) {

    header( 'Location:'.$_server["http_host"].'/'.$mainfolder.'/admin/login.php' ) ;

} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

}

?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <title></title>
    <!-- Main styles for this application-->
    <link href="css/style.css" rel="stylesheet">
    <link href="vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  </head>
  <body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Sign Up</h1>
                <p class="text-muted">Sign Up to have new account</p>
                <form  method="POST" class="register-form" id="login-form">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  
                  <input class="form-control" name="name" type="text" placeholder="Username">
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <input class="form-control" name="password" type="password" placeholder="Password">
                
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <input class="form-control" name="email" type="text" placeholder="Email">
                
                </div>
                <div class="row">
                  <div class="col-6">
                    <input class="btn btn-primary px-4" type="submit" name="submit" id="submit" value="Submit"/>
                  </div>
                </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
