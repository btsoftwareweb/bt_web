<?php include 'header.php';?>


        <!-- Intro Section -->
        <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/blog/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Blogs</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="blsec">
  <div class="container mb-20 mt-20">
    <div class="row">
		<div class="left-bar">
			<!--Blog-1-->
			<div class="col-md-3">
				<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<h3 class="body-heading "> Why custom logo designs are essential for a Start-up?</h3>
				</a>
				<div class="red-bottom-bar"></div>
				<div class="" style="margin-top:10px"> 
					<span class="cus-button grad-color">Logo Design</span>
					<div style="display: inline-block;float: left;margin-top: 8px;"> 
						<span class="info-text"><i class="fa fa-clock-o" aria-hidden="true"></i>Sep 7, 2018 </span> 
						<span class="info-text1"><i class="fa fa-user" aria-hidden="true"></i>By BT Software Design </span> 
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="box-container">
					<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<div class="box"> <img src="assets/images/blog/blog-1.jpg" class="img-responsive" style="width:100%;"> </div>
				  </a>
				</div>
				<div class="blog-detail">
					<p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.... </p>
					<p class="read"><a href="../<?php echo $mainfolder ?>/blog/blog-1.php" class="primary-color text-uppercase font-700">read more <i class="fa fa-long-arrow-right"></i></a></p>
				</div>
            </div>
			<!--Blog-2-->
			<div class="col-md-3">
				<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<h3 class="body-heading ">Custom Website Design – Do’s and don’ts</h3>
				</a>
				<div class="red-bottom-bar"></div>
				<div class="" style="margin-top:10px"> 
					<span class="cus-button grad-color">Web Design</span>
					<div style="display: inline-block;float: left;margin-top: 8px;"> 
						<span class="info-text"><i class="fa fa-clock-o" aria-hidden="true"></i>Aug 30, 2018 </span> 
						<span class="info-text1"><i class="fa fa-user" aria-hidden="true"></i>By BT Software Design </span> 
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="box-container">
					<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<div class="box"> <img src="assets/images/blog/blog-2.jpg" class="img-responsive" style="width:100%;"> </div>
				  </a>
				</div>
				<div class="blog-detail">
					<p>Designing a customized website is a tricky matter. There are A LOT of things that needs to be considered before getting it done.... </p>
					<p class="read"><a href="../<?php echo $mainfolder ?>/blog/blog-1.php" class="primary-color text-uppercase font-700">read more <i class="fa fa-long-arrow-right"></i></a></p>
				</div>
            </div>
			<!--Blog-3-->
			<div class="col-md-3">
				<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<h3 class="body-heading "> Enhance Your Brand Visibly with Professional Logo Design</h3>
				</a>
				<div class="red-bottom-bar"></div>
				<div class="" style="margin-top:10px"> 
					<span class="cus-button grad-color">E Commerce</span>
					<div style="display: inline-block;float: left;margin-top: 8px;"> 
						<span class="info-text"><i class="fa fa-clock-o" aria-hidden="true"></i>Jul 27, 2018 </span> 
						<span class="info-text1"><i class="fa fa-user" aria-hidden="true"></i>By BT Software Design </span> 
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="box-container">
					<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<div class="box"> <img src="assets/images/blog/blog-3.jpg" class="img-responsive" style="width:100%;"> </div>
				  </a>
				</div>
				<div class="blog-detail">
					<p>One of the major goal of every business is to maximize its reach, attract a large number of customers – in short enhance its visibility. That is where businesses starts to invest generously in branding.... </p>
					<p class="read"><a href="../<?php echo $mainfolder ?>/blog/blog-1.php" class="primary-color text-uppercase font-700">read more <i class="fa fa-long-arrow-right"></i></a></p>
				</div>
            </div>
			<!--Blog-4-->
			<div class="col-md-3">
				<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<h3 class="body-heading "> Quick Tips Before you choose a Web Design Agency</h3>
				</a>
				<div class="red-bottom-bar"></div>
				<div class="" style="margin-top:10px"> 
					<span class="cus-button grad-color">E Commerce</span>
					<div style="display: inline-block;float: left;margin-top: 8px;"> 
						<span class="info-text"><i class="fa fa-clock-o" aria-hidden="true"></i>Jul 16, 2018 </span> 
						<span class="info-text1"><i class="fa fa-user" aria-hidden="true"></i>By BT Software Design </span> 
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="box-container">
					<a href="../<?php echo $mainfolder ?>/blog/blog-1.php">
					<div class="box"> <img src="assets/images/blog/blog-4.jpg" class="img-responsive" style="width:100%;"> </div>
				  </a>
				</div>
				<div class="blog-detail">
					<p>Designing a website is a basic step for a company to let the people know about your existence. The market is bustled with so many web developers....  </p>
					<p class="read"><a href="../<?php echo $mainfolder ?>/blog/blog-1.php" class="primary-color text-uppercase font-700">read more <i class="fa fa-long-arrow-right"></i></a></p>
				</div>
            </div>
			<div class="clearfix"></div> 
		
        </div>
    <!-- </div> -->
    </div>
</section>
		
<?php include 'footer.php';?>