<?php include 'header.php';?>
        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/about-slider-bg.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Privacy Policy</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="privacy-policy">
  <div class="container">
  	<div class="p-t">
    	<h2 class="secondary-color text-uppercase font-900">privacy policy</h2>
  		<h4 class="secondary-color text-uppercase">What information do we gather?</h4>
        <p>We only gather personally identifiable information when you sign into our site or provide information in our form fields. You might be requested to provide your full name, email address, mailing address and phone number.</p>
        
        <h4 class="secondary-color text-uppercase">What do we do with this information?</h4>
        <p>The details that we gather might be used in following ways:</p>
        
        <h4 class="secondary-color text-uppercase">To personalize your experience:</h4>
        <p>Your information allows us to respond better to your individual needs.</p>
        
        <h4 class="secondary-color text-uppercase">To make our website better:</h4>
        <p>We constantly try to enhance our website performance and services based on the details and response we get from you.</p>
        
        <h4 class="secondary-color text-uppercase">To enhance our customer service:</h4>
        <p>Your details allow us to immediately respond to your customer service requests and support needs.</p>
        
        <h4 class="secondary-color text-uppercase">To send promotional emails:</h4>
        <p>The email address you give might be utilized to send promotional emails and special offers from time to time.</p>
        
        <h4 class="secondary-color text-uppercase">To get in touch with you:</h4>
        <p>We can contact you on your request on your provided details.</p>
        
        <h4 class="secondary-color text-uppercase">How do we safeguard your information?</h4>
        <p>We have taken different steps to maintain the security of your personal details when you provide, enter or access your personal information. Your details are stored in a password encrypted database.</p>
        
        <h4 class="secondary-color text-uppercase">Do we utilize Cookies?</h4>
        <p>Yes. Cookies are tiny files that a website transfers to its visitor’s hard drive via your web browser (If allowed) that allow the sites to identify your browser and record certain information. We utilize cookies to help us remember, understand and store your preferences for future and put together the site traffic and site interaction so we can enhance site experiences in coming future. Such information is recorded in an anonymous form that does not carry any of your personally identifiable information. We don’t collect any personally identifiable information unless it’s submitted directly via the fill-in form.</p>
        
        <h4 class="secondary-color text-uppercase">Remarketing</h4>
        <p>The cookies can be used to show remarketing advertisements depending upon the user’s old visits to our website across the internet.
Remarketing advertisements are shown through Google’s display-advertisement network. Visitors can stop the Google’s use of cookies anytime by accessing Google Ads Preference Manager.
</p>

<h4 class="secondary-color text-uppercase">Do we disclose these details to outside parties?</h4>
<p>We do not distribute, sell, trade or otherwise transfer your personally identifiable information to outside parties. That does not include the third party entities that assist us in managing our website, operating our business or serving you, so as long as these parties consent to keeping the details confidential. We might disclose the details to comply with the law, enforce our site policies or safeguard our’s or other’s rights, property and safety. The non-personally identifiable information however can be shared with different parties for advertising and other purposes.</p>

<h4 class="secondary-color text-uppercase">Third party links</h4>
        <p>Sometimes, we might include or allow third party products or services on our site. Such sites have different privacy policies. We will not be held responsible for the content or activities of these third party sites.</p>
        
        <h4 class="secondary-color text-uppercase">Online Privacy Policy Only</h4>
        <p>This privacy policy is only applicable to details gathered through website and not the details gathered offline.</p>
        
        <h4 class="secondary-color text-uppercase">Terms and Conditions:</h4>
        <p>Kindly also visit our Terms and Conditions sections and familiarize yourself with the disclaimers and limitations of ability regarding the usage of our website.</p>
        
         <h4 class="secondary-color text-uppercase">Your Consent</h4>
        <p>You agree to our website’s privacy policy by using our site.</p>
        
        <h4 class="secondary-color text-uppercase">Alterations in our Privacy Policy</h4>
        <p>In case we change our privacy policy, we will keep posting these changes here. We encourage you to keep checking this page for any changes.</p>
        
        <h4 class="secondary-color text-uppercase">This website uses Google AdWords</h4>
        <p>This website uses the Google AdWords remarketing service to advertise on third party websites (including Google) to previous visitors to our site. It could mean that we advertise to previous visitors who haven’t completed a task on our site, for example using the contact form to make an enquiry. This could be in the form of an advertisement on the Google search results page, or a site in the Google Display Network. Third-party vendors, including Google, use cookies to serve ads based on someone’s past visits to the Inceptive Design’s website. Of course, any data collected will be used in accordance with our own privacy policy and Google’s privacy policy.</p>
        
        <p>Opt out of a third-party vendor’s use of cookies by visiting the Network Advertising Initiative opt-out page.</p>
        
        <h4 class="secondary-color text-uppercase">Contact Us</h4>
        <p>In case you have any queries in reference to this privacy policy kindly reach us using the details below:</p>
        
        <h4 class="secondary-color text-uppercase">Website:</h4>
        <p><a href="http://www.inceptivedesigns.com/">www.inceptivedesigns.com</a></p>
        
        <h4 class="secondary-color text-uppercase">Email:</h4>
        <p><a href="mailto:info@inceptivedesigns.com">info@inceptivedesigns.com</a></p>
        
    </div>
  </div>
</section>
<div class="clearfix"></div>
		
<?php include 'footer.php';?>