<?php include 'header.php';?>
        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="assets/images/digital-marketing/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>DIGITAL MARKETING</span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="services-tabs" style="background-image: url(assets/images/digital-marketing/seo-tab-bg.jpg); background-size: cover; background-repeat: no-repeat;">
  <div class="row">
    <div class="col-md-4 col-sm-5 remove-padding">
      <div class="serv-sidebar">
        <div class="serv-sidebar-inner pt-80">
          <h2 class="white-color text-uppercase">Digital marketing services</h2>
          <p class="white-color mt-30 mb-40">Create a strong persona of your company by our digital marketing services. Today, users expect a hyper-personalized, 24/7 availability and a smooth experience. At BT Software, we provide support to your marketing efforts with personalized tactics to engage, attract and retain your potential customers.</p>
          <ul class="nav nav-tabs">
            <li class="active" data-toggle="tab" href="#menu1"><a href="#">Search Engine Optimization <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu2"><a href="#">Search Engine Marketing <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu3"><a href="#">Advertisement Campaigns <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu4"><a href="#">Email Marketing <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
            <li data-toggle="tab" href="#menu5"><a href="#">Social Media Marketing <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
          </ul>
        </div>
        <!--serv-sidebar-inner-->
        
        <div class="clearfix"></div>
      </div>
      <!--serv-sidebar--> 
      
    </div>
    <div class="col-md-8 col-sm-7 remove-padding">
      <div class="tab-content">
        <div id="menu1" class="tab-pane fade in active">
          <h1 class="secondary-color text-uppercase">SEARCH 
ENGINE OPTIMIZATION</h1>
          <p class="mt-20 animated bounceInRight">Regardless of your location and language, BT Software will make your business visible with our exceptional SEO services. <br>
			We make sure that you generate quality traffic on your website. Our dedicated SEO team will help your brand grow and enable you to develop better strategies and meet the right audience. <br><br>
			With our SEO service solutions, you can get:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5 tab-content-para">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i>  Keyword Research and Competitive Analysis</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Building Link </p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i>  Website optimization </p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Content Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Activating  Public Relations</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Press Release Optimization</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Local Search Optimization</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Web Marketing Analytics</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images//digital-marketing/seo-tab-img.png" alt="" class="img-responsive lazy"></div>
          </div>
        </div>
        <div id="menu2" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">SEARCH 
ENGINE marketing</h1>
          <p class="mt-20 animated bounceInRight">Maximize your sales with our SEM solutions. We provide digital marketing services ensuring to enhance your brand awareness by generating a huge traffic on your websites. <br>
			With the power of Internet users on search engines (mainly Google), receives every second in favour of your business. We offer guaranteed success with our specified services. If you’re looking to generate sales lead and customer engagement, then BT Software’s SEM solutions are the best approach for you. <br><br>
			We help you create:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> ROI Attentive  Strategies</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> AdWords Certified Partner</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Leveraging Search & Ad Networks</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Landing Page & Ad Design</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images//digital-marketing/serach-engine-marketing.png" alt="" class="lazy"></div>
          </div>
        </div>
        <div id="menu3" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Advertisement Campaigns </h1>
          <p class="mt-20 animated bounceInRight">Digital advertisements have expanded dramatically to different forms to ensure the widespread of your message. At <span class="text-grad">BT Software</span> – we make sure that all your ads reach your potential clients at the right time and through the right medium. We specialize in creating text ad, pictorial ads, animated video ads, and website ad campaigns. Using a target-based strategy we put a major emphasis on client’s preferred audience to create the campaigns.<br>
			Our advertising campaign services include:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Custom Campaign Strategy</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Qualified Sales Leads</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Quantity Focused Strategies</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Indisputable ROI</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images//digital-marketing/advertisement-campaigns.png" alt="" class="lazy"></div>
          </div>
        </div>
        <div id="menu4" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">Email marketing</h1>
          <p class="mt-20 animated bounceInRight">When it comes to maintaining online relationships with clients – Email marketing is a mainstream tool. However, not all emails you sent and receive are worth looking at. This is where <span class="text-grad">BT Software</span> comes in. Our approach is to personalize your email marketing strategies. Our digital marketers make sure that you marketing email never reaches the spam folder. We will help you create quirky and interesting emails.<br>
			Our Email Marketing Services include:
		  </p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Unique Email Content and Formatting</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> List Development</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Creative Automated Lead Nurturing</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Campaign Management</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
              
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images//digital-marketing/email-Marketing.png" alt="" class="lazy"></div>
          </div>
        </div>
        <div id="menu5" class="tab-pane fade in">
          <h1 class="secondary-color text-uppercase animated bounceInDown">soical media marketing</h1>
          <p class="mt-20 animated bounceInRight">In the few year, social media has become the fastest growing marketing tool across the world. It has dramatically filled the gap between brands and clients. Whether you are a start-up or a multi-national – at <span class="text-grad">BT Software</span>, we have a proficient team of professionals to handle your social media communications. We will help you in uncovering valuable social media insights to reach to your clients and generate 100% results in the long-run.</p>
          <p class="mt-20 animated bounceInRight">Get the maximum user engagement and outreach with our oh-so social media marketing strategies.</p>
          <div class="row mt-30 animated zoomIn">
            <div class="col-md-5">
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Create and Enhance Your Follower Base</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Customer Engagement</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Precise and Rich Content</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Customer Insights</p>
              <p class="secondary-color font-700 text-uppercase"><i class="fa fa-angle-double-right mr-10"></i> Track It to Believe It</p>
              
              <a href="#" class="btn grad-color mt-20" data-toggle="modal" data-target="#SignupModal">SIGN UP NOW!</a>
             
            </div>
            <div class="col-md-7 animated bounceInRight"><img src="assets/images//digital-marketing/social-media-marketing.png" alt="" class="lazy"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--services-tabs-->
<div class="clearfix"></div>
<section class="our-client  pt-40 pb-50">
	<div class="container">
    	<div class="row text-center">
        	<div class="col-md-12">
            	<h1 class="primary-color text-uppercase">Our Partners</h1>
                <p class="hidden-xs">We are proud to have partnered with some of the region's and the world's leading companies, <br> bringing their brands to life and transforming their people and customer experiences.</p>
            </div>
        </div>
        	<div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl1.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl1.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl2.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl2.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl3.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl3.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl4.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl4.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl5.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl5.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl6.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl6.jpg" style="display: block;"></div>
            </div>

        	<div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl7.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl7.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl11.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl11.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl12.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl12.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl13.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl13.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl14.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl14.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl15.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl15.jpg" style="display: block;"></div>
            </div>


            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl16.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl16.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl17.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl17.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl18.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl18.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl19.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl19.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl20.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl20.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl21.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl21.jpg" style="display: block;"></div>
            </div>


            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl22.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl22.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl23.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl23.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl24.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl24.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl25.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl25.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl26.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl26.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="assets/images/clients/cl27.jpg" class="img-responsive lazy" width="430" height="267" src="assets/images/clients/cl27.jpg" style="display: block;"></div>
            </div>

        <!--<div class="row mt-50 text-center">
        	<div class="col-md-12">
            	<a href="/portfolio/" class="btn grad-color mt-20">VIEW MORE</a>
            </div>
        </div>-->
    </div>
</section><div class="clearfix"></div>
	
		<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section>		
 <section class="packages portfolio-sec">
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">Digital Marketing Packages We Offer</h1>
                <p class="wow fadeInUp">BT Software Design believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget. </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box one">
      <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
        <div class="price-box">
          <h6><strike>$1000</strike> $500</h6>
          <p><strong class="text-uppercase">Basic </strong> <br>(Search Engine Optimization)</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="one">
        <div class="package-list">
          <ul>
            <li>Exhaustive Keyword List Presentation</li>
            <li>Identify Best-suited Keywords for Your Business</li>
            <li>Identify Best-suited Keywords for Your Market</li>
            <li>Latent Semantic Analysis</li>
            <li>Competitor Keyword Research</li>
            <li>Analyzing Most Suited Keywords</li>
            <li>Mutual keyword finalization</li>
            <li>Top 3 Listing Guarantee* </li>
            <li>1st Page Guarantee* </li>
            <li>First 2 Pages Guarantee* </li>
            <li>Website Structure Analysis</li>
            <li>dentify Broken Links, Error Pages, Duplicate Content, etc</li>
			<li class="grad-color">WEBMASTER</li>
			<li>Google Webmaster Setup</li>
			<li>Yahoo Webmaster Setup</li>
			<li>Bing Webmaster Setup</li>
			<li class="grad-color">ON-PAGE OPTIMIZATION</li>
			<li>Meta Description Optimization</li>
			<li>Page Title & Header Tags Optimization</li>
			<li>Creation & Validation of Robots.txt</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
        </div>
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box one">
      <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
        <div class="price-box">
          <h6><strike>$2000</strike> $1000</h6>
          <p><strong class="text-uppercase">Professional </strong> <br>(Search Engine Optimization)</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="one">
        <div class="package-list">
          <ul>
            <li>Exhaustive Keyword List Presentation</li>
            <li>Identify Best-suited Keywords for Your Business</li>
            <li>Identify Best-suited Keywords for Your Market</li>
            <li>Latent Semantic Analysis</li>
            <li>Competitor Keyword Research</li>
            <li>Analyzing Most Suited Keywords</li>
            <li>Mutual keyword finalization</li>
			<li>Top 3 Listing Guarantee* </li>
            <li>1st Page Guarantee* </li>
            <li>First 2 Pages Guarantee* </li>
            <li>Website Structure Analysis</li>
            <li>dentify Broken Links, Error Pages, Duplicate Content, etc</li>
			<li class="grad-color">WEBMASTER</li>
			<li>Google Webmaster Setup</li>
			<li>Yahoo Webmaster Setup</li>
			<li>Bing Webmaster Setup</li>
			<li class="grad-color">ON-PAGE OPTIMIZATION</li>
			<li>Meta Description Optimization</li>
			<li>Page Title & Header Tags Optimization</li>
			<li>Creation & Validation of Robots.txt</li>
			<li>Keyword Density Analyzer</li>
			<li>Spider Friendly Navigation Setup (upon request)</li>
			<li>Creation of a Spider Friendly Site Map on Request</li>
			<li>Optimization of Internal Link Structure (if needed)</li>
			<li class="grad-color">SITEMAP</li>
			<li>HTML Sitemap Creation</li>
			<li>XML Sitemap Creation</li>
			<li>XML Sitemap Submission to Google</li>
			<li>XML Sitemap Submission to Yahoo</li>
			<li>XML Sitemap Submission to Bing</li>
			<li>Fixing Spider Stoppers & Trouble Spots in Website</li>
			<li>Canonicalization for Duplicate Pages</li>
			<li>H1/H2 tags Optimization</li>
			<li>Image Alt Tags Optimization</li>
			<li>Content Re-writing with Right Keyword Density*</li>
			<li>Re-optimization and Tweaking to Enhance Positions</li>
			<li>Fat Footer Setup</li>
			<li>hCard Integration</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
            
          </ul>
        </div>
      </div>
      
	    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box one">
      <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
        <div class="price-box">
          <h6><strike>$2400</strike> $1200</h6>
          <p><strong class="text-uppercase">Premium</strong> <br>(Search Engine Optimization)</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="one">
        <div class="package-list">
          <ul>
            <li>Exhaustive Keyword List Presentation</li>
            <li>Identify Best-suited Keywords for Your Business</li>
            <li>Identify Best-suited Keywords for Your Market</li>
            <li>Latent Semantic Analysis</li>
            <li>Competitor Keyword Research</li>
            <li>Analyzing Most Suited Keywords</li>
            <li>Mutual keyword finalization</li>
			<li>Top 3 Listing Guarantee* </li>
            <li>1st Page Guarantee* </li>
            <li>First 2 Pages Guarantee* </li>
            <li>Website Structure Analysis</li>
            <li>dentify Broken Links, Error Pages, Duplicate Content, etc</li>
			<li class="grad-color">WEBMASTER</li>
			<li>Google Webmaster Setup</li>
			<li>Yahoo Webmaster Setup</li>
			<li>Bing Webmaster Setup</li>
			<li class="grad-color">ON-PAGE OPTIMIZATION</li>
			<li>Meta Description Optimization</li>
			<li>Page Title & Header Tags Optimization</li>
			<li>Creation & Validation of Robots.txt</li>
			<li>Keyword Density Analyzer</li>
			<li>Spider Friendly Navigation Setup (upon request)</li>
			<li>Creation of a Spider Friendly Site Map on Request</li>
			<li>Optimization of Internal Link Structure (if needed)</li>
			<li class="grad-color">SITEMAP</li>
			<li>HTML Sitemap Creation</li>
			<li>XML Sitemap Creation</li>
			<li>XML Sitemap Submission to Google</li>
			<li>XML Sitemap Submission to Yahoo</li>
			<li>XML Sitemap Submission to Bing</li>
			<li>Fixing Spider Stoppers & Trouble Spots in Website</li>
			<li>Canonicalization for Duplicate Pages</li>
			<li>H1/H2 tags Optimization</li>
			<li>Image Alt Tags Optimization</li>
			<li>Content Re-writing with Right Keyword Density*</li>
			<li>Re-optimization and Tweaking to Enhance Positions</li>
			<li>Fat Footer Setup</li>
			<li>hCard Integration</li>
			<li>Local Search Website Optimization</li>
			<li>Local Search Engine Submission</li>
			<li>Google Places Optimization</li>
			<li>Bing Local Listing Center Optimization</li>
			<li>Trusted News Sites (CNN, Yahoo Finance, NBC, etc)</li>
			<li>EDU & .Gov Sites (Harvard, Standford.edu, etc)</li>
			<li>Premium Website Directories</li>
			<li>Niche Article Directories</li>
			<li>Web 2.0 properties</li>
			<li>Social Network Sites</li>
			<li>High Page Rank Blogs</li>
			<li>Social Bookmarking Sites</li>
			<li>Wiki Sites <br>(Wikipedia.org, Wikimedia.com)</li>
			<li>Video Sharing Sites <br>(YouTube, Vimeo, etc)</li>
			<li>Google "Penguin" Friendly Sources & Variation</li>
			<li>Full Link Reports for Your Review</li>
			<li>Monthly Guaranteed High Quality Links to Websites</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
        </div>
      </div>
      
	   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="clearfix"></div>
  
</div>
                       
                </div>
              </div>
            </div>
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
                <h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>              </div>
            </div>
          </div>
        </section>

        <div class="clearfix"></div>
		
		
		<section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT Software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT Software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT Software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="/blog/blog1"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog2">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog3">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog4/">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
        </section>		
		
        <?php include 'footer.php';?>