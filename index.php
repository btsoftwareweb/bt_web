<?php

include 'header.php';
include 'mainfolder.php';
if(isset($_POST['chat_btn']))
{
    $_chat_btn = 2;
}
?>

<section id="tt-home-carousel" class="home-video-slider carousel slide carousel-fade trendy-slider control-one">
        <div class="homepage-hero-module">
			<div class="home-video-container">
				
				<video autoplay loop muted class="fillWidth" id="vid">
					<source src="video/launch.mp4" type="video/mp4" />
					<source src="video/launch.webm" type="video/webm" />
				</video>
				<div class="poster hidden">
					<img src="video/launch.jpg" alt="">
				</div>
			</div>
		</div>
		<div class="carousel-caption">
            <h1 class="animated fadeInDown delay-1"><span>Creativity</span>. Brand. <br>
				Design. <span>Technology</span>. </h1>
            <p class="animated fadeInDown delay-3">Creating Exceptional Brand Experiences</p>
                      <!---->
        </div>
</section> <!-- /#home -->

<!--Mobile Banner Start-->
<section class="home-mobile-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banr-txt">
					<h1>Interactive <span>websites</span> that <span>engage</span> visitors</h1>
					<p>We have a blend of both design and development expertise that gives us the capabilities of building some complex and <strong>custom web solutions</strong> for you.</p>
					<ul>
						<li>Responsive and User-Centric Websites</li>
						<li>Supreme Customer Satisfaction</li>
						<li>100% Ownership  Rights</li>
                    </ul>
					<div class="ban-cta-line">
						<p class="chat-avail"><a href="javascript:$zopim.livechat.window.show();">Chat Now To Avail Discounts</a></p>
						<p class="ban-nomber"><a href="tel:+1 917 475 0802" > <span> Call Us:</span> +1 917 475 0802</a></p>
						<p class="ban-email"><a href="mailto:info@Btsoftwaredesigns.com"><span> Email:</span> info@Btsoftwaredesigns.com</a></p>
						
					</div>
					<div class="clearfix"></div>
					<div class="banner-bot">
                    <ul>
                      <li><a href="#"><img src="assets/images/rateimg.png"></a></li>
                      <li><a href="#"><img src="assets/images/gpartner.png"></a></li>
                      <li><a href="#"><img src="assets/images/binsider.png"></a></li>
                    </ul>
                  </div>
				</div>
			</div>
		</div>
	</div>
	<section class="get-start">
<div class="container">
<div class="row">
    
	<div class="col-md-8">
		<div class="second wow fadeInLeft animated">
			<h2>Chat With Us To <br><span>Avail 50% Discount</span></h2>
			<p>100% Money BACK GUARANTEE</p>
			<div class="row">
				<div class="col-md-12 col-xs-12">
				  <div class="contact_ecomm wow fadeInUp">
					<div class="conform" id="banform">
					  <form name="contact" class="jform" action = "abc" method="POST">
						<div class="row">
						  <div class="col-md-6 col-xs-12">
							<div class="field">
							  <input type="text" class="required" name="cn" placeholder="Name" maxlength="70">
							</div>
						  </div>
						  <div class="col-md-6 col-xs-12">
							<div class="field">
							  <input type="text" name="ce" class="email required" placeholder="Email" maxlength="100">
							</div>
						  </div>
						</div>
						<div class="row">
						  <div class="col-md-6 col-xs-12">
							<div class="field">
							  <select name="pc" class="countrylist required">
							  </select>
							</div>
						  </div>
						  <div class="col-md-2 col-xs-12">
							<div class="field code">
							  <input name="cpcode" disabled="" type="text">
							</div>
						   </div>
						   <div class="col-md-4 col-xs-12">
							<div class="field number">
							  <input name="cp" class="required number" type="number" minlength="1" maxlength="13" value="" placeholder="Phone Number">
							</div>
						   </div> 
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 text-center">
								<input type="hidden" value="Lp-Footer contact form" name="sfp">
								<input type="hidden" value="index.php" name="su">
								<input type="hidden" name="ctry" value="Solomon Islands">
								<input type="hidden" value="/" name="pth">
								<input type="hidden" value="1" name="tp">
								<input type="hidden" value="2" name="ft" />
								<input type="hidden" name="ru" value="">
								<input type="submit" name="chat_btn" value="Submit" class="grad-color">
								
							</div>
						</div>
					  </form>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-2"></div>
</div>
</div>

</section>
<div class="clearfix"></div></section>
<!--Mobile Banner End-->
<div class="desktop-form">
	<section class="get-start">
<div class="container">
<div class="row">
	<div class="col-md-8">
		<div class="second wow fadeInLeft animated">
			<h2>Chat With Us To <br><span>Avail 50% Discount</span></h2>
			<p>100% Money BACK GUARANTEE</p>
			<div class="row">
				<div class="col-md-12 col-xs-12">
				  <div class="contact_ecomm wow fadeInUp">
					<div class="conform" id="banform-desktop">
					  <form class="jform"  method="POST">
						<div class="row">
						  <div class="col-md-6 col-xs-12">
							<div class="field">
							  <input type="text" class="required" name="cn" placeholder="Name" maxlength="70">
							</div>
						  </div>
						  <div class="col-md-6 col-xs-12">
							<div class="field">
							  <input type="text" name="ce" class="email required" placeholder="Email" maxlength="100">
							</div>
						  </div>
						</div>
						<div class="row">
						  <div class="col-md-12 col-xs-12">
							<div class="field">
							  <select name="order_service" class="countrylist required">
								<option>option 1</option>
								<option>option 2</option>
								<option>option 2</option>
                              </select> 
							</div>
						  </div>
						 
						   <div class="col-md-12 col-xs-12">
							<div class="field number">
							    <input type="tel" id="mobile-number" placeholder="e.g. +1 702 123 4567">
							  <input name="cp" class="required number" type="number" minlength="1" maxlength="13" value="" placeholder="Phone">
							</div>
						   </div> 
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 text-center">
								<input type="submit" name="chat_btn" value="Submit" class="grad-color">
							</div>
							<div class="col-md-12 col-xs-12 text-center">
							    <span style= "color:red;">
								<?php 
								if($_chat_btn == 2)
								    {
								        echo "Your request has been submitted successfuly!";
								        $_chat_btn = 3;
								    }
								    ?>
								    </span>
							</div>
						</div>
					  </form>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-2"></div>
</div>
</div>

</section>
<div class="clearfix"></div></div>


        <section class="short-desc text-center">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <p class="wow fadeInUp"><span class="text-grad text-uppercase">Bt software</span> – a creative web design agency with a sole purpose to develop exclusive things for our beloved clients. We believe in building meaningful and long-term relationships with our clients and their brands through our creative designs, web development and engaging software solutions. </p>
					<p class="wow fadeInUp">We’re a team of thinkers, coders, developers, go-getters and designers. Tell us your problem we'll fix it with full attention and dedication. </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/contact-us.php" class="btn grad-color wow fadeInUp">CONTACT US</a>
              </div>
            </div>
          </div>
        </section>

        <section class="about-sec">
          <img src="assets/images/what-bg.png" class="shape about">
          <div class="container">
			<!--responsive Tabs hide on desktop-->
			<div class="row">
				<div class="col-md-8">
					<ul class="nav nav-tabs wow fadeInUp mbile-servi-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#webd" role="tab" data-toggle="tab">
								<h4>Website <span>Development</span></h4>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#mbleapp" role="tab" data-toggle="tab">
								<h4>Mobile <span>App Development</span></h4>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#softdev" role="tab" data-toggle="tab" >
								<h4>Software <span>Development</span></h4>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#cop-idt" role="tab" data-toggle="tab" >
								<h4>Corporate <span>Identity</span></h4>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#cont-wrt" role="tab" data-toggle="tab" >
								<h4>Content <span>Writing</span></h4>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#dig-mark"" role="tab" data-toggle="tab" >
								<h4>Digital <span>Marketing</span></h4>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#vdo-animt" role="tab" data-toggle="tab" >
								<h4>Video <span>Animation</span></h4>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#lgo-des" role="tab" data-toggle="tab" >
								<h4>Logo <span>Design</span></h4>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!--responsive Tabs hide on desktop-->
            <div class="row">
				<div class="col-md-8">
					<h2 class="text-grad text-uppercase wow fadeInLeft"><b>SERVICES</b> WE ARE <br> PROUD TO <b>OFFER</b></h2>
					<ul class="nav nav-tabs wow fadeInUp" role="tablist">
						<img src="assets/images/home-services.png" class="home-services">
						<li role="presentation" class="active first-1 ">
							<a href="#webd" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/web-dev-icon.png">
									<h5>Website <br>Development</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="second">
							<a href="#mbleapp" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/mobile-ap-dev.png">
									<h5>Mobile App <br>Development</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="third">
							<a href="#softdev" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/software-icon.png">
									<h5>Software <br>Development</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="forth">
							<a href="#cop-idt" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/corporate-icon.png">
									<h5>Corporate <br> Identity</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="five">
							<a href="#cont-wrt" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/content-icon.png">
									<h5>Content <br>Writing</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="six">
							<a href="#dig-mark" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/digital-icon.png">
									<h5>Digital <br>Marketing</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="seven">
							<a href="#vdo-animt" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/video-icon.png">
									<h5>Video <br>Animation</h5>
								</div>
							</a>
						</li>
						<li role="presentation" class="eight">
							<a href="#lgo-des" role="tab" data-toggle="tab">
								<div class="wow flipInY">
									<!--<span class="">&nbsp;</span>-->
									<img src="assets/images/home-service-icons/logo-icon.png">
									<h5>Logo <br>Design</h5>
								</div>
							</a>
						</li>
						<!--<li role="presentation" class="active first-1">
							<a href="#webd" role="tab" data-toggle="tab">
								<div class="box one wow flipInY">
									<img src="assets/images/web-dev.png">
									<h4>Website Design & Development</h4>
								</div>
							</a>
						</li>
						<li role="presentation" class="first-1">
							<a href="#apde" role="tab" data-toggle="tab">
								<div class="box two wow flipInY" data-wow-delay="0.5s">
									<img src="assets/images/app-dev.png">
									<h4>App Development</h4>
								</div>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#bran" role="tab" data-toggle="tab" >
								<div class="box three wow flipInY" data-wow-delay="0.5s">
									<img src="assets/images/branding.png">
									<h4>Branding</h4>
								</div>
							</a>
						</li>
						<li role="presentation" class="Second">
							<a href="#vdo-anim" role="tab" data-toggle="tab" >
								<div class="box four wow flipInY" data-wow-delay="0.5s">
									<img src="assets/images/video-anim.png">
									<h4>Video Animation</h4>
								</div>
							</a>
						</li>--->
					</ul>
			</div>
              <div class="col-md-4">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane in active desc logo-pricing" id="webd">
							<h1 class="wow fadeInUp">Website Design & Development</h1>
							<p class="wow fadeInUp"><span class="text-grad">Bt software</span> started its operations as a Design and Web Development company. We offer custom website designs services that are built to provide scalability to your business and generate valuable leads. We maintain design consistency throughout to ensure that your brand image is highlighted appropriately.</p>

							<p class="wow fadeInUp">From website design and development to re-designing, we have a team of experts to handle your most complex and compound projects. </p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/web-development.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="mbleapp">
							<h1 class="wow fadeInUp">Mobile App Development</h1>
							<p class="wow fadeInUp">We offer cross-platform customized mobile app development. We are proud to bear the testimony of over 100 successful mobile projects. Regardless of project size and complexity <span class="text-grad">Bt software</span> ensures to deliver a smooth app development process. We have a team of exceptional analysts including UI/UX experts and developers who are well-versed in building both native and cross-platform apps that our beloved clients need and want. </p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/mobile-apps-development.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="softdev">
							<h1 class="wow fadeInUp">Software Development</h1>
							<p class="wow fadeInUp">We provide complete Software Development Services to our clients. Our process starts from requirement analysis to wireframing, prototyping, designing, development, quality assurance, deployment and instant support. <br>
							Our professionals will help you realize your solution flawlessly by providing customized software development with strategic insights to generate persuasive brand engagement, higher conversions and consistent progressive results. 
							</p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/software-development.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="cop-idt">
							<h1 class="wow fadeInUp">Corporate Identity</h1>
							<p class="wow fadeInUp">Enhance your business with our outstanding corporate identity designing services. A corporate identity will evoke emotions and express your Company’s story with your potential clientele. We have a team of best branding experts to give you the unique identity solution. With our corporate identity solutions – you can bring new life to tired brands or unique push to a new brand. 
							</p>
							<a href="corporate-identity-design/stationery-design" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="cont-wrt">
							<h1 class="wow fadeInUp">Content Writing</h1>
							<p class="wow fadeInUp"><span class="text-grad">Bt software</span> provides quality content writing services. We’ve handpicked every one of our writers to make sure you receive nothing but the best. We offer unique content with zero percent plagiarism, no grammatical errors and fine use of keywords. Our content writing services include: 
							<ul class="wow fadeInUp">
							<li>•	Website Content</li>
							<li>•	Article Writing</li>
							<li>•	Blog Posts</li>
							<li>•	SEO Content</li>
							<li>•	Product Description</li>

							</ul>

							</p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/copy-writing.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="dig-mark">
							<h1 class="wow fadeInUp">Digital Marketing</h1>
							<p class="wow fadeInUp"><span class="text-grad">Bt software's</span> digital marketing solutions provide support to your marketing efforts with personalized tactics to engage, attract and retain your potential customers.  We will help you create an impression and a positive connection in every interaction you make with your clients. We are specialized in SEO, SEM, SMM, Email Marketing and Advertising Campaigns. Our digital marketing experts at your service to create a smooth experience for your clients and will also help you enhance your <span class="text-grad">brand awareness, customer engagement, ROI, and lead generations. </span>
							</p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/digital-marketing.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="vdo-animt">
							<h1 class="wow fadeInUp">Video animation</h1>
							<p class="wow fadeInUp">Make engaging videos with Bt software. Our eye-catching video animation services will help you bring your brand to life. It is the easiest way to communicate and help you engage your customers. <br>
							Our dexterous animators work ingeniously without the limits to make heart-felt and striking videos. <span class="text-grad">Bt software's</span> video animation services include promotional brand videos, testimonials, editing and post-productions, white board explainers, company introduction, particular product/services and what not!

							</p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/video-animation.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
						<div role="tabpanel" class="tab-pane in desc logo-pricing" id="lgo-des">
							<h1 class="wow fadeInUp">Logo Design</h1>
							<p class="wow fadeInUp">Create your identity with the help of our creative Logo designs. At <span class="text-grad">Bt software</span>, we have a record to providing unique logo designs to over <span class="text-grad">2000</span> satisfied clients. <br>
							A logo is not just a small detail – it is the IMAGE of your business, as it creates the first impression of your business. Our designers will craft unique layouts that will make you distinctive from others 

							</p>
							<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/logo-design.php" class="btn grad-color wow fadeInUp">READ MORE</a>
						</div>
					</div>
				<!--<div class="owl-carousel testimonials">
                    <div class="item desc">
                        <h1 class="wow fadeInUp">What we do</h1>
						<p class="wow fadeInUp">Bt software Design has authenticated itself as a logo and web development company innovating with graphics at all possible levels. The dedication and perseverance of the team is obvious while originating high-quality illustrations that are customized, impressive and alluring. Bt software Design, is reflected as the epitome of perfection by its clients and hence, our clienteles are the living proof of the premium services we provide.</p>
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/services.php" class="btn grad-color wow fadeInUp">READ MORE</a>
                    </div>
					<div class="item desc">
                        <h1 class="wow fadeInUp">What we do</h1>
						<p class="wow fadeInUp">Bt software Design has authenticated itself as a logo and web development company innovating with graphics at all possible levels. The dedication and perseverance of the team is obvious while originating high-quality illustrations that are customized, impressive and alluring. Bt software Design, is reflected as the epitome of perfection by its clients and hence, our clienteles are the living proof of the premium services we provide.</p>
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/services.php" class="btn grad-color wow fadeInUp">READ MORE</a>
                    </div>
					<div class="item desc">
                        <h1 class="wow fadeInUp">What we do</h1>
						<p class="wow fadeInUp">Bt software Design has authenticated itself as a logo and web development company innovating with graphics at all possible levels. The dedication and perseverance of the team is obvious while originating high-quality illustrations that are customized, impressive and alluring. Bt software Design, is reflected as the epitome of perfection by its clients and hence, our clienteles are the living proof of the premium services we provide.</p>
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/services.php" class="btn grad-color wow fadeInUp">READ MORE</a>
                    </div>
                </div>-->
              </div>
            </div>
          </div>
        </section>

        <section class="portfolio-sec">
          <img src="assets/images/client-bg.png" class="shape">
          <div class="container">
            <h1 class="wow fadeInUp">We are serving <span>2000+</span> Clients</h1>
<p class="wow fadeInUp">We create experiences that transform brands & grow businesses. We have enriched some of the biggest brands and our partnerships with them have caused the creation of some outstanding outcomes. </p>			<div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs wow fadeInUp" role="tablist">
						<li role="presentation" class="active"><a href="#website-port" role="tab" data-toggle="tab">Website</a></li>
						<li role="presentation"><a href="#logo-port" role="tab" data-toggle="tab">Logo</a></li>
						<li role="presentation"><a href="#corp-iden-port" role="tab" data-toggle="tab">Branding</a></li>
						<li role="presentation"><a href="#vid-anim-port" role="tab" data-toggle="tab">Video <span>Animation</span></a></li>
						<li role="presentation"><a href="#cpy-write-port" role="tab" data-toggle="tab">Copy <span>Writing</span></a></li>
						<li role="presentation"><a href="#app-des-port" role="tab" data-toggle="tab">App <span>Design</span></a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active logo-pricing" id="website-port">
							<div class="row logo logo-portfolio">
	<div class="row mr">
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/1.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/1-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/2.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/2-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/4.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/4-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/5.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/5-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/6.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/6-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/7.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/7-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/8.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/8-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/9.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/9-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/10.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/10-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/11.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/11-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/12.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/12-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/13.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/13-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/14.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/14-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/15.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/15-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/16.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/16-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/17.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/17-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/18.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/18-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/19.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/19-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/20.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/20-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/21.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/21-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="wow fadeInLeft ">
				<div class="hovereffect-portfolio">
					<img src="assets/images/web-design/22.jpg" alt="image" class="img-responsive center-block">
					<div class="overlayPort">
						<ul class="info text-center list-inline">
							<li class="last">
								<a href="assets/images/web-design/22-hvr.jpg" class="fancybox-pop">
									<i class="ion-android-search"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>  
</div>						</div>
						<div role="tabpanel" class="tab-pane fade logo-pricing" id="logo-port">
							<div class="tabs logo logo-portfolio">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#illus-logo" data-toggle="tab" aria-expanded="false"> Illustrative <span>Design</span></a></li>
		<li class=""><a href="#mascot-logo" data-toggle="tab" aria-expanded="false"> Mascot <span>Design</span></a></li>
		<!--<li class=""><a href="#3d-logo" data-toggle="tab" aria-expanded="false">3D Design </a></li>-->
		<li class=""><a href="#abs-logo" data-toggle="tab" aria-expanded="false">Abstract <span>Design</span></a></li>
		<li class=""><a href="#icon-logo" data-toggle="tab" aria-expanded="true">Iconic <span>Design</span></a></li>
		<li class=""><a href="#flat-logo" data-toggle="tab" aria-expanded="false">Flat <span>Design</span> </a></li>
		<!--<li class=""><a href="#real-logo" data-toggle="tab" aria-expanded="false">Realistic Logo Design</a></li>-->
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		
		<!--illus-logo-->
		<div id="illus-logo" class="tab-pane active">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6 ">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/2-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/3-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/8-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/10.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/10.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/11.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/11.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/12.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/12.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/13.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/13.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/14.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/14.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/15.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/15.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/16.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/16.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/17.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/17.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/illustrative/18.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/illustrative/18.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--mascot-logo-->
		<div id="mascot-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/10.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/10.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/11.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/11.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/mascots/12.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/mascots/12.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--3d-logo
		<div id="3d-logo" class="tab-pane">
			<div class="row mr">
				
			</div>
        </div>
		<!--abs-logo-->
		<div id="abs-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/1-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/2-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/4-hvr.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/abstract/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/abstract/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		<!--icon-logo-->
		<div id="icon-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/iconic/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/iconic/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
		
		<!--flat-logo-->
		<div id="flat-logo" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/1.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/1.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/2.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/2.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/3.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/3.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/4.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/4.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/5.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/5.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/6.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/6.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/7.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/7.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/8.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/8.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="wow fadeInLeft ">
						<div class="hovereffect-portfolio">
							<img src="assets/images/logo-design/flat/9.jpg" alt="image" class="img-responsive center-block">
							<div class="overlayPort">
								<ul class="info text-center list-inline">
									<li class="last">
										<a href="assets/images/logo-design/flat/9.jpg" class="fancybox-pop">
											<i class="ion-android-search"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
        </div>
	</div>
</div>
						</div>
						<div role="tabpanel" class="tab-pane fade logo-pricing" id="corp-iden-port">
							<div class="tabs logo logo-portfolio">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#stationery-design" data-toggle="tab" aria-expanded="false">Stationery <span>Design</span></a></li>
		<li class=""><a href="#social-media-design" data-toggle="tab" aria-expanded="false">Social Media <span>Design</span> </a></li>
		<li class=""><a href="#brochure-design" data-toggle="tab" aria-expanded="false"> Brochure <span>Design</span></a></li>
		<li class=""><a href="#banner-design" data-toggle="tab" aria-expanded="true">Banner <span>Design</span></a></li>
		<li class=""><a href="#promotional-design" data-toggle="tab" aria-expanded="false">Promotional <span>Design</span></a></li>
		<li class=""><a href="#mag-design" data-toggle="tab" aria-expanded="false">Magazine <span>Design</span></a></li>
	</ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--stationery-design-->
		<div id="stationery-design" class="tab-pane active">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--social-media-design-->
		<div id="social-media-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/social-media-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/social-media-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--brochure-design-->
		<div id="brochure-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img10.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img10.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img11.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img11.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/brochure-desgin/img12.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/brochure-desgin/img12.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
								
                             
</div>			</div>
        </div>
		<!--banner-design-->
		<div id="banner-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/stationary-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/corporate-identity-design/stationary-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--promotional-design-->
		<div id="promotional-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img10.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img10.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img11.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img11.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/corporate-identity-design/promotional-design/img12.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					<li class="last">
						<a href="assets/images/corporate-identity-design/promotional-design/img12.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
		<!--Magazine-Design-->
		<div id="mag-design" class="tab-pane">
			<div class="row mr">
				<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img7.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img8.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img8.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/magzine-design/img9.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/magzine-design/img9.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>			</div>
        </div>
	</div>
</div>
						</div>
						<div role="tabpanel" class="tab-pane fade logo-pricing" id="vid-anim-port">
							<div class="row video-portfolio">
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/1.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=1Q_S3nqOGU8" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/2.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=7T-yrsTQii4" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/3.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=bLv44cEw5Gk" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/4.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=7oUSaF4x5pQ" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/5.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=glAj2gdMupY" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/6.jpg" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=gBVohsdSbFE" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/7.png" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=Qlcqc1acySc" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/8.png" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=ZRbOC3MS1Lg" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="col-md-4  col-sm-6 col-xs-6 remove-padding video-pop">
		<div class="hovereffect-portfolio">
            <img src="assets/images/video-animation/portfolio/9.png" alt="image" class="img-responsive center-block">
            <div class="overlayPort">
                <ul class="info text-center list-inline">
                    <li class="first last">
                         <a class="video-play popup-youtube" href="https://www.youtube.com/watch?v=UzRsGpUO8NQ" title=""><i class="fa fa-play"></i></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	
</div>						</div>
						<div role="tabpanel" class="tab-pane fade logo-pricing" id="cpy-write-port">
							<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img1.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img1.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img2.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img2.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img3.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img3.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img4.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img4.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/copy-writing/img5.png" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/copy-writing/img5.png" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>						</div>
						<div role="tabpanel" class="tab-pane fade logo-pricing" id="app-des-port">
							<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img5-hvr.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img7.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img7-hvr.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="assets/images/app-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="assets/images/app-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>						</div>
									
					</div>
				</div>
              </div>
            </div>
			<div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL</a></div>            
          </div>
        </section>

        <section class="inspired">
          <img src="assets/images/inspired-bg.png" class="shape">
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-center">
                <h1 class="wow fadeInUp">Inspired design for the digital age</h1>
                <p class="wow fadeInUp">As design planners, we bridge the gap between planning and execution. Give us the charge – and we’ll align your goals with the needs of your customers – creating charming and excellent brand.</p>
              </div>
            </div>
            <div class="row mt-30">
              <div class="col-md-6">
                <div class="row box wow fadeInLeft">
                  <div class="col-md-3 text-center"><img src="assets/images/inspired-icon1.png"></div>
                  <div class="col-md-9">
                    <h4>Scheme</h4>
                    <p>Every idea and dimension of the project is keenly strategize to maximize its perfection. </p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row box wow fadeInRight">
                  <div class="col-md-3 text-center"><img src="assets/images/inspired-icon2.png"></div>
                  <div class="col-md-9">
                    <h4>Design</h4>
                    <p>Our designers create delight. We ensure to put attention-to-detail to maximize the appeal of every part of the project. </p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row box wow fadeInLeft">
                  <div class="col-md-3 text-center"><img src="assets/images/inspired-icon3.png"></div>
                  <div class="col-md-9">
                    <h4>QUALITY ASSURANCE</h4>
                    <p>Once completed, every project is monitored to deliver the finest quality. </p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row box wow fadeInRight">
                  <div class="col-md-3 text-center"><img src="assets/images/inspired-icon4.png"></div>
                  <div class="col-md-9">
                    <h4>Outcome</h4>
                    <p>Promising results are delivered in the labelled time frame with complete exactitude.  </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center">
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/contact-us.php" class="btn grad-color mt-30 wow fadeInLeft">BOOK A CALL</a>
                <a href="javascript:;" data-toggle="modal" data-target="#SignupModal" class="btn grad-color mt-30 wow fadeInRight">REQUEST A QUOTE</a>
              </div>
            </div>
          </div>
        </section>

        <section class="packages portfolio-sec">
          <img src="assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">Packages We Offer</h1>
                <p class="wow fadeInUp">Bt software Design believes in delivering distinctive services in competitive price models.  We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget.  </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs wow fadeInUp" role="tablist">
						  <li role="presentation" class="active"><a href="#website-pkg" role="tab" data-toggle="tab">Website</a></li>
                          <li role="presentation" ><a href="#logo-pkg" role="tab" data-toggle="tab">Logo</a></li>
                          <li role="presentation"><a href="#corp-iden-pkg" role="tab" data-toggle="tab">Branding</a></li>
                          <li role="presentation"><a href="#vid-anim-pkg" role="tab" data-toggle="tab">Video <span>Animation</span></a></li>
                          <li role="presentation"><a href="#cpy-write-pkg" role="tab" data-toggle="tab">Copy <span>Writing</span></a></li>
						  <li role="presentation"><a href="#app-design-pkg" role="tab" data-toggle="tab">App <span>Designs</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
						  <div role="tabpanel" class="tab-pane fade in active" id="website-pkg">
                            <div class="tabs">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#resp_web" data-toggle="tab" aria-expanded="true">Responsive <span>Website</span></a></li>
        <li class=""><a href="#cms_web" data-toggle="tab" aria-expanded="false">CMS <span>Website</span></a></li>
		<li class=""><a href="#ecomm_web" data-toggle="tab" aria-expanded="false">E-Comm<span>erce Development</span></a></li>
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<div id="resp_web" class="tab-pane active">
			<div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box ninteen">
				  <div class="collapse-top collapsed" data-target="#ninteen" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$800</strike> $400</h6>
					  <p><strong class="text-uppercase">Basic </strong></br>Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="ninteen">
					<div class="package-list">
					  <ul>
						<li>4 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 3 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 2 Months Web Hosting</li>
						<li><strong>FREE</strong> 2 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 1GB Web Hosting Space</li>
						<li><strong>FREE</strong> 3 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twenty">
				  <div class="collapse-top collapsed" data-target="#twenty" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1600</strike> $800</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twenty">
					<div class="package-list">
					  <ul>
						<li>8 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 5 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 4 Months Web Hosting</li>
						<li><strong>FREE</strong> 4 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 3GB Web Hosting Space</li>
						<li><strong>FREE</strong> 5 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					</ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyone">
				  <div class="collapse-top collapsed" data-target="#twentyone" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$3200</strike> $1600</h6>
					  <p><strong class="text-uppercase">Premium</strong></br>Responsive Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyone">
					<div class="package-list">
					  <ul>
						<li>16 Unique Pages Design</li>
						<li>1 Basic Contact / Inquiry Form</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li>Mobile Websites</li>
						<li>Custom Design - Exclusve Rights</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 10 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li><strong>FREE</strong> 6 Months Web Hosting</li>
						<li><strong>FREE</strong> 6 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 5GB Web Hosting Space</li>
						<li><strong>FREE</strong> 10 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box twentytwo bundle">
				  <div class="collapse-top collapsed" data-target="#twentytwo" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1500</strike> $750</h6>
					  <p><strong class="text-uppercase">Bundle </strong></br>(Logo & Web)<br>Responsive Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentytwo">
					<div class="package-list">
						<div class="col-md-4">
							<h6 class="text-grad">Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
								<li class="first">6 Unique Logo Concepts <strong>(FREE)</strong></li>
								<li>Complementary Icon <strong>(FREE)</strong></li>
								<li>Unlimited Revisions* <strong>(FREE)</strong></li>
								<li>100% Ownership Rights <strong>(FREE)</strong></li>
								<li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats <strong>(FREE)</strong></li>
								<li>Complementary Rush Delivery <strong>(FREE)</strong></li>
								<li class="last">Get Initial Concepts within 24 hours.</li>
							</ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
								<li class="first">8 Unique Pages Design</li>
								<li>1 Basic Contact / Inquiry Form</li>
								<li>Responsive ( Mobile / Tablet )</li>
								<li>Complete W3C Certified HTML</li>
								<li class="last">6 Month Maintenance/Support <strong>(FREE)</strong></li>
							</ul>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Branding</h6>
							<ul>
								<li class="first">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
								<li>Letterhead Design <strong>(FREE)</strong></li>
								<li>Envelope Design <strong>(FREE)</strong></li>
								<li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
							</ul>
						</div>
						<div class="col-xs-12">
							<ul>
								<li class="first"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>5 Stock Photos</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong> Google Friendly Sitemap</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong> 6 Months Web Hosting</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Free Domain Registration</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>3GB Web Hosting Space</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Email Accounts</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							</ul>
						</div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>
        </div>
		<div id="cms_web" class="tab-pane">
            <div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box twentythree">
				  <div class="collapse-top collapsed" data-target="#twentythree" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$800</strike> $400</h6>
					  <p><strong class="text-uppercase">Basic </strong></br>CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentythree">
					<div class="package-list">
					  <ul>
						<li>5 Unique Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li><strong>FREE</strong> 2 Months Web Hosting</li>
						<li><strong>FREE</strong> 2 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 1GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 3 Email Accountst</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyfour">
				  <div class="collapse-top collapsed" data-target="#twentyfour" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1600</strike> $800</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyfour">
					<div class="package-list">
					  <ul>
						<li>10 Unique Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li>Text Editor Module</li>
						<li>Photo Gallery Module</li>
						<li>Google Map Module</li>
						<li><strong>FREE</strong> 4 Months Web Hosting</li>
						<li><strong>FREE</strong> 4 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 3GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 5 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyfive">
				  <div class="collapse-top collapsed" data-target="#twentyfive" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$2400</strike> $1200</h6>
					  <p><strong class="text-uppercase">Premium</strong></br> CMS Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyfive">
					<div class="package-list">
					  <ul>
						<li>Unlimited Pages Design</li>
						<li>Contact Form</li>
						<li>Social Media Integration</li>
						<li>Visual Composer Module</li>
						<li>Photo Gallery Module</li>
						<li>Google Map Module</li>
						<li>Sitemap Module</li>
						<li>Blog Integration</li>
						<li>Responsive ( Mobile / Tablet )</li>
						<li><strong>FREE</strong> 6 Months Web Hosting</li>
						<li><strong>FREE</strong> 6 Months Free Domain Registration</li>
						<li><strong>FREE</strong> 5GB Web Hosting Space</li>
						<li><strong>FREE</strong> 1 MySQL Database</li>
						<li><strong>FREE</strong> Control Panel Access</li>
						<li><strong>FREE</strong> FTP Access</li>
						<li><strong>FREE</strong> Manage Your Own Content</li>
						<li><strong>FREE</strong> 10 Email Accounts</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					  </ul>
					</div>
				  </div>
				  <p class="add-on" style="color: #ed145b !important;">Unlimited Pages</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box twentysix bundle">
				  <div class="collapse-top collapsed" data-target="#twentysix" aria-expanded="true">
					<div class="price-box">
					  <h6> <strike>$1500</strike> $750</h6>
					  <p><strong class="text-uppercase">Bundle</strong></br> (Logo & Web)<br>CMS Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentysix">
					<div class="package-list">
						<div class="col-md-4">
							<h6 class="text-grad">Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">8 Unique Logo Concepts <strong>(FREE)</strong></li>
                                <li>Complementary Icon<strong>(FREE)</strong></li>
                                <li>Unlimited Revisions*<strong>(FREE)</strong></li>
                                <li>100% Ownership Rights<strong>(FREE)</strong></li>
                                <li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats<strong>(FREE)</strong></li>
                                <li class="last">Stationery Design <strong>(FREE)</strong></li>
                            </ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">10 Unique Pages Design</li>
                                <li>Contact Form</li>
                                <li>Social Media Integration</li>
                                <li>Text Editor Module</li>
                                <li>Photo Gallery Module</li>
                                <li>Google Map Module</li>
                                <li>6 Month Maintenance/Support <strong>(FREE)</strong></li>
                                <li class="last">Secure Sockets Layer(SSL) <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-md-4">
							<h6 class="text-grad">Branding</h6>
							<ul>
                                <li class="first active">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
                                <li>Letterhead Design <strong>(FREE)</strong></li>
                                <li>Envelope Design <strong>(FREE)</strong></li>
                                <li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-xs-12">
                            <ul>
                                <li class="first active"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Manage Your Own Content</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Web Hosting</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>6 Months Free Domain Registration</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>3GB Web Hosting Space</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>1 MySQL Database</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Control Panel Access</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>FTP Access</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Email Accounts</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
                             </ul>
                        </div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>                            

        </div>
		<div id="ecomm_web" class="tab-pane ">
            <div class="row mr">
			  <div class="col-md-4">
				<div class="pricing-box twentyseven">
				  <div class="collapse-top collapsed" data-target="#twentyseven" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$1690</strike> $845</h6>
					  <p><strong class="text-uppercase">Basic</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyseven">
					<div class="package-list">
					  <ul>
						<li>5 Unique Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Upto 100 Products</li>
						<li>Upto 5 Categories</li>
						<li>Mini Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Complete W3C Certified HTML</li>
						<li><strong>FREE</strong> 5 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li>Dedicated Designer and Developer</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
						<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
					   </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
					 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentyeight">
				  <div class="collapse-top collapsed" data-target="#twentyeight" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$3040</strike> $1520</h6>
					  <p><strong class="text-uppercase">Professional</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentyeight">
					<div class="package-list">
					  <ul>
						<li>8 Unique Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Upto 300 Products</li>
						<li>Upto 10 Categories</li>
						<li>Customer's Login Area</li>
						<li>Full Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Easy Product Search</li>
						<li>Product Reviews</li>
						<li>Complete W3C Certified HTML</li>
						<li class="grad-color"> BASIC LOGO DESIGN</li>
						<li>2 Logo Design Concepts</li>
						<li>1 Dedicated Logo Designer</li>
						<li>Unlimited Revisions*</li>
						<li>5 Promotional Banners</li>
						<li><strong>FREE</strong> 10 Stock Photos</li>
						<li><strong>FREE</strong> Google Friendly Sitemap</li>
						<li>Team of Expert Designers and Developers</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="col-md-4">
				<div class="pricing-box twentynine">
				  <div class="collapse-top collapsed" data-target="#twentynine" aria-expanded="true">
					<div class="price-box">
					  <h6><strike>$9100</strike> $4550</h6>
					  <p><strong class="text-uppercase">Premium</strong></br> E-Commerce Website</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="twentynine">
					<div class="package-list">
					  <ul>
						<li>Unlimited Pages Design</li>
						<li>Content Management System (CMS)</li>
						<li>Unlimited Products</li>
						<li>Unlimited Categories</li>
						<li>Customer's Login Area</li>
						<li>Full Shopping Cart Integration</li>
						<li>Payment Module Integration</li>
						<li>Sales & Inventory Management</li>
						<li>Newsletter Subscription List Integration</li>
						<li>Easy Product Search</li>
						<li>Product Reviews</li>
						<li>Complete W3C Certified HTML</li>
						<li class="grad-color"> PROFESSIONAL LOGO DESIGN PACKAGE</li>
						<li>5 Logo Design Concepts</li>
						<li>2 Dedicated Logo Designers</li>
						<li>Unlimited Revisions*</li>
						<li class="grad-color"> LANDING PAGE</li>
						<li>1 Customized Landing Page Design</li>
						<li class="grad-color"> SOCIAL MEDIA</li>
						<li>Facebook, Youtube & Twitter Pages Design</li>
						<li class="grad-color"> BROCHURE</li>
						<li>1 Double Sided Brochure Design (A4) or Trifold</li>
						<li>5 Promotional Banners</li>
						<li>FREE 1 Stationery Set<span>(Letterhead, Envelope, Business Card)</span></li>
						<li>FREE 15 Stock Photos</li>
						<li>FREE Search Engine Submission</li>
						<li>FREE Google Friendly Sitemap</li>
						<li>Team of Dedicated Designers and Developers</li>
						<li>Dedicated Brand Experts</li>
						<li>100% Satisfaction Guarantee</li>
						<li>100% Unique Design Guarantee</li>
						<li>100% Money Back Guarantee*</li>
						
					  </ul>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				 <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-md-1"></div>
			  <div class="col-md-10">
				<div class="pricing-box thirty bundle">
				  <div class="collapse-top collapsed" data-target="#thirty" aria-expanded="true">
					<div class="price-box">
					  <h6> <strike>$7000</strike> $3500</h6>
					  <p><strong class="text-uppercase">Bundle</strong></br> (Logo & Web)<br>E-Commerce Website / Logo</p>
					</div>
				  </div>
				  <h4>Suitable for potential super-startups
			and brand revamps for companies.</h4>
				  <div class="collapse-div" id="thirty">
					<div class="package-list">
						<div class="col-md-4">
							<h6>Logo Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">12 Unique Logo Concepts<strong>(FREE)</strong></li>
                                <li>Complementary Icon<strong>(FREE)</strong></li>
                                <li>Unlimited Revisions*<strong>(FREE)</strong></li>
                                <li>100% Ownership Rights<strong>(FREE)</strong></li>
                                <li>AI, PSD, EPS, GIF, BMP, JPEG PNG Formats<strong>(FREE)</strong></li>
                                <li class="last">Stationery Design <strong>(FREE)</strong></li>
                            </ul>
							<p class="perpage mt40">Urgent/Rush Delivery Charges<br> <strong> $50 </strong>(Within 12 Hours )</p>
						</div>
						<div class="col-md-4">
							<h6>Web Design</h6>
							<ul style="border-right: 1px solid #c1c1c1;">
                                <li class="first active">Unlimited Unique Pages Design</li>
                                <li>Content Management System (CMS)</li>
                                <li>Upto 300 Products</li>
                                <li>Upto 10 Categories</li>
                                <li>Customer's Login Area</li>
                                <li>Full Shopping Cart Integration</li>
                                <li>Payment Module Integration</li>
                                <li>Easy Product Search</li>
                                <li>Product Reviews</li>
                                <li>Complete W3C Certified HTML</li>
								<li>1 Year Maintenance/Support <strong>(FREE)</strong></li>
								<li>Secure Sockets Layer(SSL) <strong>(FREE)</strong></li>
								<li class="last">Security Pack <strong>(Add-on)</strong></li>
                            </ul>
						</div>
						<div class="col-md-4">
							<h6>Branding</h6>
							<ul>
                                <li class="first active">Social Media Covers - Facebook/Twitter/YouTube <strong>(FREE)</strong></li>
                                <li>Letterhead Design <strong>(FREE)</strong></li>
                                <li>Envelope Design <strong>(FREE)</strong></li>
                                <li class="last">Professional Business Card Design <strong>(FREE)</strong></li>
                            </ul>
						</div>
						<div class="col-xs-12">
                            <ul>
                                <li class="first active"><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>10 Stock Photos</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Google Friendly Sitemap</li>
                                <li><i class="fa fa-check-circle" aria-hidden="true"></i> <strong> FREE </strong>Team of Expert Designers and Developers</li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
								<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
								<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
                            </ul>
                        </div>
					</div>
				  </div>
				  <p class="add-on"><b>$50/-</b> per Additional Page</p>
				   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>				  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
			  </div>
			   <div class="col-md-1"></div>
			</div>                              
        </div>
        
        
        
                                    
    </div>
</div>
					
					
					
					<!---->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="logo-pkg">
                            <div class="tabs logo-pricing">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class=""><a href="#illus-pkg" data-toggle="tab" aria-expanded="false"> Illustrative <span>Design</span></a></li>
		<li class=""><a href="#mascot-pkg" data-toggle="tab" aria-expanded="false"> Mascot <span>Design</span></a></li>
		<li class=""><a href="#3d-pkg" data-toggle="tab" aria-expanded="false">3D <span>Design</span> </a></li>
		<li class=""><a href="#abs-pkg" data-toggle="tab" aria-expanded="false">Abstract <span>Design</span></a></li>
		<li class=""><a href="#icon-pkg" data-toggle="tab" aria-expanded="true">Iconic <span>Design</span></a></li>
		<li class="active"><a href="#flat-pkg" data-toggle="tab" aria-expanded="false">Flat <span>Design</span> </a></li>
		<!--<li class=""><a href="#real-pkg" data-toggle="tab" aria-expanded="false">Realistic Logo Design</a></li>-->
    </ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--illus-logo-->
		<div id="illus-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$398</strike> $199</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box two">
					  <div class="collapse-top collapsed" data-target="#two" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$598</strike> $299</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="two">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box three">
					  <div class="collapse-top collapsed" data-target="#three" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$798</strike> $399</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Illustrative Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="three">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--mascot-logo-->
		<div id="mascot-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box four">
					  <div class="collapse-top collapsed" data-target="#four" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="four">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box five">
					  <div class="collapse-top collapsed" data-target="#five" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$700</strike> $350</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="five">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box six">
					  <div class="collapse-top collapsed" data-target="#six" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$1100</strike> $550</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Mascot Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="six">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50 for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--3d-logo-->
		<div id="3d-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box seven">
					  <div class="collapse-top collapsed" data-target="#seven" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="seven">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eight">
					  <div class="collapse-top collapsed" data-target="#eight" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$800</strike> $400</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eight">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box nine">
					  <div class="collapse-top collapsed" data-target="#nine" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$1200</strike> $600</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> 3D Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="nine">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--abs-logo-->
		<div id="abs-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box ten">
					  <div class="collapse-top collapsed" data-target="#ten" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$160</strike> $80</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="ten">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eleven">
					  <div class="collapse-top collapsed" data-target="#eleven" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$240</strike> $120</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eleven">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box tweleve">
					  <div class="collapse-top collapsed" data-target="#tweleve" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$500</strike> $250</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Abstract Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="tweleve">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--icon-logo-->
		<div id="icon-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box thirteen">
					  <div class="collapse-top collapsed" data-target="#thirteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$120</strike> $60</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="thirteen">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					    <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box forteen">
					  <div class="collapse-top collapsed" data-target="#forteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$190</strike> $95</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="forteen">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box fifteen">
					  <div class="collapse-top collapsed" data-target="#fifteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$300</strike> $150</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Iconic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="fifteen">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		
		<!--flat-logo-->
		<div id="flat-pkg" class="tab-pane active">
			<div class="row mr">
			<div class="col-md-4">
					<div class="pricing-box sixteen">
					  <div class="collapse-top collapsed" data-target="#sixteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$96</strike> $48</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="sixteen">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box seventeen">
					  <div class="collapse-top collapsed" data-target="#seventeen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$180</strike> $90</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="seventeen">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50  for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box eighteen">
					  <div class="collapse-top collapsed" data-target="#eighteen" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$240</strike> $120</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Flat Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="eighteen">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							<li>Free Final Files</li>
							<li>Free LetterHead Designs</li>
							<li>Free Social Media Pack</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
							<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $50 for 12 Hours Rush Delivery</p>
					   <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>					  <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>
		<!--real-logo--
		<div id="real-pkg" class="tab-pane">
			<div class="row mr">
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$63</strike> $20</h6>
						  <p><strong class="text-uppercase">Basic</strong><br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>2 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					   					  <a class="order_now" href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$123</strike> $49</h6>
						  <p><strong class="text-uppercase">Professional</strong><br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>4 Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
							
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					   					  <a class="order_now" href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing-box one">
					  <div class="collapse-top collapsed" data-target="#one" aria-expanded="true">
						<div class="price-box">
						  <h6><strike>$198</strike> $79</h6>
						  <p><strong class="text-uppercase">Premium</strong></br> Realistic Logo Design Package</p>
						</div>
					  </div>
					  <h4>Suitable for potential super-startups
				and brand revamps for companies.</h4>
					  <div class="collapse-div" id="one">
						<div class="package-list">
						  <ul>
							<li>6 Unique Logo Design Concepts</li>
							<li>Unlimited Revisions</li>
							<li>Free Stationery Design</li>
							<li>Free Color Options</li>
							<li>Free Grayscale Format</li>
						  </ul>
						</div>
					  </div>
					  <p class="add-on">Add on $15  for 12 Hours Rush Delivery</p>
					  <p class="vat">*VAT EXCLUDED</p>
					  					  <a class="order_now" href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/account/orderformredirector?pkid=000">Order Now</a> 
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
        </div>-->
	</div>
</div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="corp-iden-pkg">
                            <div class="tabs logo-pricing">
    <ul class="nav nav-tabs top-tabs web-tabs" id="interest_tabs">
        <!--top level tabs-->
        <!-- <li><a href="#website_design" data-toggle="tab">Website Design</a></li>-->
		<li class="active"><a href="#stat-pkg" data-toggle="tab" aria-expanded="true">Stationery <span>Design</span></a></li>
        <li class=""><a href="#sm-pkg" data-toggle="tab" aria-expanded="false"> Social Media <span>Design</span></a></li>
		<li class=""><a href="#brou-pkg" data-toggle="tab" aria-expanded="false"> Brochure <span>Design</span></a></li>
		<li class=""><a href="#banner-pkg" data-toggle="tab" aria-expanded="false">Banner <span>Design</span> </a></li>
		<li class=""><a href="#promo-pkg" data-toggle="tab" aria-expanded="false">Promotional <span>Design</span></a></li>
		<li class=""><a href="#mag-pkg" data-toggle="tab" aria-expanded="false">Magazine <span>Design</span></a></li>
	</ul>
                                
    <!--brands tab content-->
    <div class="tab-content">
		<!--stat-design-->
		<div id="stat-pkg" class="tab-pane active">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box thirtyone">
      <div class="collapse-top collapsed" data-target="#thirtyone" aria-expanded="true">
        <div class="price-box">
          <h6>$98</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Stationery Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyone">
        <div class="package-list">
          <ul>
            <li>1 Stationery Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><strike><i class="fa fa-check-circle" aria-hidden="true"></i> FREE MS Word Letterhead</strike></li>
            <li><strike><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Fax Template</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtytwo">
      <div class="collapse-top collapsed" data-target="#thirtytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$160</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Stationery Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtytwo">
        <div class="package-list">
          <ul>
            <li>2 Stationery Design Concepts</li>
            <li>2 Dedicated Designers</li>
            <li>3 Revisions Round</li>
            <li><strike>24 Hours Delivery Time</strike></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Tri-fold Brochure</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Fax Template</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtythree">
      <div class="collapse-top collapsed" data-target="#thirtythree" aria-expanded="true">
        <div class="price-box">
          <h6>$250</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Stationery Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtythree">
        <div class="package-list">
          <ul>
            <li>2 Stationery Design Concepts</li>
            <li>2 Dedicated Designers</li>
            <li>5 Revisions Round</li>
			<li><strike>24 Hours Delivery Time</strike></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 4 Pages Brochure (A4 Executive)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 T-Shirt Design</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Sticker Design</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Sticker Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--sm-design-->
		<div id="sm-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box thirtyfour">
      <div class="collapse-top collapsed" data-target="#thirtyfour" aria-expanded="true">
        <div class="price-box">
          <h6>$60</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Social Media Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyfour">
        <div class="package-list">
          <ul>
            <li>Facebook, Youtube & Twitter Pages Design</li>
            <li>1 Dedicated Logo Designer</li>
            <li>3 Rounds of Revision</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtyfive">
      <div class="collapse-top collapsed" data-target="#thirtyfive" aria-expanded="true">
        <div class="price-box">
          <h6>$98</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Social Media Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyfive">
        <div class="package-list">
          <ul>
            <li>Book Cover</li>
            <li>2 Dedicated Logo Designers</li>
            <li>5 Rounds of Revision</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Bookmark Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Icon Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Email Signature Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtysix">
      <div class="collapse-top collapsed" data-target="#thirtysix" aria-expanded="true">
        <div class="price-box">
          <h6>$150</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Social Media Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtysix">
        <div class="package-list">
          <ul>
            <li>Catalogue ( Only Theme )</li>
            <li>2 Dedicated Logo Designers</li>
            <li>Unlimited Revisions*/li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Postcard Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Product Label Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Sticker Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Poster Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Flyer Design (Single Sided)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  FREE 8 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--brou-design-->
		<div id="brou-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box thirtyseven">
      <div class="collapse-top collapsed" data-target="#thirtyseven" aria-expanded="true">
        <div class="price-box">
          <h6>$140</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Brochure Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyseven">
        <div class="package-list">
          <ul>
            <li>Tri-fold Brochure</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Bookmark Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtyeight">
      <div class="collapse-top collapsed" data-target="#thirtyeight" aria-expanded="true">
        <div class="price-box">
          <h6>$199</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Brochure Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtyeight">
        <div class="package-list">
          <ul>
            <li>4 Pages Brochure</li>
            <li>2 Dedicated Logo Designers</li>
            <li>5 Rounds of Revision</li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Stationery Set</br><span>(Letterhead, Envelope, Business Card)</span></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Fax Template Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 5 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
        <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box thirtynine">
      <div class="collapse-top collapsed" data-target="#thirtynine" aria-expanded="true">
        <div class="price-box">
          <h6>$235</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Brochure Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="thirtynine">
        <div class="package-list">
          <ul>
            <li>8 Pages Brochure</li>
            <li>2 Dedicated Logo Designers</li>
            <li>Unlimited Revisions*/li>
            <li><strike>24 Hours Delivery Time</strike></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 1 Stationery Set</br><span>(Letterhead, Envelope, Business Card)</span></li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> MS Word Letterhead</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Email Template Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Flyer (single sided) Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Promotional Mug Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  FREE 5 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--banner-design-->
		<div id="banner-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box forty">
      <div class="collapse-top collapsed" data-target="#forty" aria-expanded="true">
        <div class="price-box">
          <h6>$75</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="forty">
        <div class="package-list">
          <ul>
            <li>1 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyone">
      <div class="collapse-top collapsed" data-target="#fortyone" aria-expanded="true">
        <div class="price-box">
          <h6>$95</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyone">
        <div class="package-list">
          <ul>
            <li>2 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>4 Revisions Round</li>
            <li>24 Hours Delivery Time</li>
			     <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortytwo">
      <div class="collapse-top collapsed" data-target="#fortytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$145</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortytwo">
        <div class="package-list">
          <ul>
            <li>2 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>4 Revisions Round</li>
			<li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--promo-design-->
		<div id="promo-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortythree">
      <div class="collapse-top collapsed" data-target="#fortythree" aria-expanded="true">
        <div class="price-box">
          <h6>$89</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Promotional Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortythree">
        <div class="package-list">
          <ul>
            <li>T-Shirt</li>
            <li>1 Dedicated Logo Designer</li>
            <li>3 Rounds of Revision</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Promotional Mug Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortytwo">
      <div class="collapse-top collapsed" data-target="#fortytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$119</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Promotional Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortytwo">
        <div class="package-list">
          <ul>
            <li>Book Cover</li>
            <li>2 Dedicated Logo Designers</li>
            <li>5 Rounds of Revision</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Bookmark Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Icon Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Email Signature Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE 2 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortythree">
      <div class="collapse-top collapsed" data-target="#fortythree" aria-expanded="true">
        <div class="price-box">
          <h6>$399</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Promotional Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortythree">
        <div class="package-list">
          <ul>
            <li>Catalogue ( Only Theme )</li>
            <li>2 Dedicated Logo Designers</li>
            <li>Unlimited Revisions*/li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Postcard Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Product Label Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Sticker Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Poster Design</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Flyer Design (Single Sided)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  FREE 8 Stock Photos</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG  in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
		<!--Amgzine-design-->
		<div id="mag-pkg" class="tab-pane">
			<div class="row mr">
				<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fifty">
      <div class="collapse-top collapsed" data-target="#fifty" aria-expanded="true">
        <div class="price-box">
          <h6>$399</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Magazine Cover Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fifty">
        <div class="package-list">
          <ul>
            <li>1 Front Cover Design</li>
            <li>1 Back Cover Design</li>
            <li>1 Table Of Content Concept </li>
            <li>Unlimited Revisions</li>
            <li>Free Color Options</li>
            <li>Free Grayscale Format</li>
            <li>Free Final Files</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftyone">
      <div class="collapse-top collapsed" data-target="#fiftyone" aria-expanded="true">
        <div class="price-box">
          <h6>$499</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> Magazine Cover Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftyone">
        <div class="package-list">
          <ul>
            <li>2 Front Cover Design</li>
            <li>2 Back Cover Design</li>
            <li>1 Table Of Content Concept</li>
            <li>Unlimited Revisions</li>
            <li>Free Color Options</li>
            <li>Free Grayscale Format</li>
            <li>Free Final Files</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftytwo">
      <div class="collapse-top collapsed" data-target="#fiftytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$599</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Magazine Cover Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftytwo">
        <div class="package-list">
          <ul>
            <li>2 Front Cover Design</li>
            <li>2 Back Cover Design</li>
            <li>2 Table Of Content Concept</li>
            <li>Unlimited Revisions</li>
            <li>Free Color Options</li>
            <li>Free Grayscale Format</li>
            <li>Free Final Files</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
			</div>
        </div>
	</div>
</div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="vid-anim-pkg">
                            <div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortyfour">
      <div class="collapse-top collapsed" data-target="#fortyfour" aria-expanded="true">
        <div class="price-box">
          <h6>$399</h6>
          <p>2D Video Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyfour">
        <div class="package-list">
          <ul>
            <li>1 Video Concept</li>
            <li>Up to 10 Seconds Duration</li>
            <li>Unlimited Revisions*</li>
            <li>1 Week Delivery</li>
			<li class="grad-color">$100 Additional Charges Includes</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Storyboard Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Background Music Included</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i> Voice Over Included</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyfive">
      <div class="collapse-top collapsed" data-target="#fortyfive" aria-expanded="true">
        <div class="price-box">
          <h6>$699</h6>
          <p>3D Video Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyfive">
        <div class="package-list">
          <ul>
            <li>1 Video Concept</li>
            <li>Up to 10 Seconds Duration</li>
            <li>Unlimited Revisions*</li>
            <li>2 Week Delivery</li>
			<li class="grad-color">$100 Additional Charges Includes</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Storyboard Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Background Music Included</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i> Voice Over Included</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortysix">
      <div class="collapse-top collapsed" data-target="#fortysix" aria-expanded="true">
        <div class="price-box">
          <h6>$999</h6>
          <p>Animated Video Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortysix">
        <div class="package-list">
          <ul>
            <li>1 Video Concept</li>
            <li>Up to 10 Seconds Duration</li>
            <li>Unlimited Revisions*</li>
            <li>2 Week Delivery</li>
			<li class="grad-color">$100 Additional Charges Includes</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Storyboard Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Background Music Included</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i> Voice Over Included</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                          </div>
						  <div role="tabpanel" class="tab-pane fade" id="cpy-write-pkg">
                            <div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fortyseven">
      <div class="collapse-top collapsed" data-target="#fortyseven" aria-expanded="true">
        <div class="price-box">
          <h6>$500</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyseven">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 6 web pages)</li>
            <li>Copywriting (Up to 6 web pages)</li>
            <li>Article Writing (Up to 5 web pages)</li>
            <li>Blog Writing (Up to 1200 words)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> Proofreading and editing</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Re-write/Edit existing web copy (Up to 8 web pages)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyeight">
      <div class="collapse-top collapsed" data-target="#fortyeight" aria-expanded="true">
        <div class="price-box">
          <h6>$1000</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyeight">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 12 web pages)</li>
            <li>Copywriting (Up to 12 web pages)</li>
            <li>Article Writing (Up to 10 web pages)</li>
            <li>SEO Articles (6)</li>
            <li>Press Releases (3)</li>
            <li>On-line Catalogues</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i>  Proofreading and editing</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i>  Re-write/Edit existing web copy</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Sales Letters</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortynine">
      <div class="collapse-top collapsed" data-target="#fortynine" aria-expanded="true">
        <div class="price-box">
          <h6>$1800</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Web Copywriting Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortynine">
        <div class="package-list">
          <ul>
            <li>Company Profile Summary</li>
            <li>Brand Promise Points: Vision, Differentiation, 
Solution, Benefit, Motivation, Expression</li>
            <li>Research and concepting (Up to 30 web pages)</li>
            <li>Copywriting (Up to 30 web pages)</li>
            <li>Article Writing (Up to 20 web pages)</li>
            <li>Blog Writing (Up to 6000 words)</li>
            <li>SEO Articles (20)</li>
            <li>Press Releases (7)</li>
            <li>Original Web Content Copy</li>
            <li>Newsletters (2)</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> Script Included</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Re-write/Edit existing web copy</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> Sales Letters</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  E-mail Marketing Letter</li>
            <li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  On-line Catalogues</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                          </div>
						  <div role="tabpanel" class="tab-pane fade" id="app-design-pkg">
                            <div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box fifty">
      <div class="collapse-top collapsed" data-target="#fifty" aria-expanded="true">
        <div class="price-box">
          <h6>$1500</h6>
          <p><strong class="text-uppercase">Basic</strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fifty">
        <div class="package-list">
          <ul>
            <li>Upto 10 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftyone">
      <div class="collapse-top collapsed" data-target="#fiftyone" aria-expanded="true">
        <div class="price-box">
          <h6>$2000</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftyone">
        <div class="package-list">
          <ul>
            <li>Upto 20 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fiftytwo">
      <div class="collapse-top collapsed" data-target="#fiftytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$2500</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> App Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fiftytwo">
        <div class="package-list">
          <ul>
            <li>Upto 30 App Screen Designs</li>
            <li>Splash screen animations</li>
            <li>Unlimited Designs Revisions </li>
            <li>App Logo</li>
            <li>App Icons(Phone/Tablets)</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+971 45217500">+971 45217500</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                          </div>
                        </div>
                      </div>
              </div>
            </div>
			
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
				<h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/packages.php" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>              </div>
            </div>
          </div>
        </section>

<section class="reviews-links">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <h3 class="wow fadeInUp">Clients are endorsing Bt software Designs</h3>
          <p class="wow fadeInUp">Check what they have to say about us</p>
        </div>
        <div class="col-md-7">
          <ul>
            <li class="wow fadeInLeft" data-wow-delay="0.1s"><a href="javascript:;" target="_blank"><img src="assets/images/lp/google.png"></a></li>
            <li class="wow fadeInLeft" data-wow-delay="0.3s"><a href="javascript:;" target="_blank"><img src="assets/images/lp/facebook.png"></a></li>
            <li class="wow fadeInLeft" data-wow-delay="0.5s"><a href="javascript:;" target="_blank"><img src="assets/images/lp/trust-piolot.png"></a></li>
            <li class="wow fadeInLeft" data-wow-delay="0.7s"><a href="javascript:;" target="_blank"><img src="assets/images/lp/bbb.png"></a></li>
            <li class="wow fadeInLeft" data-wow-delay="0.9s"><a href="javascript:;" target="_blank"><img src="assets/images/lp/yelp.png"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
<section class="portfolio_area hidden-xs">
<img src="assets/images/lp/ecommerce/testim-right-bg.png" class="b2b-img wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
            <!--<div class="container">-->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="wow fadeInUp">Our Process</h1>
			<img src="assets/images/our-process-img.png" class="wow fadeInLeft animated">
		</div>
	</div>
	
    <!--<div class="b2b-portf-mac">
		<div class="b2b-portf-mac-wrapper">
			<div id="owl-demo-logo-design" class="owl-carousel">
				<div class="item">
                    <div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="assets/images/portfolio/b2b-b2c-portal/img1.jpg">
                        </div>

                    </div>
                </div>
				<div class="item">
                    <div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="assets/images/portfolio/b2b-b2c-portal/img2.jpg">
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="assets/images/portfolio/b2b-b2c-portal/img3.jpg">
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="assets/images/portfolio/b2b-b2c-portal/img4.jpg">
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="assets/images/portfolio/b2b-b2c-portal/img5.jpg">
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>-->

</div>
<div class="container">
    <div class="p200">
        <div class="row">
            <div class="col-md-12">
                 <div class="cus-port-btn-con"><a href="#" class="btn grad-color wow fadeInUp animated" title="" data-toggle="modal" data-target="#SignupModal">GET STARTED</a> <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color wow fadeInUp animated price" title="">VIEW MORE PORTFOLIO</a></div>
            </div>
       </div>
    </div>
</div>

            <!--</div>-->
</section>
<div class="clearfix"></div> 
		
		<!-- <section class="case ">
          <h1 class="wow fadeInUp">Case Studies</h1>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="carousel slide" data-ride="carousel" id="client-slider-v1" data-interval="false">
                            <ol class="carousel-indicators">
                                <li data-target="#client-slider-v1" data-slide-to="0" class="active"><img class="img-responsive " src="assets/images/case-img.jpg" alt="">
                                </li>
                                <li data-target="#client-slider-v1" data-slide-to="1"><img class="img-responsive" src="assets/images/case-img2.jpg" alt="">
                                </li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                        <div class="row">
                                          <div class="col-sm-6"></div>
                                            <div class="col-sm-6">
                                              <div class="descp">
                                                <h2>Expert Logistics Service</h2>
                                                <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage, in a performance devoid of content yet rich in form. That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. Who's paying you to waste this time, anyway?</p>
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color mt-50">VIEW PROJECT</a>
                                              </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="item">
                                        <div class="row">
                                          <div class="col-sm-6"></div>
                                            <div class="col-sm-6">
                                              <div class="descp">
                                                <h2>Expert Logistics Service</h2>
                                                <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage, in a performance devoid of content yet rich in form. That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. Who's paying you to waste this time, anyway?</p>
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/portfolio.php" class="btn grad-color mt-50">VIEW PROJECT</a>
                                              </div>
                                            </div>
                                        </div>
                                </div>
                            </div>

                            <a data-slide="prev" href="#client-slider-v1" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#client-slider-v1" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
        </section> -->
		<div class="clearfix"></div>
		
   

        <section class="fun-facts">
          <div class="left"><img src="assets/images/laptop.png"></div>
          <div class="right counter-section">
            <h1 class="wow fadeInUp">WE ARE <span>PROFESSIONAL</span> DIGITAL AGENCY</h1>
            <p class="wow fadeInUp">We are a proud squad of devoted professionals, bound to create digital experiences that generate significant outcomes. </p>
              <div class="counter-wrap wow fadeInUp">
                <strong><span class="timer">5000</span>+</strong>
                <span class="count-description">SATISFIED CUSTOMER</span>
              </div> <!-- /.col-sm-4 -->
              <div class="counter-wrap wow fadeInUp" data-wow-delay="0.5s">
                <strong><span class="timer">1650</span>+</strong>
                <span class="count-description">PROJECTS COMPLETED</span>
              </div><!-- /.col-sm-4 -->
              <div class="counter-wrap wow fadeInUp" data-wow-delay="0.7s">
                <strong><span class="timer">160</span>+</strong>
                <span class="count-description">LAUNCHED PRODUCTS</span>
              </div><!-- /.col-sm-4 -->
          </div>
        </section>

        <div class="clearfix"></div>

        <section class="testim">
          <img src="assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much Bt software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have Bt software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>
        <section class="blog-sec">
          <img src="assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From Bt software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="assets/images/blog/blog-1.jpg">
                  <h3><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog/blog1.php"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog/blog2.php">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog/blog3.php">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog/blog4.php">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/blog.php" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
				</section>
				
				<?php include 'footer.php';?>