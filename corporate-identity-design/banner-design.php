
<?php 
// echo('http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder.'/header.php');
include '../header.php';
?> 

        <!-- Intro Section -->
       <section id="about-slider">
            <div id="tt-home-carousel" class="carousel slide carousel-fade trendy-slider control-one" data-ride="carousel" data-interval="5000">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/banner-design/slide-1.jpg" alt="First slide" class="img-responsive">
                    <div class="carousel-caption">
                      <h1 class="animated fadeInDown delay-1"><span>Banner Designs </span></h1>
                      
                    </div>
                  </div>
                </div> <!-- /.carousel-inner -->

                <!-- Controls -->
                <!-- <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->

            </div> <!-- /.carousel -->
        </section> <!-- /#home -->
		<div class="clearfix"></div>
		
<section class="video-text">
<div class="container">
<div class="row">
	<div class="col-md-12">
		<h2 class="wow fadeInUp">Create striking and eye-catching <span>Web-banners</span> with <br><span class="text-grad">BT software</span>.</h2>
		<p class="wow fadeInUp">We’re a leading design agency that delivers customized web banner designs, banner ads, and flash animated web banner designs. <br>
			<br >We deliver a promising quality banner designs as per our client’s project demands and business goals. Our team make sure to create impressive designs leaving a lasting impression for your visitors. 
		</p>
		<a href="javascript:;" data-toggle="modal" data-target="#SignupModal" class="btn grad-color read-more-info">Let's Get Started</a>
	</div>	
</div>
</div>
</section>

<section class="portfolio-sec">
          <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/client-bg.png" class="shape">
          <div class="container">
            <h1 class="wow fadeInUp">We are serving <span>2000+</span> Clients</h1>
            <p class="wow fadeInUp">We create experiences that transform brands & grow businesses. We have enriched some of the biggest brands and our partnerships with them have caused the creation of some outstanding outcomes. </p>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row logo logo-portfolio">
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img1.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img1.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img2.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img2.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img3.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img3.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img4.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img4.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img5.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img5.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="hovereffect-portfolio">
			<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img6.jpg" alt="image" class="img-responsive center-block">
			<div class="overlayPort">
				<ul class="info text-center list-inline">
					
					<li class="last">
						<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/corporate-identity-design/stationary-design/img6.jpg" class="fancybox-pop">
							<i class="ion-android-search"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
                              
                             
</div>                        
                </div>
              </div>
            </div>
            <div class="text-center"><a href="/portfolio/" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL</a></div>          </div>
</section>
<div class="clearfix"></div>
<section class="cta-logo wow fadeInRight animated">
			<div class="container">
				<div class="row">
					<div class="col-md-5 wow fadeInLeft animated">
						<h2>LET’S START DISCUSSION!</h2>
						<p>Just fill out the sign up form and one of our team members will get back to you.</p>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/chat-icon.png">
							<p><a href="javascript:$zopim.livechat.window.show();">Live Chat<br>With Us</a> </p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="chat column wow fadeInUp animated">
							<img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/call-icon.png">
							<p>Call Us:<br> <a href="tel:+19174750802">+191-74750802</a> </p>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="chat column wow fadeInUp animated">
							<p class="btn-req"><a href="#" data-toggle="modal" data-target="#SignupModal">Request Info</a></p>
						</div>
					</div>
					
				</div>
			</div>
		</section>		<section class="packages portfolio-sec">
          <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/packages-offer.png" class="shape">
          <div class="container">
            <div class="row text-center">
              <div class="col-m-12">
                <h1 class="wow fadeInUp">Banner Designs Packages We Offer</h1>
                <p class="wow fadeInUp">BT software Design believes in delivering distinctive services in competitive price models. We have some attractive packages especially crafted for every service offered to offer the finest quality within the budget. </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="mission-tab">
					<div class="row mr">
  <div class="col-md-4">
    <div class="pricing-box forty">
      <div class="collapse-top collapsed" data-target="#forty" aria-expanded="true">
        <div class="price-box">
          <h6>$75</h6>
          <p><strong class="text-uppercase">Basic</strong><br> Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="forty">
        <div class="package-list">
          <ul>
            <li>1 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>3 Revisions Round</li>
            <li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortyone">
      <div class="collapse-top collapsed" data-target="#fortyone" aria-expanded="true">
        <div class="price-box">
          <h6>$95</h6>
         <p><strong class="text-uppercase">PROFESSIONAL </strong><br>Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortyone">
        <div class="package-list">
          <ul>
            <li>2 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>4 Revisions Round</li>
            <li>24 Hours Delivery Time</li>
			     <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
  <div class="col-md-4">
    <div class="pricing-box fortytwo">
      <div class="collapse-top collapsed" data-target="#fortytwo" aria-expanded="true">
        <div class="price-box">
          <h6>$145</h6>
          <p><strong class="text-uppercase">PREMIUM</strong><br> Banner Design Package</p>
        </div>
      </div>
      <h4>Suitable for potential super-startups
and brand revamps for companies.</h4>
      <div class="collapse-div" id="fortytwo">
        <div class="package-list">
          <ul>
            <li>2 Banner Design Concept</li>
            <li>1 Dedicated Designer</li>
            <li>4 Revisions Round</li>
			<li>24 Hours Delivery Time</li>
            <li><i class="fa fa-check-circle" aria-hidden="true"></i> FREE Final Files<br>
				<span>(Including AI, EPS, PSD, PDF, JPEG and PNG in formats for all print and web use)</span>
			</li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Satisfaction Guarantee</b></li>
			<li><i class="fa fa-check-circle" aria-hidden="true"></i> <b>100% Unique Design Guarantee</b></li>
			<li class="last"><i class="fa fa-check-circle" aria-hidden="true"></i>  <b>100% Money Back Guarantee*</b></li>
          </ul>
			
        </div>
		
      </div>
       <div class="call-to">
        <div>
          <p>Start Your Work</p>
          <p><a href="tel:+19174750802">+191-74750802</a></p>
        </div>
        <div>
          <p>For More Detail</p>
          <p><a href="javascript:$zopim.livechat.window.show();">Chat With us</a></p>
        </div>
      </div>      <a class="order_now" href="#" data-toggle="modal" data-target="#SignupModal">Order Now</a> </div>
  </div>
</div>
                       
                </div>
              </div>
            </div>
            <div class="row v-a-pack text-center">
              <div class="col-md-12">
                <h1 class="text-center text-grad wow fadeInUp">WORK AND PLAY WITH PASSION</h1>
                <p class="wow fadeInUp">Passion for excellence is the key driver to our success. We strive to exceed the expectations of our clients. We thrive on helping our clients to make their innovative ideas concrete! </p>
<a href="/packages/" class="btn grad-color mt-30 wow fadeInUp">VIEW ALL PACKAGES</a>              </div>
            </div>
          </div>
        </section>
<div class="clearfix"></div>
		


<section class="our-client  pt-40 pb-50">
	<div class="container">
    	<div class="row text-center">
        	<div class="col-md-12">
            	<h1 class="primary-color text-uppercase">Our Partners</h1>
                <p class="hidden-xs">We are proud to have partnered with some of the region's and the world's leading companies, <br> bringing their brands to life and transforming their people and customer experiences.</p>
            </div>
        </div>
        	<div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl1.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl1.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl2.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl2.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl3.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl3.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl4.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl4.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl5.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl5.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl6.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl6.jpg" style="display: block;"></div>
            </div>

        	<div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl7.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl7.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl11.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl11.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl12.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl12.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl13.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl13.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl14.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl14.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl15.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl15.jpg" style="display: block;"></div>
            </div>


            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl16.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl16.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl17.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl17.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl18.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl18.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl19.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl19.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl20.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl20.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl21.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl21.jpg" style="display: block;"></div>
            </div>


            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl22.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl22.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl23.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl23.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl24.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl24.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl25.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl25.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl26.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl26.jpg" style="display: block;"></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4">
            	<div class="cl"><img data-original="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl27.jpg" class="img-responsive lazy" width="430" height="267" src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/clients/cl27.jpg" style="display: block;"></div>
            </div>

        <div class="row mt-50 text-center">
        	<div class="col-md-12">
            	<a href="/portfolio/" class="btn grad-color mt-20">VIEW MORE</a>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
	
		


		<section class="testim">
          <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/testi-img.png" class="wow fadeInLeft">
          <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <h1 class="wow fadeInUp">Check What Our Client Say About us</h1>
                  <div class="owl-carousel testimonials">
                    <div class="item">
                      <p>The company is perfect for a new business startup. Price is well worth the finish item. A special thanks to their team members who were in constant contact with me; I look forward seeing my final website. Thank you for everything.</p>
                      <h5>Yolande J. Martin</h5>
                      <h6>Admin officer</h6>
                    </div>
                    <div class="item">
                      <p>So far the amazing customer service. My boss wanted some personalized service and they really helped us out. The total came out reasonable comparing with other companies which we checked! Great job.</p>
                      <h5>Anderson Keith</h5>
                      <h6>Operations manager</h6>
                    </div>
					<div class="item">
                      <p>Thank you so much BT software design agency and the assigned team which finished my project. I was not expecting this much amount of work, but you guys blow my expectations. I highly recommend their services and will be coming back to use them in the future needs.</p>
                      <h5>Brittney</h5>
                      <h6>Senior brand manager</h6>
                    </div>
					<div class="item">
                      <p>Best local design agency, I was flattered by their discounts and to be really honest with you we loved everything starting from logo, brochure, business cards and specially the animated logo. I have had few experiences before but their delivery and service is really good!  Most importantly, they give you knowledge, what's important for you and what's not and I believe that's their best part! Great experience!</p>
                      <h5>Curtis Lindsay</h5>
                      <h6>CEO</h6>
                    </div>
					<div class="item">
                      <p>I'm very pleased to have BT software Design as our design outsource agency, their understanding to our ideas and their response + communication was excellent.</p>
                      <h5>Ethel Hunter </h5>
                      <h6>Director Marketing</h6>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>        
		
		 <section class="blog-sec">
          <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/blog-shape.png" class="shape">
          <div class="container">
            <h2 class="text-center wow fadeInUp">Latest From BT software Design</h2>
            <div class="row mr">
              <div class="col-md-6">
                <div class="box one wow fadeInLeft">
                  <img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/blog/blog-1.jpg">
                  <h3><a href="/blog/blog1"> Why custom logo designs are essential</a></h3>
                  <p>Every company, even it’s a startup requires a professionally designed logo to represent the values and elements of their business.</p>
                  <h5>Sep 7, 2018</h5>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/blog/blog-2.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog2">Custom Website Design</a></h3>
                  <p>In the world of design and business, it’s all about creating that oomph factor that can grab millions of  </p>
                  <h5>Aug 30, 2018 </h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/blog/blog-3.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog3">Enhance Your Brand Visibly</a></h3>
                  <p>One of the major goal of every business is to maximize its reach, attract a large number of customers  </p>
                  <h5>Jul 27, 2018</h5>
                    </div>
                  </div>
                  <div class="row box two wow fadeInRight">
                    <div class="col-md-4 no-padding"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$mainfolder; ?>/assets/images/blog/blog-4.jpg"></div>
                    <div class="col-md-8">
                      <h3><a href="/blog/blog4/">Quick Tips Before you choose</a></h3>
                  <p>Designing a website is a basic step for a company to let the people know about your existence.  </p>
                  <h5> July 16, 2018 </h5>
                    </div>
                  </div>
                </div>
				<div class="clearfix"></div>
                <div class="text-center"><a href="/blog/" class="btn grad-color mt-50 wow fadeInUp">VIEW ALL BLOG</a></div>
            </div>
          </div>
        </section>		
        <?php include '../footer.php';?> 